#include"DxLib.h"
#include "GameCtl.h"
#include "SoundMng.h"
#include "ImageMng.h"
#include "TitleScene.h"
#include "CharacterSelectScene.h"
#include "SceneChange.h"
#include "MainMenuScene.h"

#define SCREEN_SIZE_X (1440)			// ｽｸﾘｰﾝの横ｻｲｽﾞ
#define SCREEN_SIZE_Y (810)				// ｽｸﾘｰﾝの縦ｻｲｽﾞ

#define CENTER_POS_X (416)				// 対戦ﾓｰﾄﾞﾎﾞﾀﾝの中央の位置(x軸)
#define CENTER_POS_Y (206)				// 対戦ﾓｰﾄﾞﾎﾞﾀﾝの中央の位置(y軸)

#define CENTER_TXT_POS (90)				// 対戦ﾓｰﾄﾞの文字のCENTER_POS_Yからの距離(y軸)

MainMenuScene::MainMenuScene()
{
	Init();
	OutputDebugString("MainMenuSceneｸﾗｽのｲﾝｽﾀﾝｽ\n");
}

MainMenuScene::~MainMenuScene()
{
	OutputDebugString("MainMenuSceneｸﾗｽの削除\n");
}

unique_Base MainMenuScene::Update(unique_Base own, const GameCtl & ctl)
{
	OldSceneChangeFlag = SceneChangeFlag;
	AlwaysCnt++;

	// 音
	cursorMoveFlg = false;
	if (CheckSoundMem(lpSoundMng.GetID("bgm/title.mp3")) == 0)
	{
		PlaySoundMem(lpSoundMng.GetID("bgm/title.mp3"), DX_PLAYTYPE_LOOP, true);
	}
	ChangeVolumeSoundMem(lpSceneChange.GetBright(), lpSoundMng.GetID("bgm/title.mp3"));
	// --------

	// モード選択
	if (!moveFlag)
	{
		if ((selectFlag >= 0) && (selectFlag < 3))
		{
			if (inputCancelFlag)
			{
				// 以下の入力処理をしない
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_LEFT))
			{
				selectFlag--;
				if (joypadCnt < 2)
				{
					selectFlag--;
				}
				cursorMoveFlg = true;

				if (selectFlag < 0)
				{
					selectFlag = 0;
					cursorMoveFlg = false;
				}
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_RIGHT))
			{
				selectFlag++;
				if (joypadCnt < 2)
				{
					selectFlag++;
				}
				cursorMoveFlg = true;

				if (selectFlag > 2)
				{
					selectFlag = 2;
					cursorMoveFlg = false;
				}
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_UP))
			{
				if (selectFlag != 0)
				{
					selectFlag = 0;
					cursorMoveFlg = true;
				}
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_DOWN))
			{
				if (selectFlag == 0)
				{
					selectFlag = 1;
					if (joypadCnt < 2)
					{
						selectFlag = 2;
					}
					cursorMoveFlg = true;
				}
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_2))
			{
				nextMode = NEXT_title;
			}
			else
			{
				// 何もしない
			}
		}
		else
		{
			if (inputCancelFlag)
			{
				// 以下の入力処理をしない
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_LEFT))
			{
				selectFlag--;
				cursorMoveFlg = true;

				if (selectFlag < 3)
				{
					selectFlag = 3;
				}
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_RIGHT))
			{
				if (joypadCnt >= selectFlag)
				{
					selectFlag++;
				}
				cursorMoveFlg = true;

				if (selectFlag > 5)
				{
					selectFlag = 5;
				}
			}
			else if (ctl.GetPadTrigger(0, PAD_INPUT_2))
			{
				Init();
			}
			else
			{
				// 何もしない
			}
		}

		if (inputCancelFlag)
		{
			// 以下の入力処理をしない
		}
		else if (ctl.GetPadTrigger(0, PAD_INPUT_1))
		{
			switch (selectFlag)
			{
			case 0:
				nextMode = NEXT_title;
				break;
			case 1:
				moveFlag = true;
				break;
			case 2:
				nextMode = NEXT_player1;
				break;
			case 3:
				nextMode = NEXT_player2;
				break;
			case 4:
				nextMode = NEXT_player3;
				break;
			case 5:
				nextMode = NEXT_player4;
				break;
			default:
				break;
			}
		}
	}
	else
	{
		// 音
		if (taisenMove == VECTOR2(0, 0))
		{
			PlaySoundMem(lpSoundMng.GetID("se/check.wav"), DX_PLAYTYPE_BACK, true);
		}
		// ----------

		if (ctl.GetPadTrigger(0, PAD_INPUT_1))
		{
			taisenMove = { (CENTER_POS_X - 68) , (CENTER_POS_Y - 293) };
			taisenTxtMove = (CENTER_TXT_POS - CENTER_POS_Y);
			selectFlag = 3;
			moveFlag = false;
		}
		else
		{
			taisenMove.x += 4;
			if ((68 + taisenMove.x) > CENTER_POS_X)
			{
				taisenMove.x = (CENTER_POS_X - 68);
			}

			taisenMove.y--;
			if ((293 + taisenMove.y) < CENTER_POS_Y)
			{
				taisenMove.y = (CENTER_POS_Y - 293);
			}

			if (taisenMove == VECTOR2{ CENTER_POS_X - 68, CENTER_POS_Y - 293 })
			{
				taisenTxtMove -= 2;
				if ((CENTER_POS_Y + taisenTxtMove) < CENTER_TXT_POS)
				{
					taisenTxtMove = (CENTER_TXT_POS - CENTER_POS_Y);
				}

				if (taisenTxtMove == (CENTER_TXT_POS - CENTER_POS_Y))
				{
					selectFlag = 3;
					moveFlag = false;
				}
			}
		}
	}

	// 音
	if (cursorMoveFlg)
	{
		PlaySoundMem(lpSoundMng.GetID("se/select.wav"), DX_PLAYTYPE_BACK, true);
	}
	//----------

	if (nextMode != -1)
	{
		// ｼｰﾝ切替が初期値から変更された、つまりｼｰﾝ移行する
		SceneChangeFlag = true;
		inputCancelFlag = true;
	}

	if (SceneChangeFlag)
	{
		if (lpSceneChange.FadeOut(DEF_SCENECHANGE_SPEED, DEF_SCENECHANGE_WAIT, true))
		{
			// 音
			ChangeVolumeSoundMem(255, lpSoundMng.GetID("bgm/title.mp3"));
			StopSoundMem(lpSoundMng.GetID("bgm/title.mp3"));
			StopSoundMem(lpSoundMng.GetID("se/check.wav"));
			StopSoundMem(lpSoundMng.GetID("se/select.wav"));
			//-----------

			if (nextMode == NEXT_title)
			{
				return std::make_unique<TitleScene>();
			}

			if (NEXT_player1 <= nextMode && nextMode < NEXT_max)
			{
				lpSceneMng.SetPlayerCnt(nextMode);
				return std::make_unique<CharacterSelectScene>();
			}
		}
		else
		{
			// 音
			if (SceneChangeFlag != OldSceneChangeFlag)
			{
				if (nextMode == NEXT_title)
				{
					PlaySoundMem(lpSoundMng.GetID("se/select.wav"), DX_PLAYTYPE_BACK, true);
				}
				else
				{
					PlaySoundMem(lpSoundMng.GetID("se/check.wav"), DX_PLAYTYPE_BACK, true);
				}
			}
			// --------
		}
	}

	// 描画処理
	MainMenuDraw();

	//現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す
	return std::move(own);
}

int MainMenuScene::Init(void)
{
	joypadCnt = GetJoypadNum();
	moveFlag = false;
	selectFlag = 1;
	if (joypadCnt < 2)
	{
		selectFlag = 2;
	}
	taisenMove	 = { 0, 0 };
	taisenTxtMove = 0;
	lpSceneMng.SetTutorialFlag(false);
	nextMode	= -1;
	AlwaysCnt	= 0;
	inputCancelFlag = false;
	
	for (int i = 0; i < 2; i++)
	{
		bgPosX1[i] = 0;
		bgPosX2[i] = 2700 * (1 - (i * 2));
	}

	return 0;
}

bool MainMenuScene::MainMenuDraw(void)
{
	ClsDrawScreen();		// 画面消去

	for (int i = 0; i < 2; i++)
	{
		bgPosX1[i] -= 1 - (i * 2);
		bgPosX2[i] -= 1 - (i * 2);
	}

	// 背景
	DrawGraph(0, 0, lpImageMng.GetID("image/menu_back_tile.png")[0], true);
	
	for (int j = 0; j < 2; j++)
	{
		DrawGraph(bgPosX1[BACK_DIR_LEFT], j * 600, lpImageMng.GetID("image/menu_back.png", VECTOR2(2627, 221), VECTOR2(1, 2))[(AlwaysCnt + 60) / 60 % 2], true);
		DrawGraph(bgPosX2[BACK_DIR_LEFT], j * 600, lpImageMng.GetID("image/menu_back.png", VECTOR2(2627, 221), VECTOR2(1, 2))[(AlwaysCnt + 60) / 60 % 2], true);
	}
	DrawTurnGraph(bgPosX1[BACK_DIR_RIGHT], 290, lpImageMng.GetID("image/menu_back.png", VECTOR2(2627, 221), VECTOR2(1, 2))[(AlwaysCnt + 60) / 60 % 2], true);
	DrawTurnGraph(bgPosX2[BACK_DIR_RIGHT], 290, lpImageMng.GetID("image/menu_back.png", VECTOR2(2627, 221), VECTOR2(1, 2))[(AlwaysCnt + 60) / 60 % 2], true);
	
	if (bgPosX1[0] < -2700)
	{
		bgPosX1[0] = 0;
	}
	if (bgPosX2[0] < -2700)
	{
		bgPosX2[0] = 0;
	}

	if (bgPosX1[1] > 2700)
	{
		bgPosX1[1] = 0;
	}
	if (bgPosX2[1] > 2700)
	{
		bgPosX2[1] = 0;
	}


	// ﾎﾞﾀﾝ系
	// 戻る
	DrawGraph(22, 22, lpImageMng.GetID("image/modoru_button.png", VECTOR2(200, 200), VECTOR2(2, 1))[(selectFlag == 0)], true);		// 左上

	// 文字
	// ﾓｰﾄﾞｾﾚｸﾄ
	SetFontSize(135);
	DrawString(370, 80, "モードセレクト", 0xe5a22d);

	// ﾀｲﾄﾙへ戻る
	SetFontSize(45);
	DrawString(10, 10, "タイトルへ戻る", 0xf38080);

	// ﾁｭｰﾄﾘｱﾙ
	DrawGraph(766, 293, lpImageMng.GetID("image/tutorial_button.png", VECTOR2(600, 400), VECTOR2(2, 1))[(selectFlag == 2)], true);		// 右

	// 対戦ﾓｰﾄﾞ
	if ((selectFlag >= 0) && (selectFlag < 3))
	{
		DrawGraph(68 + taisenMove.x, 293 + taisenMove.y, lpImageMng.GetID("image/taisenmode_button.png", VECTOR2(600, 400), VECTOR2(2, 1))[(selectFlag == 1)], true);		// 左

		if (joypadCnt < 2)
		{
			DrawGraph(68, 293, lpImageMng.GetID("image/taisenmode_error.png")[0], true);
		}
	}
	else
	{
		DrawGraph(CENTER_POS_X, CENTER_POS_Y, lpImageMng.GetID("image/player_select_button.png", VECTOR2(600, 400), VECTOR2(3, 1))[(selectFlag - 3)], true);		// 中央

		if (joypadCnt < 4)
		{
			DrawGraph(CENTER_POS_X + 400, CENTER_POS_Y + 150, lpImageMng.GetID("image/player_select_error.png")[0], true);

			if (joypadCnt < 3)
			{
				DrawGraph(CENTER_POS_X + 225, CENTER_POS_Y + 150, lpImageMng.GetID("image/player_select_error.png")[0], true);
			}
		}
	}
	DrawGraph(168 + taisenMove.x, 443 + taisenMove.y + taisenTxtMove, lpImageMng.GetID("image/taisen_mode_txt.png")[0], true);

	ScreenFlip();		// ｹﾞｰﾑﾙｰﾌﾟの最後に必ず必要

	return true;
}
