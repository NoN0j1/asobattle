#pragma once
#include<memory>

class BaseScene;
class GameCtl;

using unique_Base = std::unique_ptr<BaseScene>;		//BaseSceneｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀの型

class BaseScene		//各ｼｰﾝｸﾗｽの基底となるクラス
{
public:
	virtual ~BaseScene();
	//ｼｰﾝ中の処理　純粋仮想関数
	virtual unique_Base Update(unique_Base own, const GameCtl &ctl) = 0;
private:
	//初期化　純粋仮想関数
	virtual int Init(void) = 0;
protected:
	bool SceneChangeFlag	= false;
	bool OldSceneChangeFlag = false;
	int AlwaysCnt;
};

