#include "DxLib.h"
#include "SoundMng.h"
#include "CrossProduct_2D.h"
#include "SpecialAttack_Bear.h"

#define WALL_SIZE_Y	(135)		//�ǂ̏c����

#define BEAR_SIZE_X	(320)
#define BEAR_SIZE_Y	(444)

#define SPEED		(12)

#define DETH_CNT_MAX	(30)

struct CheckWall
{
	bool operator()(VECTOR2 & pos, VECTOR2 size, int depth)
	{
		if (depth >= WALL_SIZE_Y)
		{
			return true;
		}

		VECTOR2 tmpPos(pos.x, depth - size.y);

		VECTOR2 startPos_L(0, 135);
		VECTOR2 startPos_R(1400, 135);

		if (CrossProduct_2D()(startPos_L, VECTOR2(tmpPos.x, tmpPos.y + size.y), VECTOR2(startPos_L.x + 135, startPos_L.y - 135)) > 0)
		{
			int a1 = 135 / -135;
			int a3 = 0;
			pos.x = (a1 * 135 - 0 - a3 * pos.x + depth) / (a1 - a3);
			return false;
		}

		if (CrossProduct_2D()(startPos_R, tmpPos + size * 15/10, startPos_R - 135) < 0)
		{
			int a1 = 135 / 135;
			int a3 = 0;
			pos.x = (a1 * (1400 - 135) - 0 - a3 * pos.x + depth) / (a1 - a3) - size.x;
			return false;
		}
		return true;
	}
};

SpecialAttack_Bear::SpecialAttack_Bear(VECTOR2 drawOffset, VECTOR2 pos, int popNo, int depth) :Obj(drawOffset)
{
	init("image/teddy.png", VECTOR2(BEAR_SIZE_X, BEAR_SIZE_Y), VECTOR2(1, 1));

	dethFlag = false;

	this->pos.x = pos.x - (divSize.x / 3);
	this->pos.y = -drawOffset.y +  -divSize.y * 15/10;
	this->depth = depth;

	CheckWall()(this->pos, divSize * 15/10, depth);

	playerNo = popNo;

	attackPos = this->pos;
	attackSize = divSize * 15 / 10;

	attackRangeFront = 20;
	attackRangeBack = 200;

	power = 40;

	dethCnt = DETH_CNT_MAX;
}

SpecialAttack_Bear::~SpecialAttack_Bear()
{
}

bool SpecialAttack_Bear::CheckDeth(void)
{
	if (dethCnt < 0)
	{
		dethFlag = true;
	}
	return dethFlag;
}

bool SpecialAttack_Bear::CheckObjType(OBJ_TYPE type)
{
	return (OBJ_TYPE_SP_ATTACK == type);
}

bool SpecialAttack_Bear::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth)
{
	if (this->depth + attackRangeFront >= depth && this->depth - attackRangeBack <= depth)
	{
		if (this->attackPos.x < defensePos.x + defenseSize.x
			&&this->attackPos.x + attackSize.x > defensePos.x
			&&this->attackPos.y < defensePos.y + defenseSize.y
			&&this->attackPos.y + attackSize.y > defensePos.y)
		{
			return true;
		}
	}
	return false;
}

int SpecialAttack_Bear::GetPlayerNo(void)
{
	return playerNo;
}

void SpecialAttack_Bear::Draw(void)
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	DrawOval(drawOffset.x + pos.x + ((divSize.x * 15 / 10) / 2), drawOffset.y + depth - 5, divSize.x / 2, 4, 0x000000, true);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	Obj::Draw();
}

void SpecialAttack_Bear::SetMove(const GameCtl & ctl, weekListObj objList)
{
	if (pos.y >= depth - divSize.y * 15/10)
	{
		if (dethCnt == DETH_CNT_MAX)
		{
			if (CheckSoundMem(lpSoundMng.GetID("se/4goEX02.wav")) == 0)
			{
				PlaySoundMem(lpSoundMng.GetID("se/4goEX02.wav"), DX_PLAYTYPE_BACK, true);

			}
		}
		dethCnt--;
	}
	else
	{
		pos.y += SPEED;
	}

	attackPos = pos;
}