#include "Dxlib.h"
#include "SoundMng.h"
#include "SpecialAttack_Misima.h"

SpecialAttack_Misima::SpecialAttack_Misima(VECTOR2 drawOffset, int popNo) :SpecialAttack_Kusanagi(drawOffset, popNo)
{
	init("image/misima.png", VECTOR2(132, 132), VECTOR2(1, 1));

	pos.x = 200 + 995 - divSize.x;
	pos.y = 335 - divSize.y;

	attackPos = pos;

	depth = pos.y + divSize.y;

	leftFlag = true;

	leftCnt = 0;
}

SpecialAttack_Misima::~SpecialAttack_Misima()
{

}

void SpecialAttack_Misima::SetMove(const GameCtl & ctl, weekListObj objList)
{
	if (CheckSoundMem(lpSoundMng.GetID("se/6goEX02.wav")) == 0)
	{
		PlaySoundMem(lpSoundMng.GetID("se/6goEX02.wav"), DX_PLAYTYPE_LOOP, true);
	}
	if (leftFlag)
	{
		pos.x -= speed_X;
	}
	else
	{
		pos.y -= speed_Y;
		pos.x += speed_X;
	}

	if (pos.y + divSize.y <= 0)
	{
		pos.y = -divSize.y;
	}

	attackPos = pos;

	depth = pos.y + divSize.y;

	if (pos.x <= 200)
	{
		leftFlag = false;
		leftCnt++;
	}
	if (pos.x >= 200 + MOVE_SCOPE_X)
	{
		leftFlag = true;
	}

	if (leftCnt >= 2)
	{
		StopSoundMem(lpSoundMng.GetID("se/6goEX02.wav"));
		dethFlag = true;
	}
}
