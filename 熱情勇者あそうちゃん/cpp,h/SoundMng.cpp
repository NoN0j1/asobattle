#include <string>
#include "DxLib.h"
#include "SoundMng.h"

const int & SoundMng::GetID(std::string f_name)
{
	if (soundId.find(f_name) == soundId.end())
	{
		soundId[f_name] = LoadSoundMem(f_name.c_str());
	}
	return soundId[f_name];
}

SoundMng::SoundMng()
{
	_RPT0(_CRT_WARN, "SoundMng�׽��ݽ�ݽ\n");
}

SoundMng::~SoundMng()
{
	_RPT0(_CRT_WARN, "SoundMng�׽���폜\n");
}
