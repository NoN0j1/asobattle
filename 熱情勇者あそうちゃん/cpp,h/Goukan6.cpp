#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "SpecialAttack_Kusanagi.h"
#include "SpecialAttack_Misima.h"
#include "Goukan6.h"

#define GOUKAN6_HP_MAX				(120)	//HP�̍ő�l

#define GOUKAN6_DEF_SPEED			(6)		//��̫�Ă̈ړ���߰��

#define GOUKAN6_DEF_POWER			(10)	//��̫�Ă̍U����

#define GOUKAN6_SPECIAL_CNT_MAX		(70)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(368 / 2)		//�K�E�Z�̏c����
#define ANIM_SIZE_Y					(184)			//�K�E�Z�̏c����
#define ANIM_CNT					(2)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(30)			//�K�E�Z�̱�Ұ��݊Ԋu

Goukan6::Goukan6(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN6_HP_MAX;
	power = GOUKAN6_DEF_POWER;
	speed = GOUKAN6_DEF_SPEED;
	specialCntMax = GOUKAN6_SPECIAL_CNT_MAX;
	init("image/goukan6.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan6_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(2, 1));

	lpImageMng.GetID("image/cutin/6gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	_RPT0(_CRT_WARN, "6���ق�ݽ�ݽ\n");
}

Goukan6::~Goukan6()
{
	_RPT0(_CRT_WARN, "6���ق��폜\n");
}

int Goukan6::GetHPMax(void)
{
	return GOUKAN6_HP_MAX;
}

bool Goukan6::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	if (specialTimeCnt > CUTIN_END_FRAME )
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/6goEX01.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/6goEX01.wav"), DX_PLAYTYPE_BACK, true);
		}
	}

	if (specialTimeCnt == (ANIM_FRAME * (ANIM_CNT - 1) + CUTIN_END_FRAME) )
	{
		AddObjList()(objList, std::make_shared<SpecialAttack_Kusanagi>(drawOffset, playerNo));
		AddObjList()(objList, std::make_shared<SpecialAttack_Misima>(drawOffset, playerNo));
	}

	if (specialTimeCnt > (60 * 2 + CUTIN_END_FRAME) )
	{
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		Character::move = &Goukan6::Normal;
	}

	specialTimeCnt++;

	return true;
}

void Goukan6::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	int animCnt = specialTimeCnt / ANIM_FRAME;
	if (animCnt >= ANIM_CNT - 1)
	{
		animCnt = ANIM_CNT - 1;
	}
	DrawExtendGraph(
		drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 60,
		drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
		drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15/10) - 60,
		drawOffset.y + depth,
		lpImageMng.GetID("image/goukan6_Special.png")[animCnt], true);


	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/6gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/6gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan6::GetDefPower(void)
{
	return GOUKAN6_DEF_POWER;
}
