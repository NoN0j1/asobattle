#pragma once
#include<list>
#include<array>
#include<vector>
#include"ObjList.h"
#include"VECTOR2.h"
#include"BaseScene.h"

#define GAME_SCREEN_SIZE_X (1400)	//ｹﾞｰﾑ画面の横ｻｲｽﾞ
#define GAME_SCREEN_SIZE_Y (335)	//ｹﾞｰﾑ画面の縦ｻｲｽﾞ

#define UI_SIZE_X (250)
#define UI_SIZE_Y (150)

#define BG_SIZE_Y (265)

#define GAME_SCREEN_X (20)								//ｹﾞｰﾑ画面のｵﾌｾｯﾄの横ｻｲｽﾞ
#define GAME_SCREEN_Y (UI_SIZE_Y + BG_SIZE_Y)			//ｹﾞｰﾑ画面のｵﾌｾｯﾄの縦ｻｲｽﾞ

class Character;

class GameScene :
	public BaseScene
{
public:
	GameScene();
	~GameScene();
	//ｹﾞｰﾑﾓｰﾄﾞ時の処理　引数:現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀ,GameCtlｸﾗｽの情報　返り値として次のﾌﾚｰﾑ時のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す
	unique_Base Update(unique_Base own, const GameCtl &ctl);
private:
	//初期化
	int Init(void);
	
	//音声の全停止
	void StopSound(void);

	//描画処理
	bool GameDraw(void);

	// 落下するかのチェック　引数:座標,ｻｲｽﾞ
	bool CheckFall(VECTOR2 pos, VECTOR2 size);

	sharedListObj objList;			//Objｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀ型のﾘｽﾄのｼｪｱｰﾄﾞﾎﾟｲﾝﾀ

	int timeCnt;					//残り時間のｶｳﾝﾄ

	bool timeCntFlag;

	int timeFont;					//ﾀｲﾏｰのﾌｫﾝﾄ
	int timeFont_Big;				//大きいﾀｲﾏｰのﾌｫﾝﾄ

	int textFont;					//文字のﾌｫﾝﾄ
	int textFont_Big;				//大きい文字のﾌｫﾝﾄ
	
	int rankCnt;					//順位のｶｳﾝﾄ

	bool startFlag;					//ｽﾀｰﾄして良いかのﾌﾗｸﾞ

	int startCnt;

	int explanationTextCnt;
	int explanationGraphCnt;

	int bgImage;					//背景画像のﾊﾝﾄﾞﾙ
};

