#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "Goukan2.h"

#define GOUKAN2_HP_MAX				(100)	//HP�̍ő�l

#define GOUKAN2_DEF_SPEED			(6)		//��̫�Ă̈ړ���߰��

#define GOUKAN2_DEF_POWER			(9)		//��̫�Ă̍U����

#define GOUKAN2_SPECIAL_CNT_MAX		(90)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_Y					(184)			//�K�E�Z�̱�Ұ��ݏc����
#define ANIM_CNT					(5)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(10)			//�K�E�Z�̱�Ұ����ڰ�

#define BEAM_SIZE_X					(1000 / 10)		//�ްт̉�����
#define BEAM_SIZE_Y					(400 / 4)		//�ްт̏c����

#define EFFECT_SIZE_X				(1000 / 10)		//�̪�Ẳ�����
#define EFFECT_CNT					(32)			//�̪�Ă̱�Ұ��ݐ�

#define CNT_MAX						(30 * 3)		//�ްт̔��ˎ���

#define SP_POWER					(30)			//�K�E�Z�̍U����

Goukan2::Goukan2(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN2_HP_MAX;
	power = GOUKAN2_DEF_POWER;
	speed = GOUKAN2_DEF_SPEED;
	specialCntMax = GOUKAN2_SPECIAL_CNT_MAX;
	init("image/goukan2.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();
	SoundVol = 255;

	lpImageMng.GetID("image/goukan2_special.png", VECTOR2(184, ANIM_SIZE_Y), VECTOR2(5, 1));

	lpImageMng.GetID("image/effect/2gou_exAttack_00.png", VECTOR2(EFFECT_SIZE_X, 400 / 4), VECTOR2(10, 4));

	lpImageMng.GetID("image/cutin/2gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	specialTimeCnt = 0;

	_RPT0(_CRT_WARN, "2���ق�ݽ�ݽ\n");
}

Goukan2::~Goukan2()
{
	_RPT0(_CRT_WARN, "2���ق��폜\n");
}

int Goukan2::GetHPMax(void)
{
	return GOUKAN2_HP_MAX;
}

bool Goukan2::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	power = SP_POWER;
	
	if (specialTimeCnt > CUTIN_END_FRAME)
	{
		if (specialTimeCnt >= (ANIM_CNT * ANIM_FRAME) + CUTIN_END_FRAME/4)
		{
			// �ްє���
			//��
			if (CheckSoundMem(lpSoundMng.GetID("se/charge.mp3")) == 1)
			{
				StopSoundMem(lpSoundMng.GetID("se/charge.mp3"));
			}
			if (CheckSoundMem(lpSoundMng.GetID("se/biim.mp3")) == 0)
			{
				PlaySoundMem(lpSoundMng.GetID("se/biim.mp3"), DX_PLAYTYPE_BACK, true);
			}
			else
			{
				if (specialTimeCnt > (ANIM_CNT * ANIM_FRAME) + CNT_MAX / 2)
				{
					SoundVol -= 5;
					ChangeVolumeSoundMem(SoundVol, lpSoundMng.GetID("se/biim.mp3"));
				}
				//----------
			}
			attackFlag = true;
			attackSize.x += BEAM_SIZE_X;
			attackSize.y = BEAM_SIZE_Y;
		}
		else
		{
			// �ްє��ˑO
			//��
			SoundVol = 255;
			ChangeVolumeSoundMem(SoundVol, lpSoundMng.GetID("se/biim.mp3"));
			if (CheckSoundMem(lpSoundMng.GetID("se/charge.mp3")) == 0)
			{
				PlaySoundMem(lpSoundMng.GetID("se/charge.mp3"), DX_PLAYTYPE_BACK, true);
			}
			//----------
		}
	}
	if (turnFlag)
	{
		attackPos.x = defensePos.x - attackSize.x;
		attackPos.y = defensePos.y;
	}
	else
	{
		attackPos.x = defensePos.x + defenseSize.x;
		attackPos.y = defensePos.y;
	}

	if (specialTimeCnt >= (ANIM_CNT * ANIM_FRAME) + CNT_MAX)
	{
		//�K�E�Z�I�����ɍs����
		//��
		StopSoundMem(lpSoundMng.GetID("se/biim.mp3"));
		//----------

		damageFlag = true;
		specialFlag = false;
		power = GOUKAN2_DEF_POWER;
		Character::move = &Goukan2::Normal;
	}

	specialTimeCnt++;

	return true;
}

void Goukan2::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	
	int charAnimCnt = specialTimeCnt / ANIM_FRAME;
	if (charAnimCnt >= ANIM_CNT)
	{
		charAnimCnt = ANIM_CNT - 1;
	}

	int effectCnt = specialTimeCnt;
	if (effectCnt >= EFFECT_CNT)
	{
		effectCnt = EFFECT_CNT - 1;
	}

	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + pos.x - 60,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
			drawOffset.x + pos.x + ANIM_SIZE_Y * 15/10 - 60,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan2_special.png")[charAnimCnt], true);

		DrawExtendGraph(drawOffset.x + attackPos.x + (BEAM_SIZE_X / 2), drawOffset.y + attackPos.y, drawOffset.x + attackPos.x + (BEAM_SIZE_X / 2) + attackSize.x + 1, drawOffset.y + attackPos.y + attackSize.y + 1, lpImageMng.GetID("image/effect/beam.png")[0], true);
		if (CUTIN_END_FRAME/4 < specialTimeCnt)
		{
			DrawGraph(drawOffset.x + attackPos.x, drawOffset.y + attackPos.y, lpImageMng.GetID("image/effect/2gou_exAttack_00.png")[effectCnt], true);
		}
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + pos.x + ANIM_SIZE_Y * 15/10 - 30,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
			drawOffset.x + pos.x - 30,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan2_special.png")[charAnimCnt], true);

		DrawExtendGraph(drawOffset.x + defensePos.x - (BEAM_SIZE_X / 2), drawOffset.y + defensePos.y, drawOffset.x + defensePos.x - ((BEAM_SIZE_X / 2) + attackSize.x + 1), drawOffset.y + defensePos.y + attackSize.y + 1, lpImageMng.GetID("image/effect/beam.png")[0], true);
		if (CUTIN_END_FRAME/4 < specialTimeCnt)
		{
			DrawGraph(drawOffset.x + defensePos.x - EFFECT_SIZE_X, drawOffset.y + attackPos.y, lpImageMng.GetID("image/effect/2gou_exAttack_00.png")[effectCnt], true);
		}
	}
	
	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/2gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/2gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan2::GetDefPower(void)
{
	return GOUKAN2_DEF_POWER;
}
