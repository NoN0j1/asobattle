#include "DxLib.h"
#include "ImageMng.h"
#include "SpecialAttack_Denture.h"
#include "Goukan1.h"

#define GOUKAN1_HP_MAX				(80)	//HP�̍ő�l

#define GOUKAN1_DEF_SPEED			(6)		//��̫�Ă̈ړ���߰��

#define GOUKAN1_DEF_POWER			(8)		//��̫�Ă̍U����

#define GOUKAN1_SPECIAL_CNT_MAX		(50)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE					(276)	//�K�E�Z�̱�Ұ��݉摜����
#define ANIM_CNT					(4)		//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(30)	//�K�E�Z�̱�Ұ����ڰ�

Goukan1::Goukan1(VECTOR2 drawOffset, int popNo, bool aiFlag):Character(drawOffset, popNo,aiFlag)
{
	HP = GOUKAN1_HP_MAX;
	power = GOUKAN1_DEF_POWER;
	speed = GOUKAN1_DEF_SPEED;
	specialCntMax = GOUKAN1_SPECIAL_CNT_MAX;
	init("image/goukan1.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	
	initAnim();

	lpImageMng.GetID("image/goukan1_special.png", VECTOR2(184, ANIM_SIZE), VECTOR2(4, 1));

	lpImageMng.GetID("image/cutin/1gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	specialTimeCnt = 0;

	_RPT0(_CRT_WARN, "1���ق�ݽ�ݽ\n");
}

Goukan1::~Goukan1()
{
	_RPT0(_CRT_WARN, "1���ق��폜\n");
}

int Goukan1::GetHPMax(void)
{
	return GOUKAN1_HP_MAX;
}

bool Goukan1::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	if (specialTimeCnt + CUTIN_END_FRAME/2 == ((ANIM_CNT - 1) * ANIM_FRAME))
	{
		AddObjList()(objList, std::make_shared<SpecialAttack_Denture>(drawOffset, pos, divSize, playerNo, depth, turnFlag));
	}


	if (specialTimeCnt + CUTIN_END_FRAME >= (ANIM_CNT * ANIM_FRAME))
	{
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		Character::move = &Goukan1::Normal;
		return true;
	}

	specialTimeCnt++;
	return true;
}

void Goukan1::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}
	
	
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + pos.x - 25, 
			drawOffset.y + (depth - ANIM_SIZE),
			drawOffset.x + pos.x - 25 + ANIM_SIZE,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan1_special.png")[(specialTimeCnt / ANIM_FRAME) % ANIM_CNT], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + pos.x - 25 + ANIM_SIZE,
			drawOffset.y + (depth - ANIM_SIZE),
			drawOffset.x + pos.x - 25,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan1_special.png")[(specialTimeCnt / ANIM_FRAME) % ANIM_CNT], true);
	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/1gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/1gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan1::GetDefPower(void)
{
	return GOUKAN1_DEF_POWER;
}
