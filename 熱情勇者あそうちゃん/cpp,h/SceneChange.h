#pragma once

#define lpSceneChange SceneChange::GetInstance()

#define DEF_SCENECHANGE_SPEED	(5)
#define DEF_SCENECHANGE_WAIT	(60)

class SceneChange
{
public:
	static SceneChange &GetInstance(void)
	{
		static SceneChange s_Instance;				//SceneChangeｸﾗｽのｱﾄﾞﾚｽを格納する変数
		return s_Instance;
	}
	SceneChange();
	~SceneChange();

	// ﾌｪｰﾄﾞｲﾝ [引数]ﾌｪｰﾄﾞｲﾝ処理終了後の待機時間{ﾌﾚｰﾑ数}, 処理終了後に明るくするか{<true>:明るくする,<false>:暗くする}	
	// [返り値]<true>:画面処理が完了,<false>:画面処理がまだ
	bool FadeIn(int wait, bool nextbright);

	// ﾌｪｰﾄﾞｲﾝ [引数]暗転の早さ{輝度の加算値}, ﾌｪｰﾄﾞｲﾝ処理終了後の待機時間{ﾌﾚｰﾑ数}, 処理終了後に明るくするか{<true>:明るくする,<false>:暗くする}
	// [返り値]<true>:画面処理が完了,<false>:画面処理がまだ
	bool FadeIn(int speed,int wait, bool nextbright);

	// ﾌｪｰﾄﾞｱｳﾄ [引数]ﾌｪｰﾄﾞｱｳﾄ処理終了後の待機時間{ﾌﾚｰﾑ数}, 処理終了後に明るくするか{<true>:明るくする,<false>:暗くする}
	// [返り値]<true>:画面処理が完了,<false>:画面処理がまだ
	bool FadeOut(int wait, bool nextbright);

	// ﾌｪｰﾄﾞｱｳﾄ [引数]暗転の早さ{輝度の減算値}, ﾌｪｰﾄﾞｱｳﾄ処理終了後の待機時間{ﾌﾚｰﾑ数}, 処理終了後に明るくするか{<true>:明るくする,<false>:暗くする}
	// [返り値]<true>:画面処理が完了,<false>:画面処理がまだ
	bool FadeOut(int speed,int wait, bool nextbright);

	// 現在の輝度値を返す
	const int GetBright();

private:
	void Init();
	unsigned int CompleteCnt;
	unsigned int ChangeCnt;

	// true = 明るい,false = 暗い
	bool SceneBrightFlag;
	bool ChangeEndFlag;
	bool InitFlag;

	//  true = 明るくする,false = 真っ黒にする
	void SetBrightDefault(bool darkness);

	int bright;
};

