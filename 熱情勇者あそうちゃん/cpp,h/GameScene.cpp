#include"DxLib.h"
#include"SceneMng.h"
#include"ImageMng.h"
#include"SoundMng.h"
#include"GameCtl.h"
#include"CrossProduct_2D.h"
#include"Obj.h"
#include"Character.h"
#include"Goukan1.h"
#include"Goukan2.h"
#include"Goukan3.h"
#include"Goukan4.h"
#include"Goukan5.h"
#include"Goukan6.h"
#include"Goukan7.h"
#include"Goukan8.h"
#include"Goukan9.h"
#include"Goukan10.h"
#include"CHARA_ID.h"
#include"ResultScene.h"
#include"MainMenuScene.h"
#include"GameScene.h"

#define HOLE_SIZE_Y		(50)

#define TIME_CNT_MAX	(60 * 60 * 1)

#define TIME_FONT_SIZE	(100)

#define TEXT_SIZE	(50)

#define START_CNT_MAX	(60 * 3 + 30)

GameScene::GameScene()
{
	Init();
	_RPT0(_CRT_WARN, "GameSceneｸﾗｽをｲﾝｽﾀﾝｽ\n");
}


GameScene::~GameScene()
{
	DeleteGraph(bgImage);
	DeleteFontToHandle(timeFont);
	DeleteFontToHandle(timeFont_Big);
	DeleteFontToHandle(textFont);
	DeleteFontToHandle(textFont_Big);
	_RPT0(_CRT_WARN, "GameSceneｸﾗｽを削除\n");
}

unique_Base GameScene::Update(unique_Base own, const GameCtl & ctl)
{
	if (!startFlag)
	{
		for (int p = 0; p < lpSceneMng.GetPlayerCnt(); p++)
		{
			if (ctl.GetPadTrigger(p, PAD_INPUT_8))
			{
				explanationGraphCnt++;
			}
		}
		if (explanationGraphCnt > 1)
		{
			startFlag = true;
		}
	}
	else
	{
		// 音
		if (CheckSoundMem(lpSoundMng.GetID("bgm/battle.mp3")) == 0)
		{
			if ((CheckSoundMem(lpSoundMng.GetID("bgm/hokuto.mp3")) == 0) &&
				(CheckSoundMem(lpSoundMng.GetID("bgm/10goEX01.wav")) == 0)
				)
			{
				// 再生されていない場合
				PlaySoundMem(lpSoundMng.GetID("bgm/battle.mp3"), DX_PLAYTYPE_LOOP, true);
			}

		}
		// ---------

		if (startCnt < 0)
		{
#ifdef _DEBUG
			if (ctl.GetKeyCtl(KEY_TYPE_NOW)[KEY_INPUT_F5])
			{
				Init();
			}

			if (ctl.GetKeyCtl(KEY_TYPE_NOW)[KEY_INPUT_0] & (~ctl.GetKeyCtl(KEY_TYPE_OLD)[KEY_INPUT_0]))
			{
				timeCnt -= 60;
			}

			if (ctl.GetKeyCtl(KEY_TYPE_NOW)[KEY_INPUT_1])
			{
				timeCnt = 9 * 60;
			}

			if (ctl.GetKeyCtl(KEY_TYPE_NOW)[KEY_INPUT_2])
			{
				timeCnt = 99 * 60;
			}

			if (ctl.GetKeyCtl(KEY_TYPE_NOW)[KEY_INPUT_3])
			{
				timeCnt = 999 * 60;
			}
#endif
			sharedObjList charList((*objList).size());
			auto last_itr = std::remove_copy_if((*objList).begin(), (*objList).end(), charList.begin(), [](sharedObj obj) {return !((*obj).CheckObjType(OBJ_TYPE_CHAR)); });

			if (lpSceneMng.GetTutorialFlag())
			{
				if (ctl.GetPadTrigger(0, PAD_INPUT_8))
				{
					//ﾒｲﾝﾒﾆｭｰに戻る
					StopSound();
					return std::make_unique<MainMenuScene>();
				}

				for (auto character = charList.begin(); character != last_itr; character++)
				{
					//HP回復
					if ((*character)->GetHP() <= 0)
					{
						(*character)->SetHP((*character)->GetHPMax());
					}
					if ((*character)->GetPos().y > GAME_SCREEN_SIZE_Y + HOLE_SIZE_Y)
					{
						// 落ちて画面外に出た
						Init();
					}
				}
			}

			int tmpRankCnt = rankCnt;

			(*charList.begin())->SetSPCutinFlag(false);

			if (timeCnt < 59)
			{
				(*objList).remove_if([](sharedObj obj) {return !((*obj).CheckObjType(OBJ_TYPE_CHAR)); });
				(*objList).sort([](sharedObj obj1, sharedObj obj2) {return (*obj1).GetHP() > (*obj2).GetHP(); });

				tmpRankCnt = 1;
				int tmpHP = -1;

				for (auto obj : (*objList))
				{
					if (lpSceneMng.GetRank((*obj).GetPlayerNo()) == 0)
					{
						if (tmpHP < (*obj).GetHP())
						{
							rankCnt = tmpRankCnt;
							tmpHP = (*obj).GetHP();
							lpSceneMng.SetRank((*obj).GetPlayerNo(), rankCnt);
							tmpRankCnt++;
						}
						else if (tmpHP == (*obj).GetHP())
						{
							lpSceneMng.SetRank((*obj).GetPlayerNo(), rankCnt);
							tmpRankCnt++;
						}
						else
						{
							rankCnt = tmpRankCnt;
							tmpHP = (*obj).GetHP();
							lpSceneMng.SetRank((*obj).GetPlayerNo(), rankCnt);
							tmpRankCnt++;
						}
					}
				}

				//ﾘｻﾞﾙﾄｼｰﾝに移行

				//音
				StopSoundMem(lpSoundMng.GetID("bgm/battle.mp3"));

				if (CheckSoundMem(lpSoundMng.GetID("se/KO.mp3")) == 0)
				{
					StopSound();
					return std::make_unique<ResultScene>();
				}
				//----------
			}

			for (auto character = charList.begin(); character != last_itr; character++)
			{
				if (lpSceneMng.GetRank((*character)->GetPlayerNo()) == 0)
				{
					if ((*character)->GetAnim() == "死亡")
					{
						PlaySoundMem(lpSoundMng.GetID("se/KO.mp3"), DX_PLAYTYPE_BACK, true);
						lpSceneMng.SetRank((*character)->GetPlayerNo(), rankCnt);
						tmpRankCnt--;
					}
				}
			}

			rankCnt = tmpRankCnt;

			if (rankCnt <= 1)
			{
				timeCntFlag = false;
				for (auto character = charList.begin(); character != last_itr; character++)
				{
					if (lpSceneMng.GetRank((*character)->GetPlayerNo()) == 0)
					{
						lpSceneMng.SetRank((*character)->GetPlayerNo(), rankCnt);
					}
				}
				//ﾘｻﾞﾙﾄｼｰﾝに移行

				// 音
				StopSoundMem(lpSoundMng.GetID("bgm/battle.mp3"));

				if (CheckSoundMem(lpSoundMng.GetID("se/KO.mp3")) == 0)
				{
					StopSound();
					return std::make_unique<ResultScene>();
				}
				//----------
			}

			//移動処理
			for (auto itr = objList->begin(); itr != objList->end(); itr++)
			{
				if (CheckFall((*itr)->GetDefensePos(), (*itr)->GetDefenseSize()))
				{
					if ((*itr)->GetFallFlag() == false)
					{
						// 誰かが穴に落ちた
						(*itr)->SetFallFlag(true);

						//音
						PlaySoundMem(lpSoundMng.GetID("se/down.mp3"), DX_PLAYTYPE_BACK, true);
						// ----------
					}
					else
					{
						if ((*itr)->GetPos().y > GAME_SCREEN_SIZE_Y + HOLE_SIZE_Y)
						{
							// 落ちて画面外に出た
							(*itr)->SetHP(0);
						}
						else
						{
							(*itr)->AddPos(VECTOR2(0, 5));
						}
					}
				}
				(*itr)->UpDate(ctl, objList);
			}

			// objListから要素を削除する
			objList->remove_if([](sharedObj& obj) {return obj->CheckDeth(); });

			// 攻撃する側
			for (auto obj : (*objList))
			{
				// 攻撃を受ける側
				for (auto character = charList.begin(); character != last_itr; character++)
				{
					if ((*character)->GetPlayerNo() != (*obj).GetPlayerNo())
					{
						if ((*obj).CheckHit((*character)->GetDefensePos(), (*character)->GetDefenseSize(), (*character)->GetDepth()))
						{
							if ((*character)->Hit((*obj).GetPower(), (*obj).GetKnockBack()))
							{
								(*obj).AddSpecialCnt((*obj).GetSpecialUp());
							}
						}
					}
				}
			}
		}
	}

	//描画処理
	GameDraw();

	if (startFlag)
	{
		if (startCnt < 0)
		{
			if (!lpSceneMng.GetTutorialFlag())
			{
				if (timeCntFlag)
				{
					timeCnt--;
				}
			}
		}
		else
		{
			startCnt--;
		}
	}
	else
	{
		explanationTextCnt++;
	}

	//現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す
	return std::move(own);
}

bool GameScene::GameDraw(void)
{
	//描画処理
	ClsDrawScreen();		// 画面消去

	VECTOR2 screenSize(lpSceneMng.GetScreenSize());

	if (startFlag)
	{
		VECTOR2 drawOffset(lpSceneMng.GetDrawOffset());
		VECTOR2 gameScreen(lpSceneMng.GetGameScreenSize());

		(*objList).sort([](sharedObj obj1, sharedObj obj2) {return (*obj1).GetDepth() < (*obj2).GetDepth(); });


		sharedObjList charList((*objList).size());
		auto last_itr = std::remove_copy_if((*objList).begin(), (*objList).end(), charList.begin(), [](sharedObj obj) {return !((*obj).CheckObjType(OBJ_TYPE_CHAR)); });

		//UIの描画
		for (auto character = charList.begin(); character != last_itr; character++)
		{
			VECTOR2 UiPos((UI_SIZE_X * (*character)->GetPlayerNo()) + (400 * ((*character)->GetPlayerNo() >= 2)), 8);

			//UIｷｬﾗ
			if ((*character)->GetAnim() != "死亡")
			{
				DrawExtendGraph(drawOffset.x + UiPos.x - 4, UiPos.y + 12, drawOffset.x + UiPos.x + (UI_SIZE_X / 2) + 1, -UiPos.y + UI_SIZE_Y + 1, lpImageMng.GetID((*character)->GetImageName())[0], true);
			}
			else
			{
				DrawExtendGraph(drawOffset.x + UiPos.x + 8, UiPos.y + 12, drawOffset.x + UiPos.x + (UI_SIZE_X / 2) - 25 + 1, -UiPos.y + UI_SIZE_Y - 5 + 1, lpImageMng.GetID((*character)->GetImageName())[10], true);
			}

			//HPｹﾞｰｼﾞ
			for (int h = 0; h < (*character)->GetHP(); h++)
			{
				DrawRectGraph(drawOffset.x + UiPos.x + 109 + (13 * h), UiPos.y + 53, 11 * h, 0, 11, 26, lpImageMng.GetID("image/HPgauge.png")[0], true, false);
			}

			//必殺技ｹﾞｰｼﾞ
			if ((*character)->GetSpecialCnt() < (*character)->GetSpecialCntMax())
			{
				for (int s = 0; s < (*character)->GetSpecialCnt() / ((*character)->GetSpecialCntMax() / 10); s++)
				{
					DrawGraph(drawOffset.x + UiPos.x + 109 + (13 * s), UiPos.y + 95, lpImageMng.GetID("image/ACTgauge.png")[s], true);
				}
			}
			else
			{
				for (int s = 0; s < 10; s++)
				{
					DrawGraph(drawOffset.x + UiPos.x + 109 + (13 * s), UiPos.y + 95, lpImageMng.GetID("image/ACT_MAXgauge.png")[s], true);
				}
			}

		}

		DrawGraph(0, 0, bgImage, true);

		int time = timeCnt / 60;

		if (time > 5)
		{
			DrawFormatStringToHandle(drawOffset.x + (gameScreen.x / 2) - 25 * ((time >= 100) + (time >= 10) + (time >= 0)), TEXT_SIZE, 0xffffff, timeFont, "%d", time);
		}

		if (lpSceneMng.GetTutorialFlag())
		{
			DrawExtendGraph(drawOffset.x + (UI_SIZE_X * 2), 0, drawOffset.x + (UI_SIZE_X * 2) + 400, 300, lpImageMng.GetID("image/sousa.png")[0], false);
		}

		//objListに登録されているｸﾗｽの描画処理を行う
		for (auto &data : (*objList))
		{
			(*data).Draw();
		}

		if (time <= 5)
		{
			if (time > 0)
			{
				SetDrawBlendMode(DX_BLENDMODE_ALPHA, 200);
				DrawFormatStringToHandle(drawOffset.x + (gameScreen.x / 2) - 25 * ((time >= 100) + (time >= 10) + (time >= 0)) - 25, gameScreen.y / 2 + 50, 0xff4c4c, timeFont_Big, "%d", time);
				SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
			}
			else
			{
				if (timeCnt > 0)
				{
					if(CheckSoundMem(lpSoundMng.GetID("se/KO.mp3")) == 0)
					{
						PlaySoundMem(lpSoundMng.GetID("se/KO.mp3"), DX_PLAYTYPE_BACK, true);
					}
				}
				DrawStringToHandle(drawOffset.x + (gameScreen.x / 2) - (150 * 3), gameScreen.y / 2 + 50, "ＴＩＭＥＵＰ", 0xff4c4c, textFont_Big);
			}
		}

		if (startCnt > 0)
		{
			if (startCnt > 60)
			{
				DrawFormatStringToHandle(drawOffset.x + (gameScreen.x / 2) - 50, gameScreen.y / 2 + 50, 0xff4c4c, timeFont_Big, "%d", startCnt / 60);
			}
			else
			{
				int StrLen = (int)strlen("ＳＴＡＲＴ");
				int StrWidth = GetDrawStringWidthToHandle("ＳＴＡＲＴ", StrLen, textFont_Big);
				DrawStringToHandle((screenSize.x / 2) - (StrWidth / 2), gameScreen.y / 2 + 50, "ＳＴＡＲＴ", 0xff4c4c, textFont_Big);
			}
		}

	}
	else
	{
		if (explanationGraphCnt <= 0)
		{
			DrawExtendGraph(0, 0, screenSize.x, screenSize.y, lpImageMng.GetID("image/北里＿操作説明.png")[0], false);
		}
		else
		{
			DrawExtendGraph(0, 0, screenSize.x, screenSize.y, lpImageMng.GetID("image/落下画像.png")[0], false);
		}

		if (((explanationTextCnt / 30) % 2))
		{
			if (explanationGraphCnt < 1)
			{
				DrawCircle(1245, 245, 10, 0xb83dba);
			}

			int StrLen = (int)strlen("ＰＬＥＡＳＥ ＰＵＳＨ ＳＴＡＲＴ ＢＵＴＴＯＮ");
			int StrWidth = GetDrawStringWidthToHandle("ＰＬＥＡＳＥ ＰＵＳＨ ＳＴＡＲＴ ＢＵＴＴＯＮ", StrLen, textFont);
			DrawStringToHandle((screenSize.x / 2) - (StrWidth / 2), screenSize.y / 2 + 60, "ＰＬＥＡＳＥ ＰＵＳＨ ＳＴＡＲＴ ＢＵＴＴＯＮ", 0xffffff, textFont);
		}
	}

	ScreenFlip();		// ｹﾞｰﾑﾙｰﾌﾟの最後に必ず必要
	return true;
}

// 落下するかのチェック　引数:座標,ｻｲｽﾞ
bool GameScene::CheckFall(VECTOR2 pos, VECTOR2 size)
{
	VECTOR2 startVec_R(0, 135);
	VECTOR2 startVec_L(1400, 135);

	if (   (CrossProduct_2D()(startVec_R,VECTOR2(pos.x + size.x, pos.y + size.y), startVec_R + 200) < 0)
		|| (pos.y + size.y > GAME_SCREEN_SIZE_Y)
		|| (CrossProduct_2D()(startVec_L, VECTOR2(pos.x, pos.y + size.y), VECTOR2(startVec_L.x - 200, startVec_L.y + 200)) > 0) )
	{
		return true;
	}
	return false;
}

int GameScene::Init(void)
{
	if (!objList)
	{
		objList = std::make_shared<sharedObjList>();
	}
	objList->clear();		//objListを全削除する
	lpSceneMng.SetGameScreenSize(VECTOR2(GAME_SCREEN_SIZE_X, GAME_SCREEN_SIZE_Y));
	lpSceneMng.SetDrawOffset(VECTOR2(GAME_SCREEN_X, GAME_SCREEN_Y));
	VECTOR2 gameScreen = lpSceneMng.GetGameScreenSize();

	lpSceneMng.RankInit();

	//選択したｷｬﾗへの振り分け
	auto CharacterSetUp = [=](CHARA_ID id,int i,bool aiFlag) {
		switch (id)
		{
		case CHARA_ID::GOUKAN1:
			AddObjList()(objList, std::make_unique<Goukan1>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN2:
			AddObjList()(objList, std::make_unique<Goukan2>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN3:
			AddObjList()(objList, std::make_unique<Goukan3>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN4:
			AddObjList()(objList, std::make_unique<Goukan4>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN5:
			AddObjList()(objList, std::make_unique<Goukan5>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN6:
			AddObjList()(objList, std::make_unique<Goukan6>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN7:
			AddObjList()(objList, std::make_unique<Goukan7>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN8:
			AddObjList()(objList, std::make_unique<Goukan8>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN9:
			AddObjList()(objList, std::make_unique<Goukan9>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		case CHARA_ID::GOUKAN10:
			AddObjList()(objList, std::make_unique<Goukan10>(lpSceneMng.GetDrawOffset(), i, aiFlag));
			break;
		default:
			_RPT0(_CRT_WARN, "エラー　ｷｬﾗ振り分け\n");
			break;
		}
	};

	std::vector<CHARA_ID> playerChars;
	for (int i = 0; i < 4 ; i++)
	{
		if (i < lpSceneMng.GetPlayerCnt())
		{
			CharacterSetUp(lpSceneMng.GetPlayerId(i), i,false);
		}
		else
		{
			CharacterSetUp(lpSceneMng.GetPlayerId(i), i, true);
		}

	}


	lpImageMng.GetID("image/UI_frame.png", VECTOR2(1000 / 4, 142), VECTOR2(4, 1));
	lpImageMng.GetID("image/ACTgauge.png", VECTOR2(110 / 10, 26), VECTOR2(10, 1));
	lpImageMng.GetID("image/ACT_MAXgauge.png", VECTOR2(110 / 10, 26), VECTOR2(10, 1));

	timeCnt = TIME_CNT_MAX;

	timeCntFlag = true;


	timeFont = CreateFontToHandle(NULL, TIME_FONT_SIZE, 10, DX_FONTTYPE_EDGE);


	timeFont_Big = CreateFontToHandle(NULL, 200, 10, DX_FONTTYPE_EDGE);


	textFont = CreateFontToHandle(NULL, TEXT_SIZE, 10, DX_FONTTYPE_EDGE);


	textFont_Big = CreateFontToHandle(NULL, 150, 10, DX_FONTTYPE_EDGE);

	rankCnt = 4;

	startFlag = lpSceneMng.GetTutorialFlag();

	startCnt = START_CNT_MAX * (!lpSceneMng.GetTutorialFlag());

	explanationTextCnt = 0;

	explanationGraphCnt = 0;

	//背景作成
	bgImage = MakeScreen(lpSceneMng.GetScreenSize().x, lpSceneMng.GetScreenSize().y, true);
	SetDrawScreen(bgImage);

	VECTOR2 drawOffset = lpSceneMng.GetDrawOffset();

	//後ろ壁
	DrawGraph(drawOffset.x, 0, lpImageMng.GetID("image/background.png")[0], true);
	//床
	DrawGraph(drawOffset.x, 0, lpImageMng.GetID("image/field.png")[0], true);
	//横壁
	DrawGraph(drawOffset.x, 0, lpImageMng.GetID("image/wall.png")[0], true);
	//穴
	DrawGraph(drawOffset.x, 0, lpImageMng.GetID("image/hole.png")[0], true);

	sharedObjList charList((*objList).size());
	auto last_itr = std::remove_copy_if((*objList).begin(), (*objList).end(), charList.begin(), [](sharedObj obj) {return !((*obj).CheckObjType(OBJ_TYPE_CHAR)); });

	for (auto character = charList.begin(); character != last_itr; character++)
	{
		VECTOR2 UiPos((UI_SIZE_X * (*character)->GetPlayerNo()) + (400 * ((*character)->GetPlayerNo() >= 2)), 8);

		//UIﾌﾚｰﾑ
		DrawGraph(drawOffset.x + UiPos.x, UiPos.y, lpImageMng.GetID("image/UI_frame.png")[(*character)->GetPlayerNo()], true);

		//ｹﾞｰｼﾞ外枠
		for (int k = 0; k < 2; k++)
		{
			DrawGraph(drawOffset.x + UiPos.x + 106, UiPos.y + 48 + (40 * k), lpImageMng.GetID("image/UI_gaugeframe.png")[0], true);
		}
	}

	//ﾀｲﾏｰの描画
	DrawStringToHandle(drawOffset.x + (gameScreen.x / 2) - (TEXT_SIZE * 2), 0, "ＴＩＭＥ", 0xffffff, textFont);

	SetDrawScreen(DX_SCREEN_BACK);

	return 0;
}

void GameScene::StopSound(void)
{
	StopSoundMem(lpSoundMng.GetID("bgm/battle.mp3"));
	StopSoundMem(lpSoundMng.GetID("bgm/hokuto.mp3"));
	StopSoundMem(lpSoundMng.GetID("bgm/10goEX01.wav"));
	StopSoundMem(lpSoundMng.GetID("se/1goEX.wav"));
	StopSoundMem(lpSoundMng.GetID("se/charge.mp3"));
	StopSoundMem(lpSoundMng.GetID("se/biim.mp3"));
	StopSoundMem(lpSoundMng.GetID("se/3goEX.wav"));
	StopSoundMem(lpSoundMng.GetID("se/4goEX01.wav"));
	StopSoundMem(lpSoundMng.GetID("se/4goEX02.wav"));
	StopSoundMem(lpSoundMng.GetID("se/5goEX01.wav"));
	StopSoundMem(lpSoundMng.GetID("se/5goEX02.wav"));
	StopSoundMem(lpSoundMng.GetID("se/6goEX01.wav"));
	StopSoundMem(lpSoundMng.GetID("se/6goEX02.wav"));
	StopSoundMem(lpSoundMng.GetID("se/6goEX03.wav"));
	StopSoundMem(lpSoundMng.GetID("se/7goEX.wav"));
	StopSoundMem(lpSoundMng.GetID("se/8goEX.wav"));
	StopSoundMem(lpSoundMng.GetID("se/10goEX02.wav"));
	StopSoundMem(lpSoundMng.GetID("se/cutin.mp3"));
	StopSoundMem(lpSoundMng.GetID("se/down.mp3"));
	StopSoundMem(lpSoundMng.GetID("se/jump.mp3"));
	StopSoundMem(lpSoundMng.GetID("se/kick.wav"));
	StopSoundMem(lpSoundMng.GetID("se/KO.mp3"));
	StopSoundMem(lpSoundMng.GetID("se/punch.wav"));
	StopSoundMem(lpSoundMng.GetID("se/wall.wav"));
}
