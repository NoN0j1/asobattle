#pragma once
#include"ObjList.h"
#include"VECTOR2.h"
#include "BaseScene.h"

enum NEXT_MODE
{
	NEXT_title,
	NEXT_player1,
	NEXT_player2,
	NEXT_player3,
	NEXT_player4,
	NEXT_max
};

// 背景のｽｸﾛｰﾙ方向
enum BACK_DIR
{
	BACK_DIR_LEFT,
	BACK_DIR_RIGHT,
	BACK_DIR_MAX
};

class MainMenuScene :
	public BaseScene
{
public:
	MainMenuScene();		// ｺﾝｽﾄﾗｸﾀ
	~MainMenuScene();		// ﾃﾞｽﾄﾗｸﾀ
	unique_Base Update(unique_Base own, const GameCtl &ctl);	//ﾒｲﾝﾒﾆｭｰ時の処理　引数:現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀ,GameCtlｸﾗｽの情報　返り値として次のﾌﾚｰﾑ時のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す

private:
	int Init(void);					//初期化
	bool MainMenuDraw(void);		//描画処理

	bool moveFlag;				// true:ﾎﾞﾀﾝの移動演出処理中, false:ﾎﾞﾀﾝの移動演出処理中ではない
	int selectFlag;				// 選択中のﾎﾞﾀﾝ　0:戻る, 1:対戦ﾓｰﾄﾞ, 2:ﾁｭｰﾄﾘｱﾙ, 3:2人ﾌﾟﾚｲ, 4:3人ﾌﾟﾚｲ, 5:4人ﾌﾟﾚｲ
	VECTOR2 taisenMove;			// 対戦ﾓｰﾄﾞﾎﾞﾀﾝの移動量
	int taisenTxtMove;			// 対戦ﾓｰﾄﾞの文字の移動量
	bool cursorMoveFlg;			// <true>:そのﾌﾚｰﾑ内でｶｰｿﾙ移動をした, <false>:そのﾌﾚｰﾑ内ではｶｰｿﾙ移動をしていない
	int nextMode;				// ｼｰﾝ移行するﾓｰﾄﾞ
	bool inputCancelFlag;		// 入力を受け付けない状態にするﾌﾗｸﾞ　true:受け付けない, false:受け付ける
	int joypadCnt;				// 入力されてるﾊﾟｯﾄﾞ数
	int bgPosX1[BACK_DIR_MAX];
	int bgPosX2[BACK_DIR_MAX];
};

