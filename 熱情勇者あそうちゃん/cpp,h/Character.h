#pragma once
#include<functional>
#include<array>
#include "ATTACK_TYPE.h"
#include "Obj.h"

class VECTOR2;

using DIR_TBL = std::array<VECTOR2, 2>;
using ATTACK_POS_TBL = std::array<VECTOR2[static_cast<int>(ATTACK_TYPE::MAX)], 2>;
using ATTACK_SIZE_TBL = std::array<VECTOR2, static_cast<int>(ATTACK_TYPE::MAX)>;

#define CUTIN_ANIM_MAX		(4)
#define CUTIN_ANIM_SPEED	(3)
#define CUTIN_END_FRAME		(30)

#define DEF_ATTACK_FRONT		(20)	//ﾃﾞﾌｫﾙﾄの攻撃範囲(手前)
#define DEF_ATTACK_BACK			(30)	//ﾃﾞﾌｫﾙﾄの攻撃範囲(奥)

class Character :
	public Obj
{
public:
	Character(VECTOR2 drawOffset, int popNo, bool aiFlag);
	virtual ~Character();
	//ｱﾆﾒｰｼｮﾝの初期化
	bool initAnim(void);

	//ｵﾌﾞｼﾞｪｸﾄのﾀｲﾌﾟが引数で指定したﾀｲﾌﾟか確かめる　OBJ_TYPE_CHARでtrue
	bool CheckObjType(OBJ_TYPE type);

	//当たり判定 引数:攻撃を受ける座標,攻撃を受けるｻｲｽﾞ,ｼﾞｬﾝﾌﾟしてるか
	bool CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth);

	//攻撃を受けた時の時の処理
	bool Hit(const int ATK, int knockBack);

	//奥行情報を取得
	int GetDepth(void);

	//HPを取得する
	int GetHP(void);

	//HPの最大値を取得する
	virtual int GetHPMax(void);

	//必殺技ｹﾞｰｼﾞを取得する
	int GetSpecialCnt(void);

	//必殺技ｹﾞｰｼﾞを増加させる
	void AddSpecialCnt(int specialUp);

	//必殺技ｹﾞｰｼﾞの増加量を取得する
	int GetSpecialUp(void);

	//必殺技ｹﾞｰｼﾞの最大値を取得する　仮想関数
	int GetSpecialCntMax(void);

	//自分のﾌﾟﾚｲﾔｰ番号を取得する
	int GetPlayerNo(void);

	//ﾉｯｸﾊﾞｯｸの値を取得する
	int GetKnockBack(void);

	//ｷｬﾗｸﾀｰの移動速度を取得する
	int GetSpeed(void);

	//描画処理
	void Draw(void);

	//暗転描画中かを取得する
	void SetSPCutinFlag(bool flg);

	// 必殺技を使っているか取得する
	bool GetSpecialFlag(void);

protected:
	int speed;			//移動ｽﾋﾟｰﾄﾞ

	int specialCnt;		//必殺技ゲージ
	int specialCntMax;	//必殺技ゲージの最大値
	bool specialFlag;	//必殺技ﾌﾗｸﾞ

	unsigned int specialTimeCnt;	//必殺技の時間

	int depth;			//足元の座標

	bool turnFlag;		//反転ﾌﾗｸﾞ

	int playerNo;		//自分のﾌﾟﾚｲﾔｰ番号

	int attackRangeFront;	//攻撃範囲(手前)
	int attackRangeBack;	//攻撃範囲(奥)

	int knockBack;		//攻撃した時に下がらせる値

	bool damageFlag;	//攻撃を受けるかﾌﾗｸﾞ　true:受ける,false:受けない

	bool powerUpFlag;
	int powerUpCnt;		//攻撃力ｱｯﾌﾟ時間

	bool (Character::*move)(const GameCtl&, weekListObj);		//ｷｬﾗｸﾀｰの動き　関数ﾎﾟｲﾝﾀ

	static bool SPcutinFlag;

	//通常時の処理
	bool Normal(const GameCtl &ctl, weekListObj objList);
	
	std::array<VECTOR2, 4> CutinPosTbl;	// ｶｯﾄｲﾝ描画位置ﾃｰﾌﾞﾙ

private:
	//移動処理
	void SetMove(const GameCtl &ctl, weekListObj objList);

	//AI処理
	void AIAct(int& rand, int minlange, VECTOR2 vec);

	//ｼﾞｬﾝﾌﾟ時の処理
	bool Jump(const GameCtl &ctl, weekListObj objList);
	//ｼﾞｬﾝﾌﾟｷｯｸ時の処理
	bool JumpKick(const GameCtl &ctl, weekListObj objList);
	//ﾊﾟﾝﾁ時の処理
	bool Punch(const GameCtl &ctl, weekListObj objList);
	//ｷｯｸ時の処理
	bool Kick(const GameCtl &ctl, weekListObj objList);
	//必殺技時の処理
	virtual bool Special(const GameCtl &ctl, weekListObj objList) = 0;
	//死亡時の処理
	bool Deth(const GameCtl &ctl, weekListObj objList);

	//攻撃情報をｾｯﾄする
	void AttackSet(ATTACK_TYPE type, bool turnFlag);

	//必殺技時の描画　純粋仮想関数
	virtual void SpecialDraw(bool specialFlag) = 0;

	//ﾃﾞﾌｫﾙﾄの攻撃力を取得する
	virtual int GetDefPower(void) = 0;

	VECTOR2 moveSpeed;			//移動ｽﾋﾟｰﾄﾞ

	int jumpSpeed;				//ｼﾞｬﾝﾌﾟのｽﾋﾟｰﾄﾞ
	int jumpCut;				//ｼﾞｬﾝﾌﾟの秒数

	unsigned int damageCnt;		//無敵時間
	DIR_TBL defensePosTbl;				//攻撃を受ける用の座標ﾃｰﾌﾞﾙ

	ATTACK_POS_TBL attackPosTbl;		//攻撃の座標ﾃｰﾌﾞﾙ
	ATTACK_SIZE_TBL attackSizeTbl;		//攻撃のｻｲｽﾞﾃｰﾌﾞﾙ

	std::array<VECTOR2, 4> defPosTbl;	//初期座標のﾃｰﾌﾞﾙ
	std::array<bool, 4> defTurnTbl;		//初期反転ﾌﾗｸﾞのﾃｰﾌﾞﾙ

	//ﾃｰﾌﾞﾙの初期化
	bool TblInit(void);

	bool aiFlag;		// aiかどうか（true:ai false;player）

	int actCnt;	// ｱｸｼｮﾝ間隔
};

