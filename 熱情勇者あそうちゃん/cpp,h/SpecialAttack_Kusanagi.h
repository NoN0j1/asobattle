#pragma once
#include "Obj.h"

#define MOVE_SCOPE_X	(995 - divSize.x)
#define MOVE_SCOPE_Y	(335)

class SpecialAttack_Kusanagi :
	public Obj
{
public:
	SpecialAttack_Kusanagi(VECTOR2 drawOffset, int popNo);
	virtual ~SpecialAttack_Kusanagi();

	// ｵﾌﾞｼﾞｪｸﾄを削除して良いかのﾁｪｯｸ
	bool CheckDeth(void);

	//ｵﾌﾞｼﾞｪｸﾄのﾀｲﾌﾟが引数で指定したﾀｲﾌﾟか確かめる
	bool CheckObjType(OBJ_TYPE type);

	//当たり判定 引数:攻撃を受ける座標,攻撃を受けるｻｲｽﾞ,ｼﾞｬﾝﾌﾟしてるか
	bool CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth);

	//自分のﾌﾟﾚｲﾔｰ番号を取得する
	int GetPlayerNo(void);

	//描画処理
	void Draw(void);

protected:
	int depth;			//足元の座標

	bool dethFlag;		//削除して良いかのﾌﾗｸﾞ

	int speed_X;		//横移動ｽﾋﾟｰﾄﾞ
	int speed_Y;		//縦移動ｽﾋﾟｰﾄﾞ

private:
	//移動処理
	virtual void SetMove(const GameCtl &ctl, weekListObj objList);

	int playerNo;		//ﾌﾟﾚｲﾔｰの番号

	int attackRangeFront;	//攻撃範囲(手前)
	int attackRangeBack;	//攻撃範囲(奥)

	bool upFlag;			//上に移動するかﾌﾗｸﾞ

	int upCnt;				//上に移動した回数
};
