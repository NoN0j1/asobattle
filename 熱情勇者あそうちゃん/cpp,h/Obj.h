#pragma once
#include<string>
#include<map>
#include"VECTOR2.h"
#include"ObjList.h"

enum ANIM_TBL{
	ANIM_TBL_START_ID,		//開始位置
	ANIM_TBL_FRAME,			//ｺﾏ数
	ANIM_TBL_INV,			//間隔
	ANIM_TBL_LOOP,			//ﾙｰﾌﾟするかどうか
	ANIM_TBL_MAX
};

enum OBJ_TYPE
{
	OBJ_TYPE_CHAR,
	OBJ_TYPE_CURSOR,
	OBJ_TYPE_SP_ATTACK,
	OBJ_TYPE_MAX
};

class GameCtl;

class Obj
{
public:
	//ｺﾝｽﾄﾗｸﾀ
	Obj();
	//ｺﾝｽﾄﾗｸﾀ　引数:描画ｵﾌｾｯﾄ
	Obj(VECTOR2 drawOffset);

	//初期化
	bool init(std::string fileName, VECTOR2 divSize, VECTOR2 divCut);
	//初期化	引数:ﾌｧｲﾙの名前,分割ｻｲｽﾞ,分割数,座標
	bool init(std::string fileName, VECTOR2 divSize, VECTOR2 divCut, VECTOR2 pos);

	virtual ~Obj();

	//情報更新
	void UpDate(const GameCtl &ctl);
	void UpDate(const GameCtl &ctl, weekListObj objList);

	//ｱﾆﾒｰｼｮﾝ初期化　仮想関数
	virtual bool initAnim(void) { return true; };

	//描画処理
	virtual void Draw(void);

	//描画処理 引数付き
	virtual void Draw(unsigned int id);

	// ｵﾌﾞｼﾞｪｸﾄを削除して良いかのﾁｪｯｸ
	virtual bool CheckDeth(void) { return false; };

	//ｵﾌﾞｼﾞｪｸﾄのﾀｲﾌﾟが引数で指定したﾀｲﾌﾟか確かめる　純粋仮想関数
	virtual bool CheckObjType(OBJ_TYPE type) = 0;

	//座標をｾｯﾄする　引数:座標
	void SetPos(VECTOR2 pos);
	//posの情報を取得する
	const VECTOR2 &GetPos(void);
	void AddPos(VECTOR2 addPos);

	//PlayerNoの情報を取得する
	virtual int GetPlayerNo(void);

	//自分がｼｰﾝ以降しても大丈夫かどうかの情報を取得する(true:準備完了, false:準備完了でない) 仮想関数
	virtual bool GetStandbyFlag(void);

	//画像ﾌｧｲﾙ名を取得する
	std::string GetImageName(void);

	//ｱﾆﾒｰｼｮﾝを追加する
	bool AddAnim(std::string animName, int id_x, int id_y, int frame, int inv, bool loop);
	//ｱﾆﾒｰｼｮﾝをｾｯﾄする
	bool SetAnim(std::string animName);
	//ｱﾆﾒｰｼｮﾝ情報を取得する
	std::string GetAnim(void);
	//当たり判定 仮想関数
	virtual bool CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize);
	//当たり判定 仮想関数 引数:攻撃を受ける座標,攻撃を受けるｻｲｽﾞ,ｼﾞｬﾝﾌﾟしてるか
	virtual bool CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth);

	//攻撃を受けた時の処理　仮想関数
	virtual bool Hit(const int ATK,int knockBack);
	//ダメージ処理
	bool Damage(const int ATK);

	//攻撃を受ける座標を取得する
	VECTOR2 GetDefensePos(void);
	//攻撃を受ける範囲を取得する
	VECTOR2 GetDefenseSize(void);
	//攻撃力を取得する
	const int GetPower(void);

	//奥行情報を取得
	virtual int GetDepth(void);

	//HPをｾｯﾄする
	void SetHP(int hp);

	//HPを取得する
	virtual int GetHP(void);

	//HPの最大値を取得する
	virtual int GetHPMax(void);

	//必殺技ｹﾞｰｼﾞを取得する　仮想関数
	virtual int GetSpecialCnt(void);

	//必殺技ｹﾞｰｼﾞを増加させる　仮想関数
	virtual void AddSpecialCnt(int specialUp);

	//必殺技ｹﾞｰｼﾞの増加量を取得する　仮想関数
	virtual int GetSpecialUp(void);

	//必殺技ｹﾞｰｼﾞの最大値を取得する　仮想関数
	virtual int GetSpecialCntMax(void);

	//落下ﾌﾗｸﾞをｾｯﾄする
	void SetFallFlag(bool flag);

	//落下ﾌﾗｸﾞの状態を取得する
	const bool GetFallFlag(void);

	//ﾉｯｸﾊﾞｯｸの値を取得する　仮想関数
	virtual int GetKnockBack(void);

	//前ﾌﾚｰﾑからHPに変更があるか確認する
	const bool GetDamageHP(void);

	//ｷｬﾗｸﾀｰの移動速度を取得する
	virtual int GetSpeed(void);

	//暗転描画中かを取得する
	virtual void SetSPCutinFlag(bool flg);

	virtual bool GetSpecialFlag(void);

protected:
	VECTOR2 pos;					//座標

	VECTOR2 attackPos;				//攻撃をする座標
	VECTOR2 attackSize;				//攻撃をするｻｲｽﾞ
	bool attackFlag;				//攻撃のﾌﾗｸﾞ

	VECTOR2 defensePos;				//攻撃を受ける座標
	VECTOR2 defenseSize;			//攻撃を受けるｻｲｽﾞ

	int HP;							//体力
	int power;						//攻撃力
	int OldHP;						//1F前の体力値
	bool fallFlag;					// 穴に落下したかどうか

	std::string imageName;			//表示画像ﾌｧｲﾙ名
	VECTOR2 divSize;				//分割ｻｲｽﾞ
	VECTOR2 divCut;					//分割数
	const VECTOR2 drawOffset;		//描画ｵﾌｾｯﾄ
	unsigned int animCnt;			//ｱﾆﾒｰｼｮﾝｶｳﾝﾀｰ
	bool animEndFlag;				//ｱﾆﾒｰｼｮﾝ最終ｺﾏ到達ﾌﾗｸﾞ
	bool visible;					//表示・非表示ﾌﾗｸﾞ

	std::map<std::string, int[ANIM_TBL_MAX]> animTable;		//ｱﾆﾒｰｼｮﾝ情報
	std::string animName;									//表示ｱﾆﾒｰｼｮﾝ名
private:
	//移動処理　仮想関数
	virtual void SetMove(const GameCtl &ctl);
	virtual void SetMove(const GameCtl &ctl, weekListObj objList);
};