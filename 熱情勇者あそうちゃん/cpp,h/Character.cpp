#include<algorithm>
#include"DxLib.h"
#include"GameCtl.h"
#include"SceneMng.h"
#include"SoundMng.h"
#include"ImageMng.h"
#include"CrossProduct_2D.h"
#include"WALL.h"
#include "Character.h"

#define CHAR_DEF_JUMP_SPEED (5)		//ﾃﾞﾌｫﾙﾄのｼﾞｬﾝﾌﾟｽﾋﾟｰﾄﾞ
#define CHAR_DEF_CUT_MAX (15)		//ﾃﾞﾌｫﾙﾄのｼﾞｬﾝﾌﾟ最高到達点までのｶｳﾝﾄ

#define DAMAGE_CUT_MAX (60U)		//ﾀﾞﾒｰｼﾞを受けた後の無敵時間

#define WALL_SIZE_Y	(135)			//壁の縦ｻｲｽﾞ
#define WALL_BOUND (10)				//壁の反発

#define SPECIAL_CNT_UP_ATTACK	(8)		//攻撃したときの必殺技ｹﾞｰｼﾞ増加量
#define SPECIAL_CNT_UP_DAMAGE	(10)	//攻撃を受けた時の必殺技ｹﾞｰｼﾞ増加量

#define CHARACTER_SIZE (192)

bool Character::SPcutinFlag = false;

Character::Character(VECTOR2 drawOffset,int popNo, bool aiFlag) :Obj(drawOffset)
{
	TblInit();

	this->playerNo = popNo;

	pos = defPosTbl[popNo];

	Character::move = &Character::Normal;
	jumpSpeed = CHAR_DEF_JUMP_SPEED;
	
	depth = (pos.y + CHARACTER_SIZE);

	damageFlag = true;
	damageCnt = 0U;

	defenseSize = VECTOR2(78, 180);

	attackRangeFront	= DEF_ATTACK_FRONT;
	attackRangeBack		= DEF_ATTACK_BACK;

	specialCnt = 100 * lpSceneMng.GetTutorialFlag();

	turnFlag = defTurnTbl[popNo];

	fallFlag = false;

	specialFlag = false;

	knockBack = 40;

	powerUpFlag = false;
	powerUpCnt = 0;

	this->aiFlag = aiFlag;
	actCnt = 0;
}

Character::~Character()
{
}

bool Character::initAnim(void)
{
	AddAnim("立ち", 0, 0, 1, 10, false);
	AddAnim("歩き", 0, 0, 2, 6, true);
	AddAnim("パンチ", 1, 1, 1, 20, false);
	AddAnim("キック", 1, 2, 1, 20, false);
	AddAnim("ジャンプ", 0, 3, 2, 3, false);
	AddAnim("ジャンプキック", 1, 4, 1, 3, false);
	AddAnim("ダウン", 0, 5, 2, 20, false);
	AddAnim("死亡", 0, 5, 1, 20, false);
	SetAnim("立ち");
	return true;
}

bool Character::CheckObjType(OBJ_TYPE type)
{
	return (type == OBJ_TYPE_CHAR);
}

bool Character::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize,int depth)
{
	if (attackFlag)
	{
		if (this->depth + attackRangeFront >= depth && this->depth - attackRangeBack <= depth)
		{
			if (this->attackPos.x < defensePos.x + defenseSize.x
				&&this->attackPos.x + attackSize.x > defensePos.x
				&&this->attackPos.y < defensePos.y + defenseSize.y
				&&this->attackPos.y + attackSize.y > defensePos.y)
			{
				return true;
			}
		}
	}
	return false;
}

bool Character::Hit(const int ATK,int knockBack)
{
	if (damageFlag)
	{
		if (Damage(ATK))
		{
			pos.x -= knockBack;
			AddSpecialCnt(SPECIAL_CNT_UP_DAMAGE);
			damageFlag = false;
		}
		return true;
	}
	return false;
}

int Character::GetDepth(void)
{
	return depth;
}

int Character::GetHP(void)
{
	if (HP / (GetHPMax() / 10) <= 0)
	{
		if (HP > 0)
		{
			return 1;
		}
	}
	return HP / (GetHPMax() / 10);
}

int Character::GetHPMax(void)
{
	return 0;
}

int Character::GetSpecialCnt(void)
{
	return specialCnt;
}

void Character::AddSpecialCnt(int specialUp)
{
	specialCnt += specialUp;
}

int Character::GetSpecialUp(void)
{
	return SPECIAL_CNT_UP_ATTACK;
}

int Character::GetSpecialCntMax(void)
{
	return specialCntMax;
}

int Character::GetPlayerNo(void)
{
	return playerNo;
}

int Character::GetKnockBack(void)
{
	return knockBack * (turnFlag - (!turnFlag));
}

int Character::GetSpeed(void)
{
	return speed;
}

void Character::Draw(void)
{
	if (GetAnim() != "死亡" && (!fallFlag))
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

		DrawOval(	drawOffset.x + pos.x + ((divSize.x *15/10) / 2),
					drawOffset.y + depth - 14,
					CHARACTER_SIZE / 4 + 4, 
					12, 
					0x000000, true);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}

	if (powerUpCnt > 0)
	{
		SetDrawBright(255, 185 + powerUpCnt % 61, 185 + powerUpCnt % 61);
	}

	if (!specialFlag)
	{
		if (turnFlag)
		{
			if (imageName.length() == 0)
			{
				return;
			}
			unsigned int id = 0;
			if (animTable.find(animName) != animTable.end())
			{
				int count = animCnt / animTable[animName][ANIM_TBL_INV];
				if (animTable[animName][ANIM_TBL_LOOP] || count < animTable[animName][ANIM_TBL_FRAME])
				{
					count %= animTable[animName][ANIM_TBL_FRAME];
				}
				else
				{
					count = animTable[animName][ANIM_TBL_FRAME] - 1;
					animEndFlag = true;
				}

				id = animTable[animName][ANIM_TBL_START_ID] + count;
			}

			if (id < IMAGE_ID(imageName).size())
			{
				if (visible)
				{
					DrawExtendGraph(drawOffset.x + pos.x + divSize.x * 15/10,
									drawOffset.y + pos.y,
									drawOffset.x + pos.x,
									drawOffset.y + pos.y + divSize.y * 15/10,
									IMAGE_ID(imageName)[id], true);
				}
			}
			animCnt++;

		}
		else
		{
			Obj::Draw();
		}
	}
	else
	{
		SpecialDraw(specialFlag);
	}

	SetDrawBright(255, 255, 255);

#ifdef _DEBUG
	if (attackFlag)
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);
		//攻撃用判定
		DrawBox(drawOffset.x + attackPos.x, drawOffset.y + attackPos.y,
			drawOffset.x + attackPos.x + attackSize.x + 1, drawOffset.y + attackPos.y + attackSize.y + 1,
			0x0000ff,
			true);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
	}

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);
	//ダメージ用判定
	DrawBox(drawOffset.x + defensePos.x, drawOffset.y + defensePos.y,
		drawOffset.x + defensePos.x + defenseSize.x + 1, drawOffset.y + defensePos.y + defenseSize.y + 1,
		0xff0000,
		true);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	VECTOR2 gameScreen(lpSceneMng.GetGameScreenSize());

	DrawLine(drawOffset.x + 0, drawOffset.y + depth - attackRangeBack, drawOffset.x + gameScreen.x, drawOffset.y + depth - attackRangeBack, 0xff0000);
	DrawLine(drawOffset.x + 0, drawOffset.y + depth + attackRangeFront, drawOffset.x + gameScreen.x, drawOffset.y + depth + attackRangeFront, 0xff0000);
#endif
}

void Character::SetSPCutinFlag(bool flg)
{
	SPcutinFlag = flg;
}

bool Character::GetSpecialFlag(void)
{
	return specialFlag;
}

struct CheckWall
{
	WALL operator()(VECTOR2 pos, VECTOR2 addPos, VECTOR2 size,int depth)
	{
		if (depth >= WALL_SIZE_Y)
		{
			return WALL::NON;
		}
		
		VECTOR2 tmpPos(pos.x + addPos.x, depth + addPos.y - size.y);

		VECTOR2 startPos_L(0, 135);
		VECTOR2 startPos_R(1400, 135);

		if (CrossProduct_2D()(startPos_L, VECTOR2(tmpPos.x, tmpPos.y + size.y), VECTOR2(startPos_L.x + 135, startPos_L.y - 135)) > 0)
		{
			return WALL::LEFT;
		}

		if (CrossProduct_2D()(startPos_R, tmpPos + size, startPos_R - 135) < 0)
		{
			return WALL::RIGHT;
		}
		return WALL::NON;
	}
};

void Character::SetMove(const GameCtl & ctl, weekListObj objList)
{
	OldHP = HP;

	if (!specialFlag)
	{
		if (!damageFlag)
		{
			damageCnt++;
		}
	}

	if (HP <= 0)
	{
		move = &Character::Deth;
	}
	else
	{
		if (damageCnt > DAMAGE_CUT_MAX)
		{
			damageFlag = true;
			damageCnt = 0U;
		}

	}

	visible = true;

	if ((damageCnt / 3) % 2)
	{
		visible = false;
	}

	auto AttackSet = [&](ATTACK_TYPE type, bool turnFlag) {
		attackPos = attackPosTbl[static_cast<int>(type)][turnFlag];
		attackSize = attackSizeTbl[turnFlag];
	};

	(this->*move)(ctl, objList);

	WALL checkWall = CheckWall()(pos, defensePosTbl[turnFlag], defenseSize, depth);

	if (checkWall != WALL::NON)
	{
		PlaySoundMem(lpSoundMng.GetID("se/wall.wav"), DX_PLAYTYPE_BACK, true);
		if (checkWall == WALL::LEFT)
		{
			moveSpeed.x = WALL_BOUND;
		}
		if (checkWall == WALL::RIGHT)
		{
			moveSpeed.x = -WALL_BOUND;
		}
		Character::move = &Character::Jump;
	}

	defensePos = pos + defensePosTbl[turnFlag];

	if (powerUpFlag)
	{
		if (powerUpCnt < 0)
		{
			power = GetDefPower();
		}
		else
		{
			powerUpCnt--;
		}
	}
}

void Character::AIAct(int& rand, int minlange, VECTOR2 vec)
{
	
}

bool Character::Normal(const GameCtl & ctl, weekListObj objList)
{
	sharedObjList charList(objList.lock()->size());
	auto last_itr = std::remove_copy_if(objList.lock()->begin(), objList.lock()->end(), charList.begin(), [](sharedObj obj) {return !((*obj).CheckObjType(OBJ_TYPE_CHAR)); });
	
	int rand = 0;
	int minlange = 10000;
	for (auto character = charList.begin(); character != last_itr; character++)
	{
		if (!aiFlag)
		{
			break;
		}
		if (((*character)->GetPlayerNo() != this->playerNo)&&(*character)->GetFallFlag() == false && (*character)->GetHP() > 0)
		{
			// aiの処理の基準の生成
			VECTOR2 vec = ((*character)->GetPos() - this->GetPos());
			if (minlange > sqrt(static_cast<float>(vec.y*vec.y) + static_cast<double>(vec.x*vec.x)))
			{
				if (vec.x > 15)
				{
					//敵が右
					if (vec.y <= 0)
					{
						//敵が上
						rand = 1;
					}
					if (vec.y > 0)
					{
						//敵が下
						rand = 2;
					}
				}

				if (vec.x < -15)
				{
					//敵が左
					turnFlag = true;
					if (vec.y <= 0)
					{
						//敵が上
						rand = 3;
					}
					if (vec.y > 0)
					{
						//敵が下

						rand = 4;
					}
				}

				if (actCnt % 3 == 0)
				{
					if (abs(vec.x) < 80)
					{
						rand = GetRand(4) + 8;

						if (abs(vec.y) < 20)
						{
							// ﾊﾟﾝﾁｷｯｸｼﾞｬﾝﾌﾟｷｯｸ
							rand = GetRand(3) + 5;
						}
					}
				}
			}
			if ((*character)->GetSpecialFlag())
			{
				if (depth > 100)
				{
					rand = 11;
				}
				else
				{
					rand = 10;
				}
			}
		}
	}

	attackFlag = false;
	specialFlag = false;

	if (moveSpeed == 0)
	{
		SetAnim("立ち");
	}
	else
	{
		SetAnim("歩き");
	}
	moveSpeed = VECTOR2(0, 0);

	if (fallFlag)
	{
		return false;
	}

	if (!aiFlag)
	{
		if (ctl.GetPad(playerNo, PAD_INPUT_RIGHT))
		{
			moveSpeed.x += speed;
			turnFlag = false;
		}
		if (ctl.GetPad(playerNo, PAD_INPUT_LEFT))
		{
			moveSpeed.x -= speed;
			turnFlag = true;
		}
		if (ctl.GetPad(playerNo, PAD_INPUT_DOWN))
		{
			moveSpeed.y += speed;
		}
		if (ctl.GetPad(playerNo, PAD_INPUT_UP))
		{
			moveSpeed.y -= speed;
		}

		//ﾊﾟﾝﾁ
		if (ctl.GetPadTrigger(playerNo, PAD_INPUT_2))
		{
			PlaySoundMem(lpSoundMng.GetID("se/punch.wav"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::Punch;
		}

		//ｷｯｸ
		if (ctl.GetPadTrigger(playerNo, PAD_INPUT_3))
		{
			PlaySoundMem(lpSoundMng.GetID("se/punch.wav"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::Kick;
		}

		//ｼﾞｬﾝﾌﾟ
		if (ctl.GetPadTrigger(playerNo, PAD_INPUT_1))
		{
			PlaySoundMem(lpSoundMng.GetID("se/jump.mp3"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::Jump;
		}

		//必殺技
		if (ctl.GetPad(playerNo, PAD_INPUT_4))
		{
			if (specialCnt >= specialCntMax)
			{
				if (Character::move == &Character::Normal)
				{
					PlaySoundMem(lpSoundMng.GetID("se/cutin.mp3"), DX_PLAYTYPE_BACK, true);
				}
				attackSize = VECTOR2(0, 0);
				damageFlag = false;
				damageCnt = 0U;
				specialTimeCnt = 0;

				Character::move = &Character::Special;
			}

		}
	}
	else
	{
		// aiの処理
		if (rand == 0)
		{
			SetAnim("立ち");
			moveSpeed = { 0,0 };
		}
		if (rand == 1)
		{
			SetAnim("歩き");
			moveSpeed.x += speed;
			moveSpeed.y -= speed;
			turnFlag = false;

		}
		if (rand == 2)
		{
			SetAnim("歩き");
			moveSpeed.x += speed;
			moveSpeed.y += speed;

			turnFlag = false;
		}
		if (rand == 3)
		{
			SetAnim("歩き");
			moveSpeed.x -= speed;
			moveSpeed.y -= speed;

			turnFlag = true;
		}
		if (rand == 4)
		{
			SetAnim("歩き");
			moveSpeed.x -= speed;
			moveSpeed.y += speed;

			turnFlag = true;
		}
		if (rand == 5)
		{
			SetAnim("パンチ");
			PlaySoundMem(lpSoundMng.GetID("se/punch.wav"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::Punch;
		}
		if (rand == 6)
		{
			SetAnim("キック");
			PlaySoundMem(lpSoundMng.GetID("se/punch.wav"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::Kick;
		}
		if (rand == 7)
		{
			SetAnim("ジャンプキック");
			PlaySoundMem(lpSoundMng.GetID("se/jump.mp3"), DX_PLAYTYPE_BACK, true);
			PlaySoundMem(lpSoundMng.GetID("se/kick.wav"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::JumpKick;
		}
		if (rand == 8)
		{
			if (specialCnt >= specialCntMax)
			{
				if (Character::move == &Character::Normal)
				{
					PlaySoundMem(lpSoundMng.GetID("se/cutin.mp3"), DX_PLAYTYPE_BACK, true);
				}
				attackSize = VECTOR2(0, 0);
				damageFlag = false;
				damageCnt = 0U;
				specialTimeCnt = 0;

				Character::move = &Character::Special;
			}

		}
		if (rand == 9)
		{
			SetAnim("歩き");
			moveSpeed.x += speed;
			turnFlag = false;
		}
		if (rand == 10)
		{
			SetAnim("歩き");
			moveSpeed.y += speed;
		}
		if (rand == 11)
		{
			SetAnim("歩き");
			moveSpeed.y -= speed;
		}
		if (rand == 12)
		{
			SetAnim("歩き");
			moveSpeed.x -= speed;
			turnFlag = true;
		}

	}

	VECTOR2 tmpPos = pos;
	tmpPos += moveSpeed;

	if ((tmpPos.x - pos.x != 0) && (tmpPos.y - pos.y != 0))
	{
		moveSpeed.x = moveSpeed.x / 1.4;
		moveSpeed.y = moveSpeed.y / 1.4;
	}
	pos += moveSpeed;

	if (pos.y < -defenseSize.y)
	{
		pos.y = -defenseSize.y;
	}

	depth = pos.y + divSize.y * 15/10;
	actCnt++;
	return true;
}

bool Character::Jump(const GameCtl & ctl, weekListObj objList)
{
	attackFlag = false;

	SetAnim("ジャンプ");
	pos.y += -jumpSpeed + ((jumpSpeed * 2) * (jumpCut / CHAR_DEF_CUT_MAX));
	if (!(depth > 0))
	{
		moveSpeed.y = 0;
	}

	pos += (moveSpeed / 2);
	depth += (moveSpeed.y / 2);

	jumpCut++;

	if (!aiFlag)
	{
		//ｼﾞｬﾝﾌﾟｷｯｸ
		if (ctl.GetPadTrigger(playerNo, PAD_INPUT_2) || ctl.GetPadTrigger(playerNo, PAD_INPUT_3))
		{
			PlaySoundMem(lpSoundMng.GetID("se/kick.wav"), DX_PLAYTYPE_BACK, true);
			Character::move = &Character::JumpKick;
		}
	}
	else
	{
		//AI
	}

	if (jumpCut / CHAR_DEF_CUT_MAX >= 2)
	{
		jumpCut = 0;
		Character::move = &Character::Normal;
	}
	return true;
}

bool Character::JumpKick(const GameCtl & ctl, weekListObj objList)
{
	SetAnim("ジャンプキック");

	pos.y += -jumpSpeed + ((jumpSpeed * 2) * (jumpCut / CHAR_DEF_CUT_MAX));
	if (!(depth > 0))
	{
		moveSpeed.y = 0;
	}

	pos += (moveSpeed / 2);
	depth += (moveSpeed.y / 2);

	jumpCut++;

	attackFlag = true;
	AttackSet(ATTACK_TYPE::JUMPKICK, turnFlag);

	if (jumpCut / CHAR_DEF_CUT_MAX >= 2)
	{
		jumpCut = 0;
		Character::move = &Character::Normal;
	}

	return true;
}

bool Character::Punch(const GameCtl & ctl, weekListObj objList)
{
	SetAnim("パンチ");
	attackFlag = true;
	AttackSet(ATTACK_TYPE::PUNCH, turnFlag);
	if (animEndFlag)
	{
		Character::move = &Character::Normal;
	}
	return true;
}

bool Character::Kick(const GameCtl & ctl, weekListObj objList)
{
	SetAnim("キック");
	attackFlag = true;
	AttackSet(ATTACK_TYPE::KICH, turnFlag);
	if (animEndFlag)
	{
		Character::move = &Character::Normal;
	}
	return true;
}

bool Character::Deth(const GameCtl & ctl, weekListObj objList)
{
	SetAnim("死亡");
	damageFlag = false;
	attackFlag = false;
	specialFlag = false;
	if (damageCnt > DAMAGE_CUT_MAX)
	{
		visible = false;
	}

	return true;
}

void Character::AttackSet(ATTACK_TYPE type, bool turnFlag)
{
	attackPos	= pos + attackPosTbl[turnFlag][static_cast<int>(type)] + 96;
	attackSize	= attackSizeTbl[static_cast<int>(type)] * 15/10;
}

bool Character::TblInit(void)
{
	VECTOR2 gameScreen = lpSceneMng.GetGameScreenSize();
	defPosTbl = { VECTOR2(gameScreen.x / 4, -64),
				  VECTOR2((gameScreen.x / 4) * 3 - 128, -64),
				  VECTOR2(gameScreen.x / 4, WALL_SIZE_Y / 2),
				  VECTOR2((gameScreen.x / 4) * 3 - 128,WALL_SIZE_Y / 2)
				};

	defTurnTbl = { false,
				   true,
				   false,
				   true
				 };

	defensePosTbl = { VECTOR2(44 * 15/10, 4 * 15/10),	// 右向き
					  VECTOR2(31 * 15/10, 4 * 15/10)	// 左向き
	};

	attackPosTbl = { VECTOR2(48, -24),	VECTOR2(84, 6),		VECTOR2(52, 36),	// 右向き
					 VECTOR2(-96, -24),	VECTOR2(-106, 6),	VECTOR2(-95, 36)	// 左向き
	};

	attackSizeTbl = { VECTOR2(32, 20),	VECTOR2(14, 20),	VECTOR2(28, 32) };

	VECTOR2 Screen = lpSceneMng.GetScreenSize();
	CutinPosTbl = { VECTOR2(0, 150),
					VECTOR2(Screen.x, 150),
					VECTOR2(0, Screen.y - 321),
					VECTOR2(Screen.x, Screen.y - 321)
	};

	return true;
}
