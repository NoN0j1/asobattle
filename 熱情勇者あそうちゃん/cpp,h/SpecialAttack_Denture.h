#pragma once
#include "Obj.h"

class SpecialAttack_Denture :
	public Obj
{
public:
	SpecialAttack_Denture(VECTOR2 drawOffset, VECTOR2 pos, VECTOR2 divSize, int popNo, int depth, bool turnFlag);
	~SpecialAttack_Denture();

	//ｱﾆﾒｰｼｮﾝの初期化
	bool initAnim(void);

	// ｵﾌﾞｼﾞｪｸﾄを削除して良いかのﾁｪｯｸ
	bool CheckDeth(void);

	//ｵﾌﾞｼﾞｪｸﾄのﾀｲﾌﾟが引数で指定したﾀｲﾌﾟか確かめる
	bool CheckObjType(OBJ_TYPE type);

	//当たり判定 引数:攻撃を受ける座標,攻撃を受けるｻｲｽﾞ,ｼﾞｬﾝﾌﾟしてるか
	bool CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth);

	//自分のﾌﾟﾚｲﾔｰ番号を取得する
	int GetPlayerNo(void);

	//描画処理
	void Draw(void);
private:
	//移動処理
	void SetMove(const GameCtl &ctl, weekListObj objList);

	int playerNo;		//ﾌﾟﾚｲﾔｰの番号

	bool turnFlag;		//反転ﾌﾗｸﾞ

	int depth;			//足元の座標

	bool dethFlag;		//削除して良いかのﾌﾗｸﾞ

	VECTOR2 attackPosTbl;	//攻撃用座標のﾃｰﾌﾞﾙ

	int attackRangeFront;	//攻撃範囲(手前)
	int attackRangeBack;	//攻撃範囲(奥)
};