#pragma once
#include "Obj.h"

#define NAIL_SIZE_X	(32)		//釘の横ｻｲｽﾞ
#define NAIL_SIZE_Y	(32)		//釘の縦ｻｲｽﾞ

class SpecialAttack_Nail :
	public Obj
{
public:
	SpecialAttack_Nail(VECTOR2 drawOffset, VECTOR2 pos, int popNo, int depth);
	~SpecialAttack_Nail();

	// ｵﾌﾞｼﾞｪｸﾄを削除して良いかのﾁｪｯｸ
	bool CheckDeth(void);

	//ｵﾌﾞｼﾞｪｸﾄのﾀｲﾌﾟが引数で指定したﾀｲﾌﾟか確かめる
	bool CheckObjType(OBJ_TYPE type);

	//当たり判定 引数:攻撃を受ける座標,攻撃を受けるｻｲｽﾞ,ｼﾞｬﾝﾌﾟしてるか
	bool CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth);

	//自分のﾌﾟﾚｲﾔｰ番号を取得する
	int GetPlayerNo(void);

	//描画処理
	void Draw(void);
private:
	//移動処理
	void SetMove(const GameCtl &ctl, weekListObj objList);

	int playerNo;		//ﾌﾟﾚｲﾔｰの番号

	int depth;			//足元の座標

	bool dethFlag;		//削除して良いかのﾌﾗｸﾞ

	int attackRangeFront;	//攻撃範囲(手前)
	int attackRangeBack;	//攻撃範囲(奥)

	int speed;				//ｽﾋﾟｰﾄﾞ
};