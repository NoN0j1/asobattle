#pragma once
#include<array>
#include<vector>
#include"CHARA_ID.h"
#include"BaseScene.h"

class ResultScene :
	public BaseScene
{
public:
	ResultScene();
	~ResultScene();
	//ｹﾞｰﾑﾓｰﾄﾞ時の処理　引数:現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀ,GameCtlｸﾗｽの情報　返り値として次のﾌﾚｰﾑ時のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す
	unique_Base Update(unique_Base own, const GameCtl &ctl);
private:
	int playerCnt;						// 人数
	std::vector<int> rank_player;		// 順位(ﾌﾟﾚｲﾔｰ順)
	std::vector<int> rank_top;			// 順位(高成績順)
	int topCnt;							// 1位の数

	std::array<VECTOR2, 8> rankPos;		// 順位の表示位置
	std::array<VECTOR2, 8> charaPos;	// ｷｬﾗｸﾀｰの表示位置
	std::array<std::array<char, 12>, static_cast<int>(CHARA_ID::MAX)> charaNameTbl;		//ｷｬﾗ名のﾃｰﾌﾞﾙ

	int timeCnt;

	int textFont;					//文字のﾌｫﾝﾄ

	//初期化
	int Init(void);
	//描画処理
	bool Draw(void);
};

