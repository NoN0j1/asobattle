#pragma once
#include "Character.h"

class Goukan8 :
	public Character
{
public:
	Goukan8(VECTOR2 drawOffset, int popNo, bool aiFlag);
	~Goukan8();

	//HPの最大値を取得する
	int GetHPMax(void);
private:
	//必殺技時の処理
	bool Special(const GameCtl &ctl, weekListObj objList);

	//必殺技時の描画
	void SpecialDraw(bool specialFlag);

	//ﾃﾞﾌｫﾙﾄの攻撃力を取得する
	int GetDefPower(void);
};