#pragma once
#include<map>
#include<vector>
#include<string>

class VECTOR2;

using VEC_INT = std::vector<int>;

#define lpImageMng ImageMng::GetInstance()
#define IMAGE_ID(X) (ImageMng::GetInstance().GetID(X))

class ImageMng
{
public:
	//外部からs_Instanceを参照するための関数
	static ImageMng &GetInstance(void)
	{
		static ImageMng s_Instance;					//ImageMngｸﾗｽのｱﾄﾞﾚｽを格納する変数
		return s_Instance;
	}

	//外部から画像のﾊﾝﾄﾞﾙを参照するための関数　//LoadGraph
	const VEC_INT& GetID(std::string f_name);
	//外部から画像のﾊﾝﾄﾞﾙを参照するための関数　//LoadDivGraph
	const VEC_INT& GetID(std::string f_name, VECTOR2 divSize, VECTOR2 divCut);

private:
	ImageMng();
	~ImageMng();

	std::map<std::string, VEC_INT> imgMap;			//画像のﾊﾝﾄﾞﾙを格納する連想配列
	
};

