#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "Goukan3.h"

#define GOUKAN3_HP_MAX				(100)		//HP�̍ő�l

#define GOUKAN3_DEF_SPEED			(5)			//��̫�Ă̈ړ���߰��

#define GOUKAN3_DEF_POWER			(10)		//��̫�Ă̍U����

#define GOUKAN3_SPECIAL_CNT_MAX		(60)		//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(920 / 5)	//�K�E�Z�̱�Ұ��݉�����
#define ANIM_SIZE_Y					(184)		//�K�E�Z�̱�Ұ��ݏc����
#define ANIM_CNT					(5)			//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(15)		//�K�E�Z�̱�Ұ����ڰ�

#define EFFECT_SIZE_X				(1600 / 10)	//�̪�Ẳ�����
#define EFFECT_SIZE_Y				(1200 / 4)	//�̪�Ă̏c����
#define EFFECT_CNT					(40)		//�̪�Ă̱�Ұ��ݐ�
#define EFFECT_FRAME				(2)			//�̪�Ă̱�Ұ����ڰ�

#define EFFECT_CNT_MAX				(90)		//�̪�Ă��o�Ă���K�E�Z�I���܂ł̶��Đ�

#define SP_POWER					(30)		//�K�E�Z�̍U����

Goukan3::Goukan3(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN3_HP_MAX;
	power = GOUKAN3_DEF_POWER;
	speed = GOUKAN3_DEF_SPEED;
	specialCntMax = GOUKAN3_SPECIAL_CNT_MAX;
	init("image/goukan3.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan3_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(5, 1));

	lpImageMng.GetID("image/effect/coin.png", VECTOR2(EFFECT_SIZE_X, EFFECT_SIZE_Y), VECTOR2(10, 4));

	lpImageMng.GetID("image/cutin/3gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	specialTimeCnt = 0;
	_RPT0(_CRT_WARN, "3���ق�ݽ�ݽ\n");
}

Goukan3::~Goukan3()
{
	_RPT0(_CRT_WARN, "3���ق��폜\n");
}

int Goukan3::GetHPMax(void)
{
	return GOUKAN3_HP_MAX;
}

bool Goukan3::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;
	attackRangeFront = 80;	//�U���͈�(��O)
	attackRangeBack  = 50;	//�U���͈�(��)
	power = SP_POWER;

	if (specialTimeCnt > CUTIN_END_FRAME + ANIM_FRAME * (ANIM_CNT - 1))
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/3goEX.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/3goEX.wav"), DX_PLAYTYPE_BACK, true);
		}
	}

	if (specialTimeCnt == (CUTIN_END_FRAME + ANIM_FRAME * (ANIM_CNT - 1)))
	{
		attackFlag = true;
		attackPos.x = (defensePos.x + (defenseSize.x / 2)) + ((ANIM_SIZE_X / 2) * ((!turnFlag) - turnFlag)) - (EFFECT_SIZE_X * (turnFlag));
		attackPos.y = depth - EFFECT_SIZE_Y * 15 / 10;
		attackSize = VECTOR2(EFFECT_SIZE_X * 15/10, EFFECT_SIZE_Y * 15/10);
	}

	if (specialTimeCnt >= (ANIM_FRAME * (ANIM_CNT - 1)) + EFFECT_CNT_MAX + CUTIN_END_FRAME  * 2)
	{
		StopSoundMem(lpSoundMng.GetID("se/3goEX.wav"));
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		power = GOUKAN3_DEF_POWER;
		attackRangeFront = DEF_ATTACK_FRONT;
		attackRangeBack = DEF_ATTACK_BACK;
		Character::move = &Goukan3::Normal;
	}

	specialTimeCnt++;

	return true;
}

void Goukan3::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	int animCnt = specialTimeCnt / ANIM_FRAME;
	if (animCnt >= ANIM_CNT - 1)
	{
		animCnt = ANIM_CNT - 1;
	}

	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - ANIM_SIZE_X,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) + (ANIM_SIZE_X * 15/10 / 2),
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan3_Special.png")[animCnt], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) + (ANIM_SIZE_X * 15 / 10 / 2) + 50,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15 / 10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - ANIM_SIZE_X + 50,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan3_Special.png")[animCnt], true);
	}

	if (attackFlag)
	{
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

		DrawOval(drawOffset.x + attackPos.x + (attackSize.x / 2), drawOffset.y + attackPos.y + attackSize.y, attackSize.x /2, 50, 0x000000, true);

		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

		int effectCnt = (specialTimeCnt - (ANIM_FRAME * (ANIM_CNT - 1))) / EFFECT_FRAME;
		if (effectCnt >= (EFFECT_CNT / 2))
		{
			effectCnt = (effectCnt % (EFFECT_CNT / 2)) + (EFFECT_CNT / 2);
		}
		DrawExtendGraph(
			drawOffset.x + attackPos.x,
			drawOffset.y + attackPos.y,
			drawOffset.x + attackPos.x + attackSize.x,
			drawOffset.y + attackPos.y + attackSize.y,
			lpImageMng.GetID("image/effect/coin.png")[effectCnt], true);

	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/3gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/3gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan3::GetDefPower(void)
{
	return GOUKAN3_DEF_POWER;
}
