#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "SpecialAttack_Bear.h"
#include "Goukan4.h"

#define GOUKAN4_HP_MAX				(100)	//HP�̍ő�l

#define GOUKAN4_DEF_SPEED			(6)		//��̫�Ă̈ړ���߰��

#define GOUKAN4_DEF_POWER			(9)		//��̫�Ă̍U����

#define GOUKAN4_SPECIAL_CNT_MAX		(80)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(920 / 5)		//�K�E�Z�̏c����
#define ANIM_SIZE_Y					(184)			//�K�E�Z�̏c����
#define ANIM_CNT					(5)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(20)			//�K�E�Z�̱�Ұ��݊Ԋu

Goukan4::Goukan4(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN4_HP_MAX;
	power = GOUKAN4_DEF_POWER;
	speed = GOUKAN4_DEF_SPEED;
	specialCntMax = GOUKAN4_SPECIAL_CNT_MAX;
	init("image/goukan4.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan4_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(5, 1));

	lpImageMng.GetID("image/cutin/4gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	_RPT0(_CRT_WARN, "4���ق�ݽ�ݽ\n");
}

Goukan4::~Goukan4()
{
	_RPT0(_CRT_WARN, "4���ق��폜\n");
}

int Goukan4::GetHPMax(void)
{
	return GOUKAN4_HP_MAX;
}

bool Goukan4::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	if (specialTimeCnt == ANIM_FRAME * (ANIM_CNT - 1))
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/4goEX01.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/4goEX01.wav"), DX_PLAYTYPE_BACK, true);
		}
		AddObjList()(objList, std::make_shared<SpecialAttack_Bear>(drawOffset, pos, playerNo, depth));
	}

	if (specialTimeCnt >= ANIM_FRAME * (ANIM_CNT - 1))
	{
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		Character::move = &Goukan4::Normal;
	}

	specialTimeCnt++;

	return true;
}

void Goukan4::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 60,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15/10) - 60,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan4_Special.png")[specialTimeCnt / ANIM_FRAME], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15 / 10) - 30,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15 / 10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 30,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan4_Special.png")[specialTimeCnt / ANIM_FRAME], true);
	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/4gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/4gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan4::GetDefPower(void)
{
	return GOUKAN4_DEF_POWER;
}
