#include"DxLib.h"
#include"ImageMng.h"
#include"SoundMng.h"
#include"CrossProduct_2D.h"
#include"SceneMng.h"
#include"SpecialAttack_Denture.h"

#define WALL_SIZE_Y	(135)		//壁の縦ｻｲｽﾞ
#define SPEED		(12)		//ﾃﾞﾌｫﾙﾄの移動ｽﾋﾟｰﾄﾞ

#define DIV_X (600)				//分割の拡大後ｻｲｽﾞ
#define DIV_Y (225)				//分割の拡大後ｻｲｽﾞ

SpecialAttack_Denture::SpecialAttack_Denture(VECTOR2 drawOffset, VECTOR2 pos, VECTOR2 divSize, int popNo, int depth, bool turnFlag) :Obj(drawOffset)
{
	init("image/effect/1gou_exAttack.png", VECTOR2(2000 / 5, 1500 / 10), VECTOR2(5, 10));

	initAnim();

	dethFlag = false;

	attackSize = VECTOR2(DIV_X / 4, DIV_Y);

	attackPosTbl = VECTOR2(((DIV_X / 4) * 3) * (!turnFlag), 0);

	if (!turnFlag)
	{
		this->pos.x = (pos.x + 200 + attackSize.x) - DIV_X;
	}
	else
	{
		this->pos.x = pos.x - attackSize.x;
	}

	this->pos.y = depth - DIV_Y;

	playerNo = popNo;
	this->depth = depth;
	this->turnFlag = turnFlag;

	attackRangeFront = 60;
	attackRangeBack	 = 60;

	power = 40;
	PlaySoundMem(lpSoundMng.GetID("se/1goEX.wav"), DX_PLAYTYPE_LOOP, true);
}

SpecialAttack_Denture::~SpecialAttack_Denture()
{
}

bool SpecialAttack_Denture::initAnim(void)
{
	AddAnim("初期", 0, 0, 30, 1, false);
	AddAnim("移動", 0, 6, 20, 1, true);
	SetAnim("初期");
	return true;
}

bool SpecialAttack_Denture::CheckDeth(void)
{
	if (dethFlag)
	{
		StopSoundMem(lpSoundMng.GetID("se/1goEX.wav"));
	}
	return dethFlag;
}

bool SpecialAttack_Denture::CheckObjType(OBJ_TYPE type)
{
	return (OBJ_TYPE_SP_ATTACK == type);
}

bool SpecialAttack_Denture::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth)
{
	if (this->depth + attackRangeFront >= depth && this->depth - attackRangeBack <= depth)
	{
		if (this->attackPos.x < defensePos.x + defenseSize.x
			&&this->attackPos.x + attackSize.x > defensePos.x
			&&this->attackPos.y < defensePos.y + defenseSize.y
			&&this->attackPos.y + attackSize.y > defensePos.y)
		{
			dethFlag = true;
			return true;
		}
	}
	return false;
}

int SpecialAttack_Denture::GetPlayerNo(void)
{
	return playerNo;
}

void SpecialAttack_Denture::Draw(void)
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	DrawOval(drawOffset.x + attackPos.x + (attackSize.x / 2), drawOffset.y + depth - 5, attackSize.x / 2, 15, 0x000000, true);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	if (turnFlag)
	{
		if (imageName.length() == 0)
		{
			return;
		}
		unsigned int id = 0;
		if (animTable.find(animName) != animTable.end())
		{
			int count = animCnt / animTable[animName][ANIM_TBL_INV];
			if (animTable[animName][ANIM_TBL_LOOP] || count < animTable[animName][ANIM_TBL_FRAME])
			{
				count %= animTable[animName][ANIM_TBL_FRAME];
			}
			else
			{
				count = animTable[animName][ANIM_TBL_FRAME] - 1;
				animEndFlag = true;
			}

			id = animTable[animName][ANIM_TBL_START_ID] + count;
		}

		if (id < IMAGE_ID(imageName).size())
		{
			if (visible)
			{
				DrawExtendGraph(
					drawOffset.x + pos.x + DIV_X,
					drawOffset.y + pos.y,
					drawOffset.x + pos.x,
					drawOffset.y + pos.y + DIV_Y,
					IMAGE_ID(imageName)[id], true);

			}
		}
		animCnt++;

	}
	else
	{
		Obj::Draw();
	}

#ifdef _DEBUG
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);
	//攻撃用判定
	DrawBox(drawOffset.x + attackPos.x, drawOffset.y + attackPos.y,
		drawOffset.x + attackPos.x + attackSize.x + 1, drawOffset.y + attackPos.y + attackSize.y + 1,
		0x0000ff,
		true);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
#endif
}

struct CheckWall
{
	bool operator()(VECTOR2 pos, VECTOR2 addPos, VECTOR2 size, int depth)
	{
		if (depth >= WALL_SIZE_Y)
		{
			return true;
		}

		VECTOR2 tmpPos(pos.x + addPos.x, depth + addPos.y - size.y);

		VECTOR2 startPos_L(0, 135);
		VECTOR2 startPos_R(1400, 135);

		if (CrossProduct_2D()(startPos_L, VECTOR2(tmpPos.x, tmpPos.y + size.y), VECTOR2(startPos_L.x + 135, startPos_L.y - 135)) > 0)
		{
			return false;
		}

		if (CrossProduct_2D()(startPos_R, tmpPos + size, startPos_R - 135) < 0)
		{
			return false;
		}
		return true;
	}
};

void SpecialAttack_Denture::SetMove(const GameCtl & ctl, weekListObj objList)
{
	attackPos = pos + attackPosTbl;

	if (animEndFlag)
	{
		SetAnim("移動");
	}
	pos.x -= SPEED * (turnFlag - (!turnFlag));

	if (!(CheckWall()(pos, attackPosTbl, attackSize, depth)))
	{
		dethFlag = true;
	}
	if (pos.x<-DIV_X || pos.x>lpSceneMng.GetGameScreenSize().x)
	{
		dethFlag = true;
	}
}