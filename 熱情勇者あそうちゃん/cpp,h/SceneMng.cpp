#include"DxLib.h"
#include"VECTOR2.h"
#include"GameCtl.h"
#include"Obj.h"
#include "SceneChange.h"
#include"CharacterSelectScene.h"
#include"GameScene.h"
#include"SceneMng.h"
#include "TitleScene.h"

#define SCREEN_SIZE_X (1440)			//ｽｸﾘｰﾝの横ｻｲｽﾞ
#define SCREEN_SIZE_Y (810)				//ｽｸﾘｰﾝの縦ｻｲｽﾞ

SceneMng::SceneMng()
{
	SysInit();
	_RPT0(_CRT_WARN, "SceneMngｸﾗｽをｲﾝｽﾀﾝｽ\n");
}

SceneMng::~SceneMng()
{
	_RPT0(_CRT_WARN, "SceneMngｸﾗｽを削除\n");
}

void SceneMng::Run(void)
{
	activScene = std::make_unique<TitleScene>();

	// ---------- ｹﾞｰﾑﾙｰﾌﾟ
	while (ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0)
	{
		//情報更新
		gameCtl->SetUpPad(playerCnt);
		gameCtl->UpDate();
		activScene = activScene->Update(std::move(activScene), (*gameCtl));
	}
}

bool SceneMng::SetGameScreenSize(VECTOR2 vec)
{
	gameScreen = vec;
	return true;
}

const VECTOR2 SceneMng::GetGameScreenSize(void)
{
	return gameScreen;
}

const VECTOR2 SceneMng::GetScreenSize(void)
{
	return VECTOR2(SCREEN_SIZE_X,SCREEN_SIZE_Y);
}

bool SceneMng::SetDrawOffset(VECTOR2 vec)
{
	drawOffset = vec;
	return true;
}

const VECTOR2 & SceneMng::GetDrawOffset(void)
{
	return drawOffset;
}

int SceneMng::GetPlayerCnt(void)
{
	return playerCnt;
}

void SceneMng::SetPlayerCnt(int playerCnt)
{
	SceneMng::playerCnt = playerCnt;

	playerId.resize(4);
	PlayerIdInit();
	decidFlag.resize(playerCnt);
	DecidFlagInit();
}

CHARA_ID SceneMng::GetPlayerId(int playerNo)
{
	return playerId[playerNo];
}

void SceneMng::SetPlayerId(int playerNo, CHARA_ID id)
{
	playerId[playerNo] = id;
}

bool SceneMng::GetDecidFlag(int playerNo)
{
	return decidFlag[playerNo];
}

void SceneMng::SetDecidFlag(int playerNo, bool decidFlag)
{
	SceneMng::decidFlag[playerNo] = decidFlag;
}

bool SceneMng::GetTutorialFlag(void)
{
	return tutorialFlag;
}

void SceneMng::SetTutorialFlag(bool tutorialFlag)
{
	SceneMng::tutorialFlag = tutorialFlag;
}

bool SceneMng::RankInit(void)
{
	rank.resize(4);
	for (int i = 0; i < rank.size(); i++)
	{
		rank[i] = 0;
	}
	return true;
}

void SceneMng::SetRank(int playerNo, int rank)
{
	this->rank[playerNo] = rank;
}

int SceneMng::GetRank(int playerNo)
{
	return rank[playerNo];
}

bool SceneMng::SysInit(void)
{
	// ｼｽﾃﾑ処理
	SetGraphMode(SCREEN_SIZE_X, SCREEN_SIZE_Y, 16);		// 65536色ﾓｰﾄﾞに設定
	ChangeWindowMode(true);					// true:window　false:ﾌﾙｽｸﾘｰﾝ
	SetWindowText("熱情勇者あそうちゃん 活劇狂想曲");
	if (DxLib_Init() == -1) return false;	// DXﾗｲﾌﾞﾗﾘ初期化処理
	SetDrawScreen(DX_SCREEN_BACK);			// ひとまずﾊﾞｯｸﾊﾞｯﾌｧに描画
	gameCtl = std::make_unique<GameCtl>();	//GameCtlｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを生成する
	tutorialFlag = false;					//ﾁｭｰﾄﾘｱﾙの時だけtrueだから最初はfalseにしておく

	SetPlayerCnt(1);
	return true;
}

void SceneMng::PlayerIdInit(void)
{
	for (int specialTimeCnt = 0; specialTimeCnt < playerId.size(); specialTimeCnt++)
	{
		playerId[specialTimeCnt] = CHARA_ID::NON;
	}
}

void SceneMng::DecidFlagInit(void)
{
	for (int specialTimeCnt = 0; specialTimeCnt < playerCnt; specialTimeCnt++)
	{
		decidFlag[specialTimeCnt] = false;
	}
}

int DrawLine(VECTOR2 vec1, VECTOR2 vec2, unsigned int Color, int Thickness)
{
	DxLib::DrawLine(vec1.x, vec1.y, vec2.x, vec2.y, Color, Thickness);
	return 0;
}
