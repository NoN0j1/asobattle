#pragma once
#include<list>
#include<vector>
#include<memory>
#include"VECTOR2.h"
#include"CHARA_ID.h"
#include"BaseScene.h"

class GameCtl;
class SceneChange;

#define lpSceneMng SceneMng::GetInstance()

int	DrawLine(VECTOR2 vec1, VECTOR2 vec2, unsigned int Color, int  Thickness = 1);

class SceneMng
{
public:
	//外部からs_Instanceを参照するための関数
	static SceneMng &GetInstance(void)
	{
		static SceneMng s_Instance;				//SceneMngｸﾗｽのｱﾄﾞﾚｽを格納する変数
		return s_Instance;
	}
	//ｹﾞｰﾑﾙｰﾌﾟ　//activSceneに格納されているｸﾗｽのUpdate関数を実行する
	void Run(void);

	//ｹﾞｰﾑ画面のｻｲｽﾞをｾｯﾄする
	bool SetGameScreenSize(VECTOR2 vec);
	//ｹﾞｰﾑ画面のｻｲｽﾞを取得する関数
	const VECTOR2 GetGameScreenSize(void);

	// 画面ｻｲｽﾞを取得する
	const VECTOR2 GetScreenSize(void);

	//ｵﾌｾｯﾄの情報をｾｯﾄする
	bool SetDrawOffset(VECTOR2 vec);
	//ｵﾌｾｯﾄの情報を取得する
	const VECTOR2 &GetDrawOffset(void);

	//ﾌﾟﾚｲ人数の情報を取得する
	int GetPlayerCnt(void);
	//ﾌﾟﾚｲ人数の情報をｾｯﾄする(引数：ｾｯﾄしたいﾌﾟﾚｲ人数)
	void SetPlayerCnt(int playerCnt);

	//ｷｬﾗIDの情報を取得する(引数：ｷｬﾗIDを取得したいﾌﾟﾚｲﾔｰの番号)
	CHARA_ID GetPlayerId(int playerNo);
	//ｷｬﾗIDの情報をｾｯﾄする(引数：ｷｬﾗIDをｾｯﾄしたいﾌﾟﾚｲﾔｰの番号, ｾｯﾄしたいｷｬﾗID)
	void SetPlayerId(int playerNo, CHARA_ID id);

	//decidFlagの情報を取得する(引数：decidFlagを取得したいﾌﾟﾚｲﾔｰの番号)
	bool GetDecidFlag(int playerNo);
	//decidFlagの情報をｾｯﾄする(引数：decidFlagをｾｯﾄしたいﾌﾟﾚｲﾔｰの番号, ｾｯﾄしたいflag)
	void SetDecidFlag(int playerNo, bool decidFlag);

	//tutorialFlagの情報を取得する
	bool GetTutorialFlag(void);
	//tutorialFlagの情報をｾｯﾄする(引数：ｾｯﾄしたいflag)
	void SetTutorialFlag(bool tutorialFlag);

	//順位の初期化
	bool RankInit(void);
	//順位のｾｯﾄ　引数:ﾌﾟﾚｲﾔｰの番号,順位
	void SetRank(int playerNo, int rank);
	//順位の取得　引数:ﾌﾟﾚｲﾔｰの番号
	int GetRank(int playerNo);
private:
	SceneMng();
	~SceneMng();
	VECTOR2 gameScreen;							//ｹﾞｰﾑｽｸﾘｰﾝのｻｲｽﾞ
	VECTOR2 drawOffset;							//ｵﾌｾｯﾄの情報

	std::unique_ptr<GameCtl> gameCtl;			//GameCtlｸﾗｽのｱﾄﾞﾚｽを格納するﾕﾆｰｸﾎﾟｲﾝﾀ

	unique_Base activScene;						//実行するｼｰﾝｸﾗｽを格納するBaseScene型のﾕﾆｰｸﾎﾟｲﾝﾀ;

	bool tutorialFlag;					//ﾁｭｰﾄﾘｱﾙかどうかのﾌﾗｸﾞ　true:ﾁｭｰﾄﾘｱﾙ, false:ﾁｭｰﾄﾘｱﾙ以外

	int playerCnt;						//ﾌﾟﾚｲ人数
	std::vector<CHARA_ID> playerId;		//各ﾌﾟﾚｲﾔｰが持っているｷｬﾗID

	std::vector<bool> decidFlag;		//ｷｬﾗ選択を決定しているかどうかのﾌﾗｸﾞ　true:決定している, false:決定してはいない

	std::vector<int> rank;				//順位

	//ｼｽﾃﾑ系の初期化
	bool SysInit(void);

	//各ﾌﾟﾚｲﾔｰが持っているｷｬﾗIDの初期化
	void PlayerIdInit(void);
	//各ﾌﾟﾚｲﾔｰが持っているdecidFlagの初期化
	void DecidFlagInit(void);
};

