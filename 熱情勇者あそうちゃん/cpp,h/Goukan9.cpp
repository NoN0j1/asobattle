#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "Goukan9.h"

#define GOUKAN9_HP_MAX				(120)	//HP�̍ő�l

#define GOUKAN9_DEF_SPEED			(4)		//��̫�Ă̈ړ���߰��

#define GOUKAN9_DEF_POWER			(10)	//��̫�Ă̍U����

#define GOUKAN9_SPECIAL_CNT_MAX		(100)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_Y					(278)			//�K�E�Z�̏c����
#define ANIM_CNT					(2)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(4)				//�K�E�Z�̱�Ұ��݊Ԋu

#define EFFECT_CNT					(20)			//�̪�Ă̱�Ұ��ݐ�
#define EFFECT_SIZE_X				(3000 / 10)		//�̪�Ẳ�����
#define EFFECT_SIZE_Y				(600 / 2)		//�̪�Ă̏c����
#define EFFECT_FRAME				(2)				//�̪�Ă̱�Ұ��݊Ԋu

#define CNT_MAX						(167)			//�K�E�Z�̔�������

#define SP_POWER					(50)			//�K�E�Z�̍U����

Goukan9::Goukan9(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN9_HP_MAX;
	power = GOUKAN9_DEF_POWER;
	speed = GOUKAN9_DEF_SPEED;
	specialCntMax = GOUKAN9_SPECIAL_CNT_MAX;
	init("image/goukan9.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan9_Special.png", VECTOR2(548 / 2, ANIM_SIZE_Y), VECTOR2(2, 1));

	lpImageMng.GetID("image/effect/hyakuretu.png", VECTOR2(EFFECT_SIZE_X, EFFECT_SIZE_Y), VECTOR2(10, 2));

	lpImageMng.GetID("image/cutin/9gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	specialTimeCnt = 0;

	_RPT0(_CRT_WARN, "9���ق�ݽ�ݽ\n");
}

Goukan9::~Goukan9()
{
	_RPT0(_CRT_WARN, "9���ق��폜\n");
}

int Goukan9::GetHPMax(void)
{
	return GOUKAN9_HP_MAX;
}

bool Goukan9::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;
	power = SP_POWER;
	attackRangeBack = 60;


	if (specialTimeCnt > CUTIN_END_FRAME)
	{
		attackFlag = true;
	}
	else
	{
		//��
		if (CheckSoundMem(lpSoundMng.GetID("bgm/hokuto.mp3")) == 0)
		{
			StopSoundMem(lpSoundMng.GetID("bgm/battle.mp3"));
			StopSoundMem(lpSoundMng.GetID("bgm/10goEX01.wav"));
			PlaySoundMem(lpSoundMng.GetID("bgm/hokuto.mp3"), DX_PLAYTYPE_BACK, true);
		}
		//----------
	}

	attackSize = VECTOR2(EFFECT_SIZE_X, EFFECT_SIZE_Y);

	if (turnFlag)
	{
		attackPos = defensePos - attackSize;
		attackPos.y += defenseSize.y;
	}
	else
	{
		attackPos = defensePos + defenseSize;
		attackPos.y -= attackSize.y;
	}

	if (specialTimeCnt > CNT_MAX + CUTIN_END_FRAME)
	{
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		power = GOUKAN9_DEF_POWER;
		attackRangeBack = DEF_ATTACK_BACK;
	
		Character::move = &Goukan9::Normal;

		//��
		StopSoundMem(lpSoundMng.GetID("bgm/hokuto.mp3"));
		//----------
	}

	specialTimeCnt++;

	return true;
}

void Goukan9::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	if (!turnFlag)
	{
		if (specialTimeCnt < CUTIN_END_FRAME)
		{
			DrawExtendGraph(
				drawOffset.x + pos.x,
				drawOffset.y + (depth - divSize.y * 15/10),
				drawOffset.x + pos.x + divSize.x * 15/10 + 1,
				drawOffset.y + depth + 1,
				lpImageMng.GetID("image/goukan9_Special.png")[0], true);
		}
		else
		{
			DrawExtendGraph(
				drawOffset.x + pos.x,
				drawOffset.y + (depth - divSize.y * 15/10),
				drawOffset.x + pos.x + divSize.x * 15/10 + 1,
				drawOffset.y + depth + 1,
				lpImageMng.GetID("image/goukan9_Special.png")[(specialTimeCnt / ANIM_FRAME) % ANIM_CNT], true);
			DrawGraph(drawOffset.x + attackPos.x, drawOffset.y + attackPos.y, lpImageMng.GetID("image/effect/hyakuretu.png")[specialTimeCnt / EFFECT_FRAME % EFFECT_CNT], true);
		}
	}
	else
	{
		if (specialTimeCnt < CUTIN_END_FRAME)
		{
			DrawExtendGraph(
				drawOffset.x + pos.x + divSize.x * 15 / 10 + 1,
				drawOffset.y + (depth - divSize.y * 15/10),
				drawOffset.x + pos.x,
				drawOffset.y + depth + 1,
				lpImageMng.GetID("image/goukan9_Special.png")[0], true);
		}
		else
		{
			DrawExtendGraph(
				drawOffset.x + pos.x + divSize.x * 15 / 10 + 1,
				drawOffset.y + (depth - divSize.y * 15/10),
				drawOffset.x + pos.x,
				drawOffset.y + depth + 1,
				lpImageMng.GetID("image/goukan9_Special.png")[(specialTimeCnt / ANIM_FRAME) % ANIM_CNT], true);
			DrawTurnGraph(drawOffset.x + attackPos.x, drawOffset.y + attackPos.y, lpImageMng.GetID("image/effect/hyakuretu.png")[specialTimeCnt / EFFECT_FRAME % EFFECT_CNT], true);
		}
	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/9gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/9gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan9::GetDefPower(void)
{
	return GOUKAN9_DEF_POWER;
}
