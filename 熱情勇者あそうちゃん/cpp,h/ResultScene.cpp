#include"DxLib.h"
#include"SceneMng.h"
#include"ImageMng.h"
#include"SoundMng.h"
#include"GameCtl.h"
#include"TitleScene.h"
#include"ResultScene.h"
#include"SceneChange.h"

ResultScene::ResultScene()
{
	Init();
	_RPT0(_CRT_WARN, "ResultSceneｸﾗｽをｲﾝｽﾀﾝｽ\n");
}

ResultScene::~ResultScene()
{
	DeleteFontToHandle(textFont);
	_RPT0(_CRT_WARN, "ResultSceneｸﾗｽを削除\n");
}

unique_Base ResultScene::Update(unique_Base own, const GameCtl & ctl)
{
	// 音
	if (CheckSoundMem(lpSoundMng.GetID("se/clap.mp3")) == 0)
	{
		PlaySoundMem(lpSoundMng.GetID("se/clap.mp3"), DX_PLAYTYPE_LOOP, true);
	}
	if (CheckSoundMem(lpSoundMng.GetID("bgm/result.mp3")) == 0)
	{
		PlaySoundMem(lpSoundMng.GetID("bgm/result.mp3"), DX_PLAYTYPE_LOOP, true);
	}
	//----------

	if (timeCnt >= 60 * 1)
	{
		for (int p = 0; p < 4; p++)
		{
			if (ctl.GetPadTrigger(p, PAD_INPUT_8) || SceneChangeFlag)
			{
				SceneChangeFlag = true;
				if (lpSceneChange.FadeOut(2, 0, true))
				{
					//音
					StopSoundMem(lpSoundMng.GetID("bgm/result.mp3"));
					StopSoundMem(lpSoundMng.GetID("se/clap.mp3"));
					//----------
					return std::make_unique<TitleScene>();
				}

			}
		}
	}

	Draw();

	timeCnt++;

	return own;
}

int ResultScene::Init(void)
{
	playerCnt = 4/*lpSceneMng.GetPlayerCnt()*/;

	// ｼｰﾝﾏﾈｰｼﾞｬｰに記憶されているrankの情報をｺﾋﾟｰ、同時に1位の数も数えておく
	rank_player.resize(playerCnt);
	topCnt = 0;

	for (int p = 0; p < playerCnt; p++)
	{
		rank_player[p] = lpSceneMng.GetRank(p);
		if (rank_player[p] == 1)
		{
			topCnt++;
		}
	}

	// rank_topには成績の高い順にﾌﾟﾚｲﾔｰの番号格納
	rank_top.resize(playerCnt);
	int cnt = 0;

	for (int r = 1; r <= playerCnt; r++)
	{
		for (int p = 0; p < playerCnt; p++)
		{
			if (rank_player[p] == r)
			{
				rank_top[cnt] = p;
				cnt++;
			}
		}
	}

	// ﾃｰﾌﾞﾙ
	rankPos = { VECTOR2(35,135), VECTOR2(10,430),
				VECTOR2(495,430), VECTOR2(985,430),
				VECTOR2(5,0), VECTOR2(745,0),
				VECTOR2(5,430), VECTOR2(745,430) };

	charaPos = { VECTOR2(380,200), VECTOR2(315,630),
				 VECTOR2(800,630), VECTOR2(1290,630),
				 VECTOR2(410,185), VECTOR2(1150,185),
				 VECTOR2(410,615), VECTOR2(1150,615) };

	charaNameTbl = { " ひーさん",
					 "　ニ　コ",
					 " ミ ッ ツ",
					 " ヨ ウ コ",
					 "ゴトウさん",
					 "ムッちゃん",
					 "なっちゃん",
					 " ハチベエ",
					 " ア ニ キ",
					 "　会　長",
					 " 鬼 軍 曹",
					 " ランダム",
					 " 未 選 択" };

	// 文字ｻｲｽﾞのｾｯﾄ
	SetFontSize(116);

	timeCnt = 0;

	textFont = CreateFontToHandle(NULL, 50, 10, DX_FONTTYPE_EDGE);

	return 0;
}

bool ResultScene::Draw(void)
{
	//描画処理
	ClsDrawScreen();		// 画面消去

	// 背景
	DrawGraph(0, 0, lpImageMng.GetID("image/result_bg.png", VECTOR2(1440, 810), VECTOR2(2, 1))[(topCnt > 1)], true);

	int playerNum;
	double exRate;

	for (int r = 0; r < playerCnt; r++)
	{
		playerNum = rank_top[r];
		if ((topCnt == 1) && (rank_player[playerNum] == 1))
		{
			DrawFormatString(550, 165, 0x334ce7, "%d PLAYER", playerNum + 1);
			DrawFormatString(850, 270, 0xea17e8, "%s", charaNameTbl[static_cast<int>(lpSceneMng.GetPlayerId(playerNum))]);
			exRate = 1.5f;
		}
		else
		{
			exRate = 1.2f;
		}

		DrawFormatString(rankPos[r + ((topCnt > 1) * 4)].x, rankPos[r + ((topCnt > 1) * 4)].y, 0xe73333 - (0x340c0c * (rank_player[playerNum] - 1)), "%d位", rank_player[playerNum]);
		DrawRotaGraph(charaPos[r + ((topCnt > 1) * 4)].x, charaPos[r + ((topCnt > 1) * 4)].y, exRate, 0, lpImageMng.GetID("image/status_character.png", VECTOR2(180, 270), VECTOR2(3, 4))[static_cast<int>(lpSceneMng.GetPlayerId(playerNum))], true);
	}

	if (timeCnt >= 60 * 2)
	{
		if ((timeCnt / 30) % 2)
		{
			VECTOR2 screenSize = lpSceneMng.GetScreenSize();
			int StrLen = (int)strlen("ＰＬＥＡＳＥ ＰＵＳＨ ＳＴＡＲＴ ＢＵＴＴＯＮ");
			int StrWidth = GetDrawStringWidthToHandle("ＰＬＥＡＳＥ ＰＵＳＨ ＳＴＡＲＴ ＢＵＴＴＯＮ", StrLen, textFont);
			DrawStringToHandle((screenSize.x / 2) - (StrWidth / 2), screenSize.y / 2, "ＰＬＥＡＳＥ ＰＵＳＨ ＳＴＡＲＴ ＢＵＴＴＯＮ", 0xffffff, textFont);
		}
	}

	ScreenFlip();
	return true;
}
