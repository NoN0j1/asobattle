#include"DxLib.h"
#include"SceneMng.h"
#include "ImageMng.h"
#include "Obj.h"

Obj::Obj()
{
	attackFlag = false;
	attackPos = pos;
	attackSize = divSize;
	defensePos = pos;
	defenseSize = divSize;

	power = 0;

	visible = true;
}

Obj::Obj(VECTOR2 drawOffset):drawOffset(drawOffset)
{
	attackFlag = false;
	attackPos = pos;
	attackSize = divSize;
	defensePos = pos;
	defenseSize = divSize;

	power = 0;

	visible = true;
}

bool Obj::init(std::string fileName, VECTOR2 divSize, VECTOR2 divCut)
{
	lpImageMng.GetID(fileName, divSize, divCut);
	imageName = fileName;
	this->divSize = divSize;
	this->divCut = divCut;
	return true;
}

bool Obj::init(std::string fileName, VECTOR2 divSize, VECTOR2 divCut, VECTOR2 pos)
{
	init(fileName, divSize, divCut);
	SetPos(pos);
	return true;
}

Obj::~Obj()
{
}

void Obj::UpDate(const GameCtl &ctl)
{
	SetMove(ctl);
}

void Obj::UpDate(const GameCtl & ctl, weekListObj objList)
{
	SetMove(ctl, objList);
}

void Obj::Draw(void)
{
	if (imageName.length() == 0)
	{
		return;
	}
	unsigned int id = 0;
	if (animTable.find(animName) != animTable.end())
	{
		int count = animCnt / animTable[animName][ANIM_TBL_INV];
		if (animTable[animName][ANIM_TBL_LOOP] || count < animTable[animName][ANIM_TBL_FRAME])
		{
			count %= animTable[animName][ANIM_TBL_FRAME];
		}
		else
		{
			count = animTable[animName][ANIM_TBL_FRAME] - 1;
			animEndFlag = true;
		}

		id = animTable[animName][ANIM_TBL_START_ID] + count;
	}

	if (id < IMAGE_ID(imageName).size())
	{
		if (visible)
		{
#ifdef _DEBUG
			DrawGraph(drawOffset.x + pos.x, drawOffset.y + pos.y, IMAGE_ID(imageName)[id], true);
#endif
			DrawExtendGraph(drawOffset.x + pos.x, 
							drawOffset.y + pos.y,
							drawOffset.x + pos.x + divSize.x * 15/10,
							drawOffset.y + pos.y + divSize.y * 15/10,
							IMAGE_ID(imageName)[id], true);
		}
	}
	animCnt++;
#ifdef _DEBUG
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);
	DrawBox(drawOffset.x + attackPos.x, drawOffset.y + attackPos.y,
		drawOffset.x + attackPos.x + attackSize.x + 1, drawOffset.y + attackPos.y + attackSize.y + 1,
		0x0000ff,
		true);
	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
#endif
}

void Obj::Draw(unsigned int id)
{
	if (imageName.length() == 0)
	{
		return;
	}
	if (id < IMAGE_ID(imageName).size())
	{
		if (visible)
		{
			DrawGraph(drawOffset.x + pos.x, drawOffset.y + pos.y, IMAGE_ID(imageName)[id], true);
		}
	}
}

const VECTOR2 & Obj::GetPos(void)
{
	return pos;
}

void Obj::AddPos(VECTOR2 addPos)
{
	pos += addPos;
}

int Obj::GetPlayerNo(void)
{
	return 0;
}

bool Obj::GetStandbyFlag(void)
{
	return false;
}

std::string Obj::GetImageName(void)
{
	return imageName;
}

bool Obj::AddAnim(std::string animName, int id_x, int id_y, int frame, int inv, bool loop)
{
	animTable[animName][ANIM_TBL_START_ID] = (id_y * divCut.x) + (id_x);
	animTable[animName][ANIM_TBL_FRAME] = frame;
	animTable[animName][ANIM_TBL_INV] = inv;
	animTable[animName][ANIM_TBL_LOOP] = loop;
	return true;
}

bool Obj::SetAnim(std::string animName)
{
	if (Obj::animName == animName)
	{
		return true;
	}
	if (animTable.find(animName) == animTable.end())
	{
		return false;
	}

	Obj::animName = animName;
	Obj::animCnt = 0;
	Obj::animEndFlag = false;
	return true;

}

std::string Obj::GetAnim(void)
{
	return animName;
}

bool Obj::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize)
{
	if (this->pos.x < defensePos.x + defenseSize.x
		&&this->pos.x + divSize.x > defensePos.x
		&&this->pos.y < defensePos.y + defenseSize.y
		&&this->pos.y + divSize.y > defensePos.y)
	{
		return true;
	}
	return false;
}

bool Obj::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth)
{
	return CheckHit(defensePos, defenseSize);
}

bool Obj::Hit(const int ATK, int knockBack)
{
	Damage(ATK);
	return true;
}

bool Obj::Damage(const int ATK)
{
	HP -= ATK;
	if (HP <= 0)
	{
		HP = 0;
	}
	_RPTN(_CRT_WARN, "Hit Damage %d HP%d \n", ATK, HP);
	return true;
}

VECTOR2 Obj::GetDefensePos(void)
{
	return defensePos;
}

VECTOR2 Obj::GetDefenseSize(void)
{
	return defenseSize;
}

const int Obj::GetPower(void)
{
	return power;
}

int Obj::GetDepth(void)
{
	return pos.y + divSize.y;
}

void Obj::SetHP(int hp)
{
	HP = hp;
}

int Obj::GetHP(void)
{
	return HP;
}

int Obj::GetHPMax(void)
{
	return 0;
}

int Obj::GetSpecialCnt(void)
{
	return 0;
}

void Obj::AddSpecialCnt(int specialUp)
{
}

int Obj::GetSpecialUp(void)
{
	return 0;
}

int Obj::GetSpecialCntMax(void)
{
	return 0;
}

void Obj::SetFallFlag(bool flag)
{
	fallFlag = flag;
}

const bool Obj::GetFallFlag()
{
	return fallFlag;
}

int Obj::GetKnockBack(void)
{
	return 0;
}

const bool Obj::GetDamageHP(void)
{
	return (HP != OldHP);
}

int Obj::GetSpeed(void)
{
	return 0;
}

bool Obj::GetSpecialFlag(void)
{
	return false;
}

void Obj::SetSPCutinFlag(bool flg)
{
}

void Obj::SetPos(VECTOR2 pos)
{
	this->pos = pos;
}

void Obj::SetMove(const GameCtl & ctl)
{
}

void Obj::SetMove(const GameCtl & ctl, weekListObj objList)
{
}
