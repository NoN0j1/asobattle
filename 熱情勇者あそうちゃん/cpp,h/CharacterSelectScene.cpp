#include<time.h>
#include<stdlib.h>
#include"DxLib.h"
#include"CHARA_ID.h"
#include"Character.h"
#include"GameCtl.h"
#include"SceneMng.h"
#include"ImageMng.h"
#include"SoundMng.h"
#include"SceneChange.h"
#include"MainMenuScene.h"
#include"GameScene.h"
#include"Obj.h"
#include"CharacterSelectCursor.h"
#include"CharacterSelectScene.h"

#define SCREEN_SIZE_X (1440)			// ｽｸﾘｰﾝの横ｻｲｽﾞ
#define SCREEN_SIZE_Y (810)				// ｽｸﾘｰﾝの縦ｻｲｽﾞ

#define STATUS_SIZE_X (495)				// ｽﾃｰﾀｽの横ｻｲｽﾞ
#define STATUS_SIZE_Y (405)				// ｽﾃｰﾀｽの縦ｻｲｽﾞ

#define STATUS_CNT_X (2)				// ｽﾃｰﾀｽの横の数
#define STATUS_CNT_Y (1)				// ｽﾃｰﾀｽの縦の数

#define STATUS_CHARA_SIZE_X (180)		// ｽﾃｰﾀｽのｷｬﾗ画像の横ｻｲｽﾞ
#define STATUS_CHARA_SIZE_Y (270)		// ｽﾃｰﾀｽのｷｬﾗ画像の縦ｻｲｽﾞ

#define STATUS_CHARA_CNT_X (3)			// ｽﾃｰﾀｽのｷｬﾗ画像の横の数
#define STATUS_CHARA_CNT_Y (4)			// ｽﾃｰﾀｽのｷｬﾗ画像の縦の数

#define STATUS_BAR_OFFSET_X (-180)		// ｷｬﾗ画像描画位置からの横ｹﾞｰｼﾞ描画位置
#define STATUS_BAR_OFFSET_Y (50)		// ｷｬﾗ画像描画位置からの縦ｹﾞｰｼﾞ描画位置

#define WIDTH		(60)				// ｽﾃｰﾀｽの描画ずらし幅
#define HP_PAR		(130 / 10)			// HPの値を割る
#define SPECIAL_PAR (50 / 10)			// SPECIALの値を割る
#define POWER_PAR	(10)				// POWERの値を割る
#define SPEED_PAR	(6)					// SPEEDの値を割る

// ﾃﾞﾊﾞｯｸﾒｯｾｰｼﾞ用定義
#ifdef _DEBUG		// 失敗時の処理
#define AST() {\
	CHAR ast_mes[256];\
	wsprintf(ast_mes, "%s %d行目\0", __FILE__, __LINE__);\
	MessageBox(0, ast_mes, "ｱｻｰﾄ表示", MB_OK);\
	}
#else				// 成功時の処理
#define AST()
#endif		// _DEBUG


CharacterSelectScene::CharacterSelectScene()
{
	bgImage = 0;
	Init();
	OutputDebugString("CharacteSselectSceneｸﾗｽのｲﾝｽﾀﾝｽ\n");
}

CharacterSelectScene::~CharacterSelectScene()
{
	DeleteGraph(bgImage);
	DeleteFontToHandle(statusfont);
	OutputDebugString("CharacteSselectSceneｸﾗｽの削除\n");
}

unique_Base CharacterSelectScene::Update(unique_Base own, const GameCtl & ctl)
{
	OldSceneChangeFlag = SceneChangeFlag;
	// 音
	if (CheckSoundMem(lpSoundMng.GetID("bgm/title.mp3")) == 0)
	{
		PlaySoundMem(lpSoundMng.GetID("bgm/title.mp3"), DX_PLAYTYPE_LOOP, true);
	}
	// ----------

	allStandbyFlag = true;

	for (auto itr = objList->begin(); itr != objList->end(); itr++)
	{
		if ((*itr)->GetStandbyFlag() == false)
		{
			if (ctl.GetPadTrigger((*itr)->GetPlayerNo(), PAD_INPUT_2) || SceneChangeFlag)
			{
				SceneChangeFlag = true;

				//音
				ChangeVolumeSoundMem(lpSceneChange.GetBright(), lpSoundMng.GetID("bgm/title.mp3"));
				if (SceneChangeFlag != OldSceneChangeFlag)
				{
					PlaySoundMem(lpSoundMng.GetID("se/select.wav"), DX_PLAYTYPE_BACK, true);
				}
				// --------

				if (lpSceneChange.FadeOut(DEF_SCENECHANGE_SPEED, DEF_SCENECHANGE_WAIT, true))
				{
					// 音
					ChangeVolumeSoundMem(255, lpSoundMng.GetID("bgm/title.mp3"));
					StopSoundMem(lpSoundMng.GetID("bgm/title.mp3"));
					StopSoundMem(lpSoundMng.GetID("se/check.wav"));
					StopSoundMem(lpSoundMng.GetID("se/select.wav"));
					// ----------
					return std::make_unique<MainMenuScene>();
				}
			}
			allStandbyFlag = false;
			continue;
		}

		if ((*itr)->GetPlayerNo() == (playerCnt - 1))
		{
			if (allStandbyFlag)
			{
				for (int specialTimeCnt = 0; specialTimeCnt < playerCnt; specialTimeCnt++)
				{
					if (ctl.GetPadTrigger(specialTimeCnt, PAD_INPUT_3) || SceneChangeFlag)
					{
						SceneChangeFlag = true;
						// IDﾗﾝﾀﾞﾑのﾌﾟﾚｲﾔｰが存在するかどうかのﾁｪｯｸ
						for (int cnt2 = 0; cnt2 < 4; cnt2++)
						{
							if (lpSceneMng.GetPlayerId(cnt2) == CHARA_ID::RANDOM || cnt2 >= playerCnt)
							{
								bool daburi = true;
								while (daburi)
								{
									daburi = false;
									lpSceneMng.SetPlayerId(cnt2, (CHARA_ID)(rand() % (CHAR_CNT_MAX - 2)));
									for (int cnt3 = 0; cnt3 < 4; cnt3++)
									{
										if (cnt3 == cnt2)
										{
											continue;
										}
										else
										{
											if (lpSceneMng.GetPlayerId(cnt2) == lpSceneMng.GetPlayerId(cnt3))
											{
												daburi = true;
												break;
											}
										}
									}
								}
							}
						}

						// 音
						if (SceneChangeFlag != OldSceneChangeFlag)
						{
							PlaySoundMem(lpSoundMng.GetID("se/check.wav"), DX_PLAYTYPE_BACK, true);
						}
						// ----------

						if (lpSceneChange.FadeOut(255, DEF_SCENECHANGE_WAIT, true))
						{
							// 音
							ChangeVolumeSoundMem(255, lpSoundMng.GetID("bgm/title.mp3"));

							StopSoundMem(lpSoundMng.GetID("bgm/title.mp3"));
							StopSoundMem(lpSoundMng.GetID("se/check.wav"));
							StopSoundMem(lpSoundMng.GetID("se/select.wav"));
							// ----------
							return std::make_unique<GameScene>();
						}
					}
				}
			}
		}
	}


	//ｶｰｿﾙの移動処理
	for (auto itr = objList->begin(); itr != objList->end(); itr++)
	{
		(*itr)->UpDate(ctl);
	}

	//描画処理
	CharSelDraw();

	//現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す
	return std::move(own);
}

bool CharacterSelectScene::CharSelDraw(void)
{
	//描画処理
	ClsDrawScreen();		// 画面消去

	// 背景
	DrawBox(0, 0, SCREEN_SIZE_X, SCREEN_SIZE_Y, 0x9999ff, true);
	DrawGraph(0, 0, bgImage, true);

	// ｽﾃｰﾀｽ
	auto charaImage = lpImageMng.GetID("image/status_character.png",
									   VECTOR2(STATUS_CHARA_SIZE_X, STATUS_CHARA_SIZE_Y),
									   VECTOR2( STATUS_CHARA_CNT_X,  STATUS_CHARA_CNT_Y) );
	SetFontSize(32);
		// 参加している方
	for (int specialTimeCnt = 0; specialTimeCnt < playerCnt; specialTimeCnt++)
	{
		// ｷｬﾗ
		CHARA_ID id = lpSceneMng.GetPlayerId(specialTimeCnt);

		// 名前
		DrawFormatString(stCharaNamePosTbl[specialTimeCnt].x, stCharaNamePosTbl[specialTimeCnt].y, 0xffffff, "%s", charaNameTbl[static_cast<int>(id)]);

		// 立ち絵
		if (id == CHARA_ID::NON)
		{
			continue;
		}
		else
		{
			DrawGraph(stCharaPosTbl[specialTimeCnt].x, stCharaPosTbl[specialTimeCnt].y, charaImage[static_cast<int>(id)], true);
		}

		int tmp = 0;


		// ｽﾃｰﾀｽ
		for (int j = 0; j < static_cast<int>(STATUS_TYPE::MAX); j++)
		{
			DrawBox(stCharaPosTbl[specialTimeCnt].x + STATUS_BAR_OFFSET_X, stCharaPosTbl[specialTimeCnt].y + STATUS_BAR_OFFSET_Y + j * WIDTH,
				stCharaPosTbl[specialTimeCnt].x + STATUS_BAR_OFFSET_X, stCharaPosTbl[specialTimeCnt].y + STATUS_BAR_OFFSET_Y + j * WIDTH,
				0xffffff, true);

			if (CHARA_ID::SECRET < id)
			{
				continue;
			}
			else
			{
				switch (j)
				{
				case static_cast<int>(STATUS_TYPE::HP) :
					tmp = status[static_cast<int>(id)][static_cast<int>(STATUS_TYPE::HP)] / HP_PAR;
					break;

				case static_cast<int>(STATUS_TYPE::SPECIAL) :
					tmp = (100 - status[static_cast<int>(id)][static_cast<int>(STATUS_TYPE::SPECIAL)]) / SPECIAL_PAR;
					break;

				case static_cast<int>(STATUS_TYPE::POWER) :
					tmp = status[static_cast<int>(id)][static_cast<int>(STATUS_TYPE::POWER)] / POWER_PAR;
					break;

				case static_cast<int>(STATUS_TYPE::SPEED) :
					tmp = status[static_cast<int>(id)][static_cast<int>(STATUS_TYPE::SPEED)] / SPEED_PAR;
					break;
				default:
					AST();
				}
				if (tmp <= 0)
				{
					tmp = 1;
				}
				if (tmp > 10)
				{
					tmp = 10;
				}
				DrawRectExtendGraph(
					3 + stCharaPosTbl[specialTimeCnt].x + STATUS_BAR_OFFSET_X,
					4 + stCharaPosTbl[specialTimeCnt].y + STATUS_BAR_OFFSET_Y + j * WIDTH,
					3 + stCharaPosTbl[specialTimeCnt].x + STATUS_BAR_OFFSET_X + (tmp * 13),
					4 + stCharaPosTbl[specialTimeCnt].y + STATUS_BAR_OFFSET_Y + j * WIDTH + 26,
					0,
					0,
					11 * tmp,
					26,
					IMAGE_ID("image/HPgauge.png")[0],
					true);
				DrawGraph(stCharaPosTbl[specialTimeCnt].x + STATUS_BAR_OFFSET_X,
					stCharaPosTbl[specialTimeCnt].y + STATUS_BAR_OFFSET_Y + j * WIDTH,
					IMAGE_ID("image/UI_gaugeframe.png")[0], true);
				DrawFormatStringToHandle(statusTextPosTbl[specialTimeCnt].x, statusTextPosTbl[specialTimeCnt].y + j * WIDTH, 0xffffff, statusfont, "%s", statusTextTbl[j]);
			}
		}
	}

	//objListに登録されているｸﾗｽ（ここではカーソル）の描画処理を行う
	for (auto &data : (*objList))
	{
		(*data).Draw((*data).GetPlayerNo());
	}

	if (allStandbyFlag)
	{
		DrawGraph(SCREEN_SIZE_X / 2 - 1200/2, SCREEN_SIZE_Y / 2 - 335/2, lpImageMng.GetID("image/ready.png")[0], true);
	}

	ScreenFlip();		// ｹﾞｰﾑﾙｰﾌﾟの最後に必ず必要

	return true;
}

int CharacterSelectScene::Init(void)
{
	// ﾌｫﾝﾄ
	statusfont = CreateFontToHandle(NULL, 20, 14, DX_FONTTYPE_EDGE);

	// ｹﾞｰﾑｽｸﾘｰﾝｻｲｽﾞ
	lpSceneMng.SetGameScreenSize(VECTOR2(SCREEN_SIZE_X, SCREEN_SIZE_Y));

	// ﾌﾟﾚｲ人数
	playerCnt = lpSceneMng.GetPlayerCnt();

	// 乱数
	srand((unsigned int) time(NULL));

	// ﾃｰﾌﾞﾙ
	playerNumPosTbl = { VECTOR2(								40,									32),
						VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X + 40,									32),
						VECTOR2(								40, SCREEN_SIZE_Y - STATUS_SIZE_Y + 32),
						VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X + 40, SCREEN_SIZE_Y - STATUS_SIZE_Y + 32) };

	playerColorTbl = { 0xff4444, 0x4444ff, 0xffff66, 0x44ff44 };

	stOutLinePosTbl = { VECTOR2(							0,							   0),
						VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X,							   0),
						VECTOR2(							0, SCREEN_SIZE_Y - STATUS_SIZE_Y),
						VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X, SCREEN_SIZE_Y - STATUS_SIZE_Y) };

	stCharaPosTbl = { VECTOR2(				  STATUS_SIZE_X / 2,					 STATUS_SIZE_Y / 4),
					  VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X / 2,					 STATUS_SIZE_Y / 4),
					  VECTOR2(				  STATUS_SIZE_X / 2, SCREEN_SIZE_Y - STATUS_SIZE_Y * 3 / 4),
					  VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X / 2, SCREEN_SIZE_Y - STATUS_SIZE_Y * 3 / 4) };

	stCharaNamePosTbl = { VECTOR2(				  STATUS_SIZE_X / 2 - 50,					 STATUS_SIZE_Y / 4 - 50),
						  VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X / 2 - 50,					 STATUS_SIZE_Y / 4 - 50),
						  VECTOR2(				  STATUS_SIZE_X / 2 - 50, SCREEN_SIZE_Y - STATUS_SIZE_Y * 3 / 4 - 50),
						  VECTOR2(SCREEN_SIZE_X - STATUS_SIZE_X / 2 - 50, SCREEN_SIZE_Y - STATUS_SIZE_Y * 3 / 4 - 50) };


	statusTextPosTbl = {  VECTOR2(STATUS_SIZE_X / 9 + STATUS_SIZE_X / 40,						STATUS_SIZE_Y / 3 - 5),
						  VECTOR2(SCREEN_SIZE_X / 2 + STATUS_SIZE_X / 2 + STATUS_SIZE_X / 11,	STATUS_SIZE_Y / 3 - 5),
						  VECTOR2(STATUS_SIZE_X / 9 + STATUS_SIZE_X / 40,						SCREEN_SIZE_Y / 2 + STATUS_SIZE_Y / 3 - 4),
						  VECTOR2(SCREEN_SIZE_X / 2 + STATUS_SIZE_X / 2 + STATUS_SIZE_X / 11,	SCREEN_SIZE_Y / 2 + STATUS_SIZE_Y / 3 - 4) };

	cursorPosTbl = { VECTOR2(500, 300) , VECTOR2(930, 300) , VECTOR2(500, 670) , VECTOR2(930, 670) };

	charaNameTbl = {" ひーさん",
					"　ニ　コ",
					" ミ ッ ツ",
					" ヨ ウ コ",
					"ゴトウさん",
					"ムッちゃん",
					"なっちゃん",
					" ハチベエ",
					" ア ニ キ",
					"　会　長",
					" 鬼 軍 曹",
					" ランダム",
					" 未 選 択"};

	// 背景

	if (bgImage <= 0)
	{
		VECTOR2 scrSize = VECTOR2(SCREEN_SIZE_X, SCREEN_SIZE_Y);
		bgImage = MakeScreen(scrSize.x, scrSize.y, true);
		SetDrawScreen(bgImage);

		DrawGraph(0, 0, IMAGE_ID("image/menu_back_tile.png")[0], true);
		SetDrawBlendMode(DX_BLENDMODE_INVSRC, 255);
		DrawGraph(0, 0, IMAGE_ID("image/mask.png")[0], true);
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

		// 文字
		DrawGraph(STATUS_SIZE_X - 23, 22, IMAGE_ID("image/character_select.png")[0], true);		// 中央上

		// ｱｲｺﾝ
		DrawGraph(STATUS_SIZE_X, STATUS_SIZE_Y / 2, IMAGE_ID("image/icon_kari.png")[0], true);		// 中央下

		auto outLineImage = lpImageMng.GetID("image/status_outline.png",
											 VECTOR2(STATUS_SIZE_X, STATUS_SIZE_Y),
											 VECTOR2(STATUS_CNT_X, STATUS_CNT_Y));

		for (int specialTimeCnt = 0; specialTimeCnt < PLAYER_CNT_MAX; specialTimeCnt++)
		{
			if (specialTimeCnt >= playerCnt)
			{
				DrawGraph(stOutLinePosTbl[specialTimeCnt].x, stOutLinePosTbl[specialTimeCnt].y, outLineImage[1], true);
				continue;
			}

			// ｱｳﾄﾗｲﾝ
			DrawGraph(stOutLinePosTbl[specialTimeCnt].x, stOutLinePosTbl[specialTimeCnt].y, outLineImage[0], true);

			// ﾌﾟﾚｲﾔｰ番号
			SetFontSize(70);
			DrawFormatString(playerNumPosTbl[specialTimeCnt].x, playerNumPosTbl[specialTimeCnt].y, playerColorTbl[specialTimeCnt], "%d", (specialTimeCnt + 1));
		}

		SetDrawScreen(DX_SCREEN_BACK);
	}

	// ｶｰｿﾙ
	if (!objList)
	{
		objList = std::make_shared<sharedObjList>();
	}
	objList->clear();		//objListを全削除する

	for (int specialTimeCnt = 0; specialTimeCnt < playerCnt; specialTimeCnt++)
	{
		AddObjList()(objList, std::make_shared<CharacterSelectCursor>(specialTimeCnt, cursorPosTbl[specialTimeCnt], VECTOR2(-6, -6)));
	}

	//ｷｬﾗｸﾀｰｽﾃｰﾀｽ情報
	status = {
		//HP,	技,		攻撃,	速度
		 35,	50,		70,		54, //1号館
		100,	90,		80,		60, //2号館
		110,	55,		90,		36, //3号館
		 90,	75,		70,		60, //4号館
		100,	80,		70,		60, //5号館
		110,	65,		100,	48, //6号館
		110,	75,		40,		36, //7号館
		100,	65,		40,		60, //8号館
		130,	95,		100,	18, //9号館
		 40,	50,		20,		42, //10号館
		  0,	 0,		0,		 0, //隠しｷｬﾗ
		};

	statusTextTbl = {
		"いのち",
		"とくぎ",
		"こうげき",
		"うごき"
	};

	return 0;
}
