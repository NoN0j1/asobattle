#pragma once
#include<string>
#include<map>

#define lpSoundMng SoundMng::GetInstance()
#define SOUND_ID(X) (SoundMng::GetInstance().GetID(X))

class SoundMng
{
public:
	// 外部からs_Instanceを参照するための関数
	static SoundMng &GetInstance(void)
	{
		static SoundMng s_Instance;
		return s_Instance;
	}

	// 外部から音のﾊﾝﾄﾞﾙを参照するための関数　引数:参照したい音ﾌｧｲﾙ名
	const int& GetID(std::string f_name);
private:
	SoundMng();
	~SoundMng();

	std::map<std::string, int> soundId;			// 音のﾊﾝﾄﾞﾙを格納する連想型配列
};

