#include "DxLib.h"
#include "SoundMng.h"
#include "SpecialAttack_Nail.h"

#define FALL_TIME	(45)	//地面までの時間

SpecialAttack_Nail::SpecialAttack_Nail(VECTOR2 drawOffset, VECTOR2 pos, int popNo, int depth) :Obj(drawOffset)
{
	init("image/kugi.png", VECTOR2(NAIL_SIZE_X, NAIL_SIZE_Y), VECTOR2(1, 1));

	dethFlag = false;

	this->pos = pos;

	this->depth = depth;

	playerNo = popNo;

	attackPos = this->pos;
	attackSize = divSize * 15 / 10;

	attackRangeFront = 10;
	attackRangeBack = 20;

	power = 30;

	speed = (depth - pos.y) / FALL_TIME;
}

SpecialAttack_Nail::~SpecialAttack_Nail()
{
}

bool SpecialAttack_Nail::CheckDeth(void)
{
	return dethFlag;
}

bool SpecialAttack_Nail::CheckObjType(OBJ_TYPE type)
{
	return (OBJ_TYPE_SP_ATTACK == type);
}

bool SpecialAttack_Nail::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth)
{
	if (this->depth + attackRangeFront >= depth && this->depth - attackRangeBack <= depth)
	{
		if (this->attackPos.x < defensePos.x + defenseSize.x
			&&this->attackPos.x + attackSize.x > defensePos.x
			&&this->attackPos.y < defensePos.y + defenseSize.y
			&&this->attackPos.y + attackSize.y > defensePos.y)
		{
			dethFlag = true;
			return true;
		}
	}
	return false;
}

int SpecialAttack_Nail::GetPlayerNo(void)
{
	return playerNo;
}

void SpecialAttack_Nail::Draw(void)
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	DrawOval(drawOffset.x + pos.x + ((divSize.x * 15 / 10) / 2), drawOffset.y + depth - 5, divSize.x / 6, 4, 0x000000, true);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	Obj::Draw();
}

void SpecialAttack_Nail::SetMove(const GameCtl & ctl, weekListObj objList)
{
	pos.y += speed;
	if ((pos.y + divSize.y * 15 / 10) >= depth)
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/5goEX02.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/5goEX02.wav"), DX_PLAYTYPE_BACK, true);
		}
		dethFlag = true;
	}

	attackPos = pos;
}
