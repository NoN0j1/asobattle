#include "DxLib.h"
#include "SceneChange.h"

// ﾃﾞﾊﾞｯｸﾒｯｾｰｼﾞ用定義
#ifdef _DEBUG		// 失敗時の処理
#define AST() {\
	CHAR ast_mes[256];\
	wsprintf(ast_mes, "%s %d行目\0", __FILE__, __LINE__);\
	MessageBox(0, ast_mes, "ｱｻｰﾄ表示", MB_OK);\
	}
#else				// 成功時の処理
#define AST()
#endif		// _DEBUG

#define INPUT_BUTTON (PAD_INPUT_1 | PAD_INPUT_2 | PAD_INPUT_3 | PAD_INPUT_4 | PAD_INPUT_5 | PAD_INPUT_6 | PAD_INPUT_7 | PAD_INPUT_8 | PAD_INPUT_9 | PAD_INPUT_10 | PAD_INPUT_11) 


SceneChange::SceneChange()
{
	Init();
	SetBrightDefault(true);
}


SceneChange::~SceneChange()
{
}


void SceneChange::Init()
{
	CompleteCnt = 0;
	ChangeCnt = 0;

	SceneBrightFlag = true;
	ChangeEndFlag = false;
}


bool SceneChange::FadeIn(int wait, bool nextbright)
{
	if (ChangeEndFlag == false)
	{
		if (InitFlag == false)
		{
			if (bright == 0)
			{
				SceneBrightFlag = false;
			}
			else
			{
				// 暗転が中途半端な場合は初期化
				SetBrightDefault(false);
				SceneBrightFlag = false;
			}
			InitFlag = true;
		}

		if (SceneBrightFlag == false)
		{
			SetDrawBright(bright, bright, bright);
			bright++;

			if (bright < 255)
			{
				ChangeEndFlag = true;
				SceneBrightFlag = true;
				return false;
			}
		}
		else
		{
			AST();
		}
	}
	else
	{
		if ((ChangeEndFlag == true) && (SceneBrightFlag == true))
		{
			// ｼｰﾝ移行処理後の待機
			if (CompleteCnt < ChangeCnt)
			{
				// ｼｰﾝ移行完了
				Init();
				if (nextbright == true)
				{
					SetBrightDefault(true);
				}
				return true;
			}
			else
			{
				ChangeCnt++;
				return false;
			}
		}
		// 初期化
		ChangeEndFlag = false;
		CompleteCnt = wait;
		InitFlag = false;
	}

	return false;
}


bool SceneChange::FadeOut(int wait, bool nextbright)
{
	CompleteCnt = wait;
	if (ChangeEndFlag == false)
	{
		if (InitFlag == false)
		{
			if (bright == 255)
			{
				SceneBrightFlag = true;
			}
			else
			{
				// 暗転が中途半端な場合は初期化
				SetBrightDefault(false);
				SceneBrightFlag = true;
			}
			InitFlag = true;
		}

		if (SceneBrightFlag == true)
		{
			SetDrawBright(bright, bright, bright);
			bright--;

			if (bright <= 0)
			{
				ChangeEndFlag = true;
				SceneBrightFlag = false;
				return false;
			}
		}
		else
		{
			AST();
		}
	}
	else
	{
		if ((ChangeEndFlag == true) && (SceneBrightFlag == false))
		{
			// ｼｰﾝ移行処理後の待機
			if (CompleteCnt < ChangeCnt)
			{
				// ｼｰﾝ移行完了
				Init();
				if (nextbright == true)
				{
					SetBrightDefault(true);
				}
				return true;
			}
			else
			{
				ChangeCnt++;
				return false;
			}
		}
		// 初期化
		ChangeEndFlag	= false;
		InitFlag		= false;
	}

	return false;
}


bool SceneChange::FadeIn(int speed,int wait, bool nextbright)
{
	if (ChangeEndFlag == false)
	{
		if (InitFlag == false)
		{
			if (bright == 0)
			{
				SceneBrightFlag = false;
			}
			else
			{
				// 暗転が中途半端な場合は初期化
				SetBrightDefault(false);
				SceneBrightFlag = false;
			}
			InitFlag = true;
		}

		if (SceneBrightFlag == false)
		{
			SetDrawBright(bright, bright, bright);
			bright += speed;

			if (bright < 255)
			{
				ChangeEndFlag = true;
				SceneBrightFlag = true;
				return false;
			}
		}
		else
		{
			AST();
		}
	}
	else
	{
		if ((ChangeEndFlag == true) && (SceneBrightFlag == true))
		{
			// ｼｰﾝ移行処理後の待機
			if (CompleteCnt < ChangeCnt)
			{
				// ｼｰﾝ移行完了
				Init();
				if (nextbright == true)
				{
					SetBrightDefault(true);
				}
				return true;
			}
			else
			{
				ChangeCnt++;
				return false;
			}
		}
		// 初期化
		ChangeEndFlag = false;
		CompleteCnt = wait;
		InitFlag = false;
	}

	return false;
}


bool SceneChange::FadeOut(int speed,int wait, bool nextbright)
{
	CompleteCnt = wait;
	if (ChangeEndFlag == false)
	{
		if (InitFlag == false)
		{
			if (bright == 255)
			{
				SceneBrightFlag = true;
			}
			else
			{
				// 暗転が中途半端な場合は初期化
				SetBrightDefault(false);
				SceneBrightFlag = true;
			}
			InitFlag = true;
		}

		if (SceneBrightFlag == true)
		{
			SetDrawBright(bright, bright, bright);
			bright -= speed;

			if (bright <= 0)
			{
				ChangeEndFlag = true;
				SceneBrightFlag = false;
				return false;
			}
		}
		else
		{
			AST();
		}
	}
	else
	{
		if ((ChangeEndFlag == true) && (SceneBrightFlag == false))
		{
			// ｼｰﾝ移行処理後の待機
			if (CompleteCnt < ChangeCnt)
			{
				// ｼｰﾝ移行完了
				Init();
				if (nextbright == true)
				{
					SetBrightDefault(true);
				}
				return true;
			}
			else
			{
				ChangeCnt++;
				return false;
			}
		}
		// 初期化
		ChangeEndFlag = false;
		InitFlag = false;
	}

	return false;
}


const int SceneChange::GetBright()
{
	return bright;
}


void SceneChange::SetBrightDefault(bool darkness)
{
	if (darkness == false)
	{
		bright = 0;
		SceneBrightFlag = false;
	}
	else
	{
		bright = 255;
		SceneBrightFlag = true;
	}
	SetDrawBright(bright, bright, bright);
}