#pragma once
#include "Character.h"

class Goukan7 :
	public Character
{
public:
	Goukan7(VECTOR2 drawOffset, int popNo, bool aiFlag);
	~Goukan7();

	//HPの最大値を取得する
	int GetHPMax(void);
private:
	//必殺技時の処理
	bool Special(const GameCtl &ctl, weekListObj objList);

	//必殺技時の描画
	void SpecialDraw(bool specialFlag);

	//ﾃﾞﾌｫﾙﾄの攻撃力を取得する
	int GetDefPower(void);
};