#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "Goukan10.h"

#define GOUKAN10_HP_MAX				(70)	//HP�̍ő�l

#define GOUKAN10_DEF_SPEED			(5)		//��̫�Ă̈ړ���߰��

#define GOUKAN10_DEF_POWER			(7)		//��̫�Ă̍U����

#define GOUKAN10_SPECIAL_CNT_MAX	(60)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(736 / 4)		//�K�E�Z�̏c����
#define ANIM_SIZE_Y					(184)			//�K�E�Z�̏c����
#define ANIM_CNT					(4)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(10)			//�K�E�Z�̱�Ұ��݊Ԋu

#define EFFECT_SIZE_X				(368 / 2)		//�̪�Ẳ�����
#define EFFECT_SIZE_Y				(184)			//�̪�Ă̏c����
#define EFFECT_CNT					(2)				//�̪�Ă̱�Ұ��ݐ�
#define EFFECT_FRAME				(10)			//�̪�Ă̱�Ұ��݊Ԋu

#define CNT_MAX						(90)			//�K�E�Z�̔�������

Goukan10::Goukan10(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN10_HP_MAX;
	power = GOUKAN10_DEF_POWER;
	speed = GOUKAN10_DEF_SPEED;
	specialCntMax = GOUKAN10_SPECIAL_CNT_MAX;
	init("image/goukan10.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan10_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(4, 1));

	lpImageMng.GetID("image/effect/onpu.png", VECTOR2(EFFECT_SIZE_X, EFFECT_SIZE_Y), VECTOR2(2, 1));

	lpImageMng.GetID("image/cutin/10gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	_RPT0(_CRT_WARN, "10���ق�ݽ�ݽ\n");
}

Goukan10::~Goukan10()
{
	_RPT0(_CRT_WARN, "10���ق��폜\n");
}

int Goukan10::GetHPMax(void)
{
	return GOUKAN10_HP_MAX;
}

bool Goukan10::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	if ((specialTimeCnt % (CNT_MAX / 10)) == 0)
	{
		if (CheckSoundMem(lpSoundMng.GetID("bgm/10goEX01.wav")) == 0)
		{
			StopSoundMem(lpSoundMng.GetID("bgm/battle.mp3"));
			StopSoundMem(lpSoundMng.GetID("bgm/hokuto.mp3"));
			PlaySoundMem(lpSoundMng.GetID("bgm/10goEX01.wav"), DX_PLAYTYPE_BACK, true);
		}

		HP += 1;
		if (HP > GOUKAN10_HP_MAX)
		{
			HP = GOUKAN10_HP_MAX;
		}
	}

	if (specialTimeCnt >= CNT_MAX + CUTIN_END_FRAME * 4)
	{
		StopSoundMem(lpSoundMng.GetID("bgm/10goEX01.wav"));
		if (CheckSoundMem(lpSoundMng.GetID("se/10goEX02.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/10goEX02.wav"), DX_PLAYTYPE_BACK, true);
		}
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		Character::move = &Goukan10::Normal;
		StopSoundMem(lpSoundMng.GetID("bgm/10goEX01.wav"));
	}

	specialTimeCnt++;

	return true;
}

void Goukan10::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	int animCnt = specialTimeCnt / ANIM_FRAME;
	if (animCnt >= ANIM_CNT)
	{
		animCnt = (animCnt % (ANIM_CNT - 1)) + 1;
	}

	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) -60,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + ANIM_SIZE_X * 15/10 -60,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan10_Special.png")[animCnt], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + ANIM_SIZE_X * 15 / 10 - 30,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15 / 10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 30,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan10_Special.png")[animCnt], true);
	}

	if (animCnt > 0)
	{
		DrawGraph(drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (EFFECT_SIZE_X / 2), drawOffset.y + (depth - EFFECT_SIZE_Y), lpImageMng.GetID("image/effect/onpu.png")[(specialTimeCnt / EFFECT_FRAME) % EFFECT_CNT], true);
	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/10gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/10gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan10::GetDefPower(void)
{
	return GOUKAN10_DEF_POWER;
}
