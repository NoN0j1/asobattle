#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "Goukan8.h"

#define GOUKAN8_HP_MAX				(100)	//HP�̍ő�l

#define GOUKAN8_DEF_SPEED			(6)		//��̫�Ă̈ړ���߰��

#define GOUKAN8_DEF_POWER			(8)		//��̫�Ă̍U����

#define GOUKAN8_SPECIAL_CNT_MAX		(70)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(920 / 5)		//�K�E�Z�̏c����
#define ANIM_SIZE_Y					(184)			//�K�E�Z�̏c����
#define ANIM_CNT					(5)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(15)			//�K�E�Z�̱�Ұ��݊Ԋu

#define POWER_UP					(16)			//�U���ͱ��ߎ���
#define POWER_UP_CNT_MAX			(60 * 5)		//�U���ͱ��ߎ���

Goukan8::Goukan8(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN8_HP_MAX;
	power = GOUKAN8_DEF_POWER;
	speed = GOUKAN8_DEF_SPEED;
	specialCntMax = GOUKAN8_SPECIAL_CNT_MAX;
	init("image/goukan8.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan8_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(5, 1));

	lpImageMng.GetID("image/cutin/8gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	_RPT0(_CRT_WARN, "8���ق�ݽ�ݽ\n");
}

Goukan8::~Goukan8()
{
	_RPT0(_CRT_WARN, "8���ق��폜\n");
}

int Goukan8::GetHPMax(void)
{
	return GOUKAN8_HP_MAX;
}

bool Goukan8::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	if (specialTimeCnt >= (ANIM_FRAME * ANIM_CNT) + 10 + CUTIN_END_FRAME)
	{
		power = POWER_UP;
		powerUpFlag = true;
		powerUpCnt = POWER_UP_CNT_MAX;

		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		Character::move = &Goukan8::Normal;
	}

	specialTimeCnt++;

	return true;
}

void Goukan8::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	int animCnt = specialTimeCnt / ANIM_FRAME;
	if (animCnt >= ANIM_CNT - 1)
	{
		animCnt = ANIM_CNT - 1;
	}
	if (animCnt == ANIM_CNT - 1)
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/8goEX.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/8goEX.wav"),DX_PLAYTYPE_BACK,true);
		}
	}
	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 60,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15 / 10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15 / 10) - 60,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan8_Special.png")[animCnt], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15 / 10) - 30,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15 / 10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 30,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan8_Special.png")[animCnt], true);
	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/8gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/8gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan8::GetDefPower(void)
{
	return GOUKAN8_DEF_POWER;
}
