#include"DxLib.h"
#include"SceneMng.h"
#include"ImageMng.h"
#include "GameCtl.h"
#include"CharacterSelectCursor.h"

#define CURSOR_DEF_SPEED (8)

#define ICON_SIZE (135)

#define ICON_CNT_X (3)
#define ICON_CNT_Y (4)

CharacterSelectCursor::CharacterSelectCursor(int popCnt, VECTOR2 pos, VECTOR2 drawOffset) :Obj(drawOffset)
{
	popNo = popCnt;
	decidFlag = false;
	init("image/cursor.png", VECTOR2(64, 64), VECTOR2(4, 1), pos);

	framePosTbl = { VECTOR2(518				   , 225				),
					VECTOR2(518 + ICON_SIZE	   , 225				),
					VECTOR2(518 + ICON_SIZE * 2, 225				),
					VECTOR2(518				   , 225 + ICON_SIZE	),
					VECTOR2(518 + ICON_SIZE	   , 225 + ICON_SIZE	),
					VECTOR2(518 + ICON_SIZE * 2, 225 + ICON_SIZE	),
					VECTOR2(518				   , 225 + ICON_SIZE * 2),
					VECTOR2(518 + ICON_SIZE	   , 225 + ICON_SIZE * 2),
					VECTOR2(518 + ICON_SIZE * 2, 225 + ICON_SIZE * 2),
					VECTOR2(518				   , 225 + ICON_SIZE * 3),
					VECTOR2(518 + ICON_SIZE	   , 225 + ICON_SIZE * 3),
					VECTOR2(518 + ICON_SIZE * 2, 225 + ICON_SIZE * 3) };
}

CharacterSelectCursor::~CharacterSelectCursor()
{
}

bool CharacterSelectCursor::CheckObjType(OBJ_TYPE type)
{
	return (type == OBJ_TYPE_CURSOR);
}

int CharacterSelectCursor::GetPlayerNo(void)
{
	return popNo;
}

void CharacterSelectCursor::SetMove(const GameCtl & ctl)
{
	if (!decidFlag)
	{
		// ｽﾋﾟｰﾄﾞ値の初期化
		moveSpeed = VECTOR2(0, 0);

		// 入力によるｽﾋﾟｰﾄﾞ値の加算
		if (ctl.GetPad(popNo, PAD_INPUT_RIGHT))
		{
			moveSpeed.x += CURSOR_DEF_SPEED;
		}
		else if (ctl.GetPad(popNo, PAD_INPUT_LEFT))
		{
			moveSpeed.x -= CURSOR_DEF_SPEED;
		}
		else
		{
			// 左右移動なし
		}
		if (ctl.GetPad(popNo, PAD_INPUT_DOWN))
		{
			moveSpeed.y += CURSOR_DEF_SPEED;
		}
		else if (ctl.GetPad(popNo, PAD_INPUT_UP))
		{
			moveSpeed.y -= CURSOR_DEF_SPEED;
		}
		else
		{
			// 上下移動なし
		}

		// 範囲制御とposの更新
		VECTOR2 screenSize = lpSceneMng.GetGameScreenSize();

		if (((pos.x + moveSpeed.x) < 0) || ((pos.x + moveSpeed.x) > (screenSize.x - divSize.x - drawOffset.x + 6)))
		{
			if ((pos.x + moveSpeed.x) < 0)
			{
				pos.x = 0;
			}
			else
			{
				pos.x = (screenSize.x - divSize.x - drawOffset.x + 6);
			}
		}
		else
		{
			pos.x += moveSpeed.x;
		}

		if (((pos.y + moveSpeed.y) < 0) || ((pos.y + moveSpeed.y) > (screenSize.y - divSize.y - drawOffset.y)))
		{
			if ((pos.y + moveSpeed.y) < 0)
			{
				pos.y = 0;
			}
			else
			{
				pos.y = (screenSize.y - divSize.y - drawOffset.y);
			}
		}
		else
		{
			pos.y += moveSpeed.y;
		}

		// id更新
		ChackId();

		// 決定確認
		if (ctl.GetPadTrigger(popNo, PAD_INPUT_1))
		{
			VECTOR2 tmpPos = (pos - VECTOR2(518, 225));

			if (((tmpPos.x > 0) && (tmpPos.x < (ICON_SIZE * ICON_CNT_X)) && (tmpPos.y > 0) && (tmpPos.y < (ICON_SIZE * ICON_CNT_Y)))
			&& !((tmpPos.x >= ICON_SIZE) && (tmpPos.x < (ICON_SIZE * 2)) && (tmpPos.y >= (ICON_SIZE * 3)) && (tmpPos.y < (ICON_SIZE * ICON_CNT_Y))))
			{
				decidFlag = true;

				// ｷｬﾗ被りのﾁｪｯｸ
				if (playerId != CHARA_ID::RANDOM)
				{
					for (int specialTimeCnt = 0; specialTimeCnt < lpSceneMng.GetPlayerCnt(); specialTimeCnt++)
					{
						if (specialTimeCnt == popNo)
						{
							continue;
						}
						else
						{
							if (lpSceneMng.GetPlayerId(specialTimeCnt) == playerId)
							{
								if (lpSceneMng.GetDecidFlag(specialTimeCnt))
								{
									// 被ってるﾌﾟﾚｲﾔｰがいたらfalseにする
									decidFlag = false;
									break;
								}
							}
						}
					}
				}

				lpSceneMng.SetDecidFlag(popNo, decidFlag);
			}
		}
	}
	else
	{
		// 決定取り消し確認
		if (ctl.GetPadTrigger(popNo, PAD_INPUT_2))
		{
			decidFlag = false;
			lpSceneMng.SetDecidFlag(popNo, false);
		}
	}
}

void CharacterSelectCursor::Draw(unsigned int id)
{
	if (!decidFlag)
	{
		Obj::Draw(id);
	}
	else
	{
		DrawGraph(framePosTbl[static_cast<int>(playerId)].x, framePosTbl[static_cast<int>(playerId)].y, lpImageMng.GetID("image/frame.png", VECTOR2(135, 135), VECTOR2(4, 1))[id], true);
	}
}

bool CharacterSelectCursor::GetStandbyFlag(void)
{
	return decidFlag;
}

void CharacterSelectCursor::ChackId(void)
{
	playerIdOld = playerId;

	VECTOR2 tmpPos = (pos - VECTOR2(518, 225));

	if ((tmpPos.x < 0) || (tmpPos.x > (ICON_SIZE * ICON_CNT_X))
	 || (tmpPos.y < 0) || (tmpPos.y > (ICON_SIZE * ICON_CNT_Y)))
	{
		playerId = CHARA_ID::NON;
	}
	else if ((tmpPos.x >= ICON_SIZE) && (tmpPos.x < (ICON_SIZE * 2))
		&& (tmpPos.y >= (ICON_SIZE * 3)) && (tmpPos.y < (ICON_SIZE * ICON_CNT_Y)))
	{
		playerId = CHARA_ID::NON;
	}
	else
	{
		playerId = static_cast<CHARA_ID>((tmpPos.y / ICON_SIZE) * ICON_CNT_X + (tmpPos.x / ICON_SIZE));
	}

	if (playerId != CHARA_ID::NON)
	{
		if (playerIdOld != playerId)
		{
			lpSceneMng.SetPlayerId(popNo, playerId);
		}
	}
}
