#pragma once
#include<random>
#include "Character.h"

class Goukan5 :
	public Character
{
public:
	Goukan5(VECTOR2 drawOffset, int popNo, bool aiFlag);
	~Goukan5();

	//HPの最大値を取得する
	int GetHPMax(void);
private:
	//必殺技時の処理
	bool Special(const GameCtl &ctl, weekListObj objList);

	//必殺技時の描画
	void SpecialDraw(bool specialFlag);

	//ﾃﾞﾌｫﾙﾄの攻撃力を取得する
	int GetDefPower(void);

	std::random_device rnd;							//非決定的乱数生成期
	std::uniform_int_distribution<> scopeNailX;		//釘の出現範囲指定
	std::uniform_int_distribution<> scopeDepth;		//釘の落下地点範囲指定
};