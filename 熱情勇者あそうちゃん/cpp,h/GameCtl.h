#pragma once
#include<array>
#include<vector>
#include"VECTOR2.h"

enum KEY_TYPE
{
	KEY_TYPE_NOW,				//今のｷｰ情報
	KEY_TYPE_OLD,				//1ﾌﾚｰﾑ前のｷｰ情報
	KEY_TYPE_MAX
};

enum PAD_NO
{
	PAD_NO_1,
	PAD_NO_2,
	PAD_NO_3,
	PAD_NO_4,
	PAD_NO_MAX
};

using KEY_ARRAY = std::array<char, 256>;	//キー情報を格納する配列の型

class GameCtl
{
public:
	GameCtl();						//ｺﾝｽﾄﾗｸﾀ
	~GameCtl();						//ﾃﾞｽﾄﾗｸﾀ
	 bool UpDate(void);				//情報更新

	 bool SetUpPad(int padMax);		//ｹﾞｰﾑﾊﾟｯﾄﾞを何個使うかｾｯﾄする

	 const KEY_ARRAY & GetKeyCtl(KEY_TYPE type) const;		//引数で指定されたKEY_TYPEのｷｰ情報を取得する

	//押してるか　引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,　どのﾎﾞﾀﾝ？　返り値:true 押してる
	 const bool GetPad(int no,int input) const;
	//押した瞬間　引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,　どのﾎﾞﾀﾝ？　返り値:true 押した瞬間
	 const bool GetPadTrigger(int no, int input) const;
	//押しっぱなし　引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,　どのﾎﾞﾀﾝ？　返り値:true 押しっぱなし
	 const bool GetPadHold(int no, int input) const;
	//離した瞬間　引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,　どのﾎﾞﾀﾝ？　返り値:true 離した瞬間
	 const bool GetPadRelease(int no, int input) const;
private:
	KEY_ARRAY data_Key;				//今のｷｰ情報
	KEY_ARRAY data_Key_Old;			//1ﾌﾚｰﾑ前のｷｰ情報
	bool KeyUpDate(void);			//ｷｰ情報の情報更新

	std::vector<unsigned int> data_Pad;			//今のｹﾞｰﾑﾊﾟｯﾄﾞ情報
	std::vector<unsigned int> data_Pad_Old;		//1ﾌﾚｰﾑ前のｹﾞｰﾑﾊﾟｯﾄﾞ情報
	bool PadUpDate(int padNo);					//ｹﾞｰﾑﾊﾟｯﾄﾞ情報の情報更新
	int padMax;									//使うｹﾞｰﾑﾊﾟｯﾄﾞ数
	std::array<int, PAD_NO_MAX> padTbl;			//ｹﾞｰﾑﾊﾟｯﾄﾞ識別ﾃｰﾌﾞﾙ
};

