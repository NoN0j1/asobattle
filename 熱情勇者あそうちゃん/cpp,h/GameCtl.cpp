#include"DxLib.h"
#include "GameCtl.h"

GameCtl::GameCtl()
{
			  //PAD1			  PAD2			 PAD3			PAD4
	padTbl = { DX_INPUT_KEY_PAD1 ,DX_INPUT_PAD2 ,DX_INPUT_PAD3 ,DX_INPUT_PAD4 };
	if (GetJoypadNum() == 0)
	{
		data_Pad.resize(0);
	}
	else
	{
		data_Pad.resize(GetJoypadNum());
	}
	data_Pad_Old.resize(data_Pad.size());
	padMax = 1;
	_RPT0(_CRT_WARN, "GameCtlｸﾗｽをｲﾝｽﾀﾝｽ\n");
}


GameCtl::~GameCtl()
{
	_RPT0(_CRT_WARN, "GameCtlｸﾗｽを削除\n");
}

bool GameCtl::UpDate(void)
{
	KeyUpDate();

	for (int no = 0; no < padMax; no++)
	{
		PadUpDate(no);
	}
	return true;
}

const KEY_ARRAY & GameCtl::GetKeyCtl(KEY_TYPE type) const
{
	if (type == KEY_TYPE_OLD)
	{
		return data_Key_Old;
	}
	return data_Key;
}

//押してるか	引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,どのﾎﾞﾀﾝ？　返り値:true 押してる
const bool GameCtl::GetPad(int no, int input) const
{
	if (no >= data_Pad.size())
	{
		return false;
	}
	if (data_Pad[no] & input)
	{
		return true;
	}
	return false;
}

//押した瞬間	引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,どのﾎﾞﾀﾝ？　返り値:true 押した瞬間
const bool GameCtl::GetPadTrigger(int no, int input) const
{
	if (data_Pad.size() == 0)
	{

	}
	if (no >= data_Pad.size())
	{
		return false;
	}
	if (data_Pad[no] & input & (~data_Pad_Old[no]))
	{
		return true;
	}
	return false;
}

//押しっぱなし	引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,どのﾎﾞﾀﾝ？　返り値:true 押しっぱなし
const bool GameCtl::GetPadHold(int no, int input) const
{
	if (no >= data_Pad.size())
	{
		return false;
	}
	if (data_Pad[no] & input & data_Pad_Old[no])
	{
		return true;
	}
	return false;
}

//離した瞬間	引数:ｹﾞｰﾑﾊﾟｯﾄﾞ番号,どのﾎﾞﾀﾝ？　返り値:true 離した瞬間
const bool GameCtl::GetPadRelease(int no, int input) const
{
	if (no >= data_Pad.size())
	{
		return false;
	}
	if ((~data_Pad[no]) & input & data_Pad_Old[no])
	{
		return true;
	}
	return false;
}

bool GameCtl::KeyUpDate(void)
{
	data_Key_Old = data_Key;
	GetHitKeyStateAll(data_Key.data());
	return true;
}

bool GameCtl::PadUpDate(int padNo)
{
	data_Pad_Old[padNo] = data_Pad[padNo];
	data_Pad[padNo] = GetJoypadInputState(padTbl[padNo]);
	return true;
}

bool GameCtl::SetUpPad(int padMax)
{
	GameCtl::padMax = padMax;
	if (padMax == 0)
	{
		GameCtl::padMax = 1;
	}
	else
	{
		data_Pad.resize(padMax);
	}
	data_Pad_Old.resize(data_Pad.size());
	return true;
}
