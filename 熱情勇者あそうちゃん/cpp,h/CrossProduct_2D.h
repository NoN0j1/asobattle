#pragma once
#include"VECTOR2.h"

struct CrossProduct_2D
{
	// 二次元の外積　引数：始点,終点1,終点2
	double operator()(VECTOR2 startPos, VECTOR2 endPos1, VECTOR2 endPos2)
	{
		VECTOR2 vec1(endPos1 - startPos);
		VECTOR2 vec2(endPos2 - startPos);
		return ( (vec1.x * vec2.y) - (vec1.y * vec2.x) );
	}
};