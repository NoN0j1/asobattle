#pragma once
#include<list>
#include<array>
#include<vector>
#include"CHARA_ID.h"
#include"ObjList.h"
#include "BaseScene.h"

#define PLAYER_CNT_MAX (4)

enum class STATUS_TYPE{
	HP,				//体力
	SPECIAL,		//必殺技ｹﾞｰｼﾞの最大量
	POWER,			//攻撃力
	SPEED,			//移動速度
	MAX
};

class CharacterSelectScene :
	public BaseScene
{
public:
	CharacterSelectScene();			//ｺﾝｽﾄﾗｸﾀ
	~CharacterSelectScene();		//ﾃﾞｽﾄﾗｸﾀ
	unique_Base Update(unique_Base own, const GameCtl &ctl);	//ｷｬﾗｸﾀｰｾﾚｸﾄ時の処理　引数:現在のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀ,GameCtlｸﾗｽの情報　返り値として次のﾌﾚｰﾑ時のｼｰﾝｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀを返す
private:
	int playerCnt;					// ﾌﾟﾚｲ人数
	sharedListObj objList;			//Objｸﾗｽのﾕﾆｰｸﾎﾟｲﾝﾀ型のﾘｽﾄのｼｪｱｰﾄﾞﾎﾟｲﾝﾀ
	bool allStandbyFlag;			// ﾌﾟﾚｲﾔｰ全員がｷｬﾗｸﾀｰを選び終わっているかのﾌﾗｸﾞ　true:選び終わっている, false:選び終わっていない

	int bgImage;
	int statusfont;

	std::array<int[static_cast<int>(STATUS_TYPE::MAX)], static_cast<int>(CHARA_ID::RANDOM)> status;		//ｷｬﾗｸﾀｰｽﾃｰﾀｽ情報のﾃｰﾌﾞﾙ
	std::array<std::array<char, 20>, static_cast<int>(STATUS_TYPE::MAX)> statusTextTbl;					//ｽﾃｰﾀｽUIのﾃｷｽﾄ
	std::array<VECTOR2, PLAYER_CNT_MAX> statusTextPosTbl;		//ｽﾃｰﾀｽ説明文字の表示位置ﾃｰﾌﾞﾙ

	std::array<VECTOR2, PLAYER_CNT_MAX> playerNumPosTbl;		//ﾌﾟﾚｲﾔｰの番号表示位置のﾃｰﾌﾞﾙ
	std::array<unsigned int, PLAYER_CNT_MAX> playerColorTbl;	//各ﾌﾟﾚｲﾔｰの色情報のﾃｰﾌﾞﾙ
	std::array<VECTOR2, PLAYER_CNT_MAX> stOutLinePosTbl;		//ｽﾃｰﾀｽ表示のｱｳﾄﾗｲﾝの位置のﾃｰﾌﾞﾙ
	std::array<VECTOR2, PLAYER_CNT_MAX> stCharaPosTbl;			//ｽﾃｰﾀｽ表示のｷｬﾗ絵の表示位置のﾃｰﾌﾞﾙ
	std::array<VECTOR2, PLAYER_CNT_MAX> stCharaNamePosTbl;		//ｽﾃｰﾀｽ表示のｷｬﾗ名の表示位置のﾃｰﾌﾞﾙ
	std::array<VECTOR2, PLAYER_CNT_MAX> cursorPosTbl;			//ｶｰｿﾙの初期位置のﾃｰﾌﾞﾙ
	std::array<std::array<char, 12>, static_cast<int>(CHARA_ID::MAX)> charaNameTbl;		//ｷｬﾗ名のﾃｰﾌﾞﾙ

	int Init(void);					//初期化
	bool CharSelDraw(void);			//描画処理
};
