#pragma once
#include "Obj.h"

#include <array>
#include "VECTOR2.h"

#define CHAR_CNT_MAX (12)

class CharacterSelectCursor :
	public Obj
{
public:
	CharacterSelectCursor(int popCnt, VECTOR2 pos, VECTOR2 drawOffset);
	~CharacterSelectCursor();

	bool CheckObjType(OBJ_TYPE type);	//ｵﾌﾞｼﾞｪｸﾄのﾀｲﾌﾟが引数で指定したﾀｲﾌﾟか確かめる　OBJ_TYPE_CURSORでtrue
	int GetPlayerNo(void);					//popNoの情報を取得する
	void SetMove(const GameCtl &ctl);	//移動処理
	void Draw(unsigned int id);			//描画処理
	bool GetStandbyFlag(void);			//自分がｼｰﾝ以降しても大丈夫かどうかの情報を取得する(true:準備完了, false:準備完了でない)

private:
	VECTOR2 moveSpeed;		//移動速度
	int popNo;				//自分のﾌﾟﾚｲﾔｰの番号
	CHARA_ID playerId;		//自分のﾌﾟﾚｲﾔｰが持っているｷｬﾗID
	CHARA_ID playerIdOld;	//1ﾌﾚｰﾑ前自分のﾌﾟﾚｲﾔｰが持っているｷｬﾗID
	bool decidFlag;			//選択決定しているかどうかのﾌﾗｸﾞ(true:決定している, false:決定していない)

	std::array<VECTOR2, CHAR_CNT_MAX> framePosTbl;		//ﾌﾚｰﾑ(決定時に出る枠)の表示位置のﾃｰﾌﾞﾙ

	void ChackId(void);		//posの値からどのｱｲｺﾝの上にいるのかのﾁｪｯｸをする
};
