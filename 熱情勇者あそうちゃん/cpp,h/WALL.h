#pragma once

enum class WALL
{
	NON,
	LEFT,
	RIGHT,
	MAX
};