#include "ImageMng.h"
#include "SceneMng.h"
#include "SoundMng.h"
#include "GameCtl.h"
#include "SceneChange.h"
#include "TitleScene.h"
#include "MainMenuScene.h"

#define GAME_SCREEN_SIZE_X (1440)			// ½ΈΨ°έΜ‘»²½ή
#define GAME_SCREEN_SIZE_Y (810)			// ½ΈΨ°έΜc»²½ή
#define TITLE_SIZE_X (666)
#define TITLE_SIZE_Y (368)

#define ANIM_END_FRAME (200)


#define INPUT_BUTTON (PAD_INPUT_1 | PAD_INPUT_2 | PAD_INPUT_3 | PAD_INPUT_4 | PAD_INPUT_5 | PAD_INPUT_6 | PAD_INPUT_7 | PAD_INPUT_8 | PAD_INPUT_9 | PAD_INPUT_10 | PAD_INPUT_11) 

TitleScene::TitleScene()
{
	Init();
	OutputDebugString("CharacteSselectSceneΈΧ½Μ²έ½ΐέ½\n");
}


TitleScene::~TitleScene()
{
	OutputDebugString("CharacteSselectSceneΈΧ½Μν\n");
}

unique_Base TitleScene::Update(unique_Base own, const GameCtl & ctl)
{
	OldSceneChangeFlag = SceneChangeFlag;

	// Ή
	if (AlwaysCnt > ANIM_END_FRAME)
	{
		StopSoundMem(lpSoundMng.GetID("bgm/opening.mp3"));
		if (CheckSoundMem(lpSoundMng.GetID("bgm/title.mp3")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("bgm/title.mp3"), DX_PLAYTYPE_LOOP, true);
		}
	}
	else
	{
		if (CheckSoundMem(lpSoundMng.GetID("bgm/opening.mp3")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("bgm/opening.mp3"), DX_PLAYTYPE_BACK, true);
		}
	}
	ChangeVolumeSoundMem( lpSceneChange.GetBright(), lpSoundMng.GetID("bgm/title.mp3") );
	// --------

	AlwaysCnt++;

	if (AlwaysCnt > ANIM_END_FRAME)
	{
		if (ctl.GetPadTrigger(PAD_NO_1, INPUT_BUTTON) || SceneChangeFlag)
		{
			SceneChangeFlag = true;

			// Ή
			if (SceneChangeFlag != OldSceneChangeFlag)
			{
				PlaySoundMem(lpSoundMng.GetID("se/check.wav"), DX_PLAYTYPE_BACK, true);
			}
			// --------

			if (lpSceneChange.FadeOut(DEF_SCENECHANGE_SPEED, DEF_SCENECHANGE_WAIT, true))
			{
				// Ή
				ChangeVolumeSoundMem(255, lpSoundMng.GetID("bgm/title.mp3"));
				StopSoundMem(lpSoundMng.GetID("bgm/title.mp3"));
				StopSoundMem(lpSoundMng.GetID("se/check.wav"));
				StopSoundMem(lpSoundMng.GetID("bgm/opening.mp3"));
				// --------
				return std::make_unique<MainMenuScene>();
			}
		}
	}
	else
	{
		if (ctl.GetPadTrigger(PAD_NO_1, INPUT_BUTTON))
		{
			AlwaysCnt = ANIM_END_FRAME;
		}
	}
	Draw();

	return std::move(own);
}

int TitleScene::Init()
{
	lpSceneMng.SetGameScreenSize(VECTOR2(GAME_SCREEN_SIZE_X, GAME_SCREEN_SIZE_Y));
	lpSceneMng.SetDrawOffset(VECTOR2( 0, 0 ));
	VECTOR2 gameScreen = lpSceneMng.GetGameScreenSize();

	ChangeFontType(DX_FONTTYPE_EDGE);

	AlwaysCnt = 0;

	SceneChangeFlag = false;
	return 0;
}


void TitleScene::Draw()
{
	ClsDrawScreen();
	if (AlwaysCnt < ANIM_END_FRAME)
	{
		DrawGraph(0, 0, IMAGE_ID("image/Titleback.png")[0], false);
		DrawGraph(GAME_SCREEN_SIZE_X / 2 - TITLE_SIZE_X / 2, 25 + AlwaysCnt / 3, IMAGE_ID("image/title1.png")[0], true);
	}
	else
	{
		DrawGraph(0, 0, IMAGE_ID("image/Titleback.png")[0], false);

		DrawGraph(GAME_SCREEN_SIZE_X / 2 - TITLE_SIZE_X / 2, TITLE_SIZE_Y / 4, IMAGE_ID("image/title2.png")[0], true);

		if (SceneChangeFlag == false)
		{
			if (((AlwaysCnt / 30) % 2))
			{
				int StrLen = 0;
				int StrWidth = 0;
				SetFontSize(40);
				StrLen = (int)strlen("o@o@`mx@atssnm");
				StrWidth = GetDrawStringWidth("o@o@`mx@atssnm", StrLen);
				DrawFormatString(40 + GAME_SCREEN_SIZE_X / 2 - StrWidth / 2, TITLE_SIZE_Y + TITLE_SIZE_Y / 2, 0xffffff, "okd`rd otrg `mx atssnm");
			}
		}
		else
		{
			if (((AlwaysCnt / 4) % 2))
			{
				int StrLen = 0;
				int StrWidth = 0;
				SetFontSize(40);
				StrLen = (int)strlen("o@o@`mx@atssnm");
				StrWidth = GetDrawStringWidth("o@o@`mx@atssnm", StrLen);
				DrawFormatString(40 + GAME_SCREEN_SIZE_X / 2 - StrWidth / 2, TITLE_SIZE_Y + TITLE_SIZE_Y / 2, 0xffffff, "okd`rd otrg `mx atssnm");
			}
		}
	}
	ScreenFlip();
}