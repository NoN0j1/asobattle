#pragma once

#include "DxLib.h"
#include "SceneMng.h"

class TitleScene :
	public BaseScene
{
public:
	TitleScene();
	~TitleScene();
	unique_Base Update(unique_Base own, const GameCtl &ctl);
private:
	int Init();
	void Draw();
	int TitleAnim;
	bool OldSceneChangeFlag;
};