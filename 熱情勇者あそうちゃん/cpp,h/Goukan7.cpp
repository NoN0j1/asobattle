#include "DxLib.h"
#include "SoundMng.h"
#include "ImageMng.h"
#include "SceneMng.h"
#include "Goukan7.h"

#define GOUKAN7_HP_MAX				(110)	//HP�̍ő�l

#define GOUKAN7_DEF_SPEED			(5)		//��̫�Ă̈ړ���߰��

#define GOUKAN7_DEF_POWER			(9)		//��̫�Ă̍U����

#define GOUKAN7_SPECIAL_CNT_MAX		(80)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(422 / 2)		//�K�E�Z�̏c����
#define ANIM_SIZE_Y					(200)			//�K�E�Z�̏c����
#define ANIM_CNT					(2)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(4)				//�K�E�Z�̱�Ұ��݊Ԋu

#define SP_SPEED					(20)			//�K�E�Z�̈ړ���߰��

#define SP_POWER					(30)			//�K�E�Z�̍U����

#define CNT_MAX						(60 * 3)		//�K�E�Z�̔�������

Goukan7::Goukan7(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN7_HP_MAX;
	power = GOUKAN7_DEF_POWER;
	speed = GOUKAN7_DEF_SPEED;
	specialCntMax = GOUKAN7_SPECIAL_CNT_MAX;
	init("image/goukan7.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan7_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(2, 1));

	lpImageMng.GetID("image/cutin/7gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	specialTimeCnt = 0;

	_RPT0(_CRT_WARN, "7���ق�ݽ�ݽ\n");
}

Goukan7::~Goukan7()
{
	_RPT0(_CRT_WARN, "7���ق��폜\n");
}

int Goukan7::GetHPMax(void)
{
	return GOUKAN7_HP_MAX;
}

bool Goukan7::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	power = SP_POWER;

	attackRangeBack  = 50;
	attackRangeFront = 50;

	if (defensePos.x <= 200)
	{
		turnFlag = false;
	}
	if (defensePos.x + defenseSize.x >= lpSceneMng.GetGameScreenSize().x - 200)
	{
		turnFlag = true;
	}
	if (CUTIN_END_FRAME < specialTimeCnt)
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/7goEX.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/7goEX.wav"), DX_PLAYTYPE_LOOP, false);
		}
		pos.x += SP_SPEED * ((!turnFlag) - turnFlag);
	}

	attackFlag = true;
	attackPos = VECTOR2(defensePos.x + (defenseSize.x / 2) - (ANIM_SIZE_X / 2), depth - ANIM_SIZE_Y * 15 / 10);
	attackSize = VECTOR2(ANIM_SIZE_X * 15/10, ANIM_SIZE_Y * 15/10);

	if (specialTimeCnt > CNT_MAX + CUTIN_END_FRAME)
	{
		//�K�E�Z�I�����ɍs����
		StopSoundMem(lpSoundMng.GetID("se/7goEX.wav"));
		power = GOUKAN7_DEF_POWER;
		damageFlag	= true;
		specialFlag = false;
		attackRangeBack		= DEF_ATTACK_BACK;
		attackRangeFront	= DEF_ATTACK_FRONT;
		Character::move = &Goukan7::Normal;
	}

	specialTimeCnt++;

	return true;
}

void Goukan7::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	DrawOval(drawOffset.x + attackPos.x + (attackSize.x / 2), drawOffset.y + attackPos.y + attackSize.y - 10, attackSize.x / 2, 20, 0x000000, true);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + attackPos.x,
			drawOffset.y + attackPos.y,
			drawOffset.x + attackPos.x + (ANIM_SIZE_X * 15/10),
			drawOffset.y + attackPos.y + (ANIM_SIZE_Y * 15/10),
			lpImageMng.GetID("image/goukan7_Special.png")[(specialTimeCnt / ANIM_FRAME) % ANIM_CNT], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + attackPos.x + (ANIM_SIZE_X * 15 / 10),
			drawOffset.y + attackPos.y,
			drawOffset.x + attackPos.x,
			drawOffset.y + attackPos.y + (ANIM_SIZE_Y * 15 / 10),
			lpImageMng.GetID("image/goukan7_Special.png")[(specialTimeCnt / ANIM_FRAME) % ANIM_CNT], true);
	}
	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/7gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/7gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan7::GetDefPower(void)
{
	return GOUKAN7_DEF_POWER;
}
