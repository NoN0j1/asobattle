#pragma once
#include<list>
#include<memory>

// ﾌﾟﾛﾄﾀｲﾌﾟ宣言
class Obj;

using sharedObj = std::shared_ptr<Obj>;				// Objｸﾗｽのｼｪｱｰﾄﾞﾎﾟｲﾝﾀの型
using sharedObjList = std::list <sharedObj>;			// Objｸﾗｽのｼｪｱｰﾄﾞﾎﾟｲﾝﾀのﾘｽﾄの型
using sharedListObj = std::shared_ptr<sharedObjList>;	// Objｸﾗｽのｼｪｱｰﾄﾞﾎﾟｲﾝﾀのﾘｽﾄのｼｪｱｰﾄﾞﾎﾟｲﾝﾀの型
using weekListObj = std::weak_ptr<sharedObjList>;		// Objｸﾗｽのｼｪｱｰﾄﾞﾎﾟｲﾝﾀのﾘｽﾄのｳｨｰｸﾎﾟｲﾝﾀの型
using ListObj_itr = sharedObjList::iterator;			// Objｸﾗｽのｼｪｱｰﾄﾞﾎﾟｲﾝﾀのﾘｽﾄのｲﾃﾚｰﾀの型

struct AddObjList
{
	// objListにobjPtrを追加する　引数:objListのｳｨｰｸﾎﾟｲﾝﾀ,Objｸﾗｽのｼｪｱｰﾄﾞﾎﾟｲﾝﾀ
	ListObj_itr operator()(weekListObj objList, sharedObj objPtr)
	{
		objList.lock()->push_back(std::move(objPtr));	// objListの最後にobjPtrを追加する objPtrから所有権をobjListに移す
		ListObj_itr itr = objList.lock()->end();
		itr--;
		return itr;										// objPtrのobjListに置けるｲﾃﾚｰﾀを返す
	}
};
