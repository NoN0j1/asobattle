#include "DxLib.h"
#include "ImageMng.h"
#include "SoundMng.h"
#include "SpecialAttack_Nail.h"
#include "Goukan5.h"

#define GOUKAN5_HP_MAX				(110)	//HP�̍ő�l

#define GOUKAN5_DEF_SPEED			(6)		//��̫�Ă̈ړ���߰��

#define GOUKAN5_DEF_POWER			(10)	//��̫�Ă̍U����

#define GOUKAN5_SPECIAL_CNT_MAX		(80)	//�K�E�Z�ް�ނ̍ő�l

#define ANIM_SIZE_X					(736 / 4)		//�K�E�Z�̏c����
#define ANIM_SIZE_Y					(184)			//�K�E�Z�̏c����
#define ANIM_CNT					(4)				//�K�E�Z�̱�Ұ��ݐ�
#define ANIM_FRAME					(20)			//�K�E�Z�̱�Ұ��݊Ԋu

#define SCOPE_RIGHT	(1400 - 200 - NAIL_SIZE_X)		//�B�̏o���͈� �E
#define SCOPE_LEFT	(200)							//�B�̏o���͈� ��
#define SCOPE_TOP	(0)								//�B�̗����͈� ���
#define SCOPE_UNDER	(335)							//�B�̗����͈� ����

Goukan5::Goukan5(VECTOR2 drawOffset, int popNo, bool aiFlag) :Character(drawOffset, popNo, aiFlag)
{
	HP = GOUKAN5_HP_MAX;
	power = GOUKAN5_DEF_POWER;
	speed = GOUKAN5_DEF_SPEED;
	specialCntMax = GOUKAN5_SPECIAL_CNT_MAX;
	init("image/goukan5.png", VECTOR2(256 / 2, 768 / 6), VECTOR2(2, 6));
	initAnim();

	lpImageMng.GetID("image/goukan5_Special.png", VECTOR2(ANIM_SIZE_X, ANIM_SIZE_Y), VECTOR2(4, 1));

	lpImageMng.GetID("image/cutin/5gou_cut.png", VECTOR2(1200 / CUTIN_ANIM_MAX, 300), VECTOR2(CUTIN_ANIM_MAX, 1));

	scopeNailX.param(std::uniform_int_distribution<>::param_type(SCOPE_LEFT, SCOPE_RIGHT));
	scopeDepth.param(std::uniform_int_distribution<>::param_type(SCOPE_TOP, SCOPE_UNDER));

	_RPT0(_CRT_WARN, "5���ق�ݽ�ݽ\n");
}

Goukan5::~Goukan5()
{
	_RPT0(_CRT_WARN, "5���ق��폜\n");
}

int Goukan5::GetHPMax(void)
{
	return GOUKAN5_HP_MAX;
}

bool Goukan5::Special(const GameCtl & ctl, weekListObj objList)
{
	specialCnt = 0;
	specialFlag = true;

	if (specialTimeCnt / ANIM_FRAME == 3)
	{
		AddObjList()(objList, std::make_shared<SpecialAttack_Nail>(drawOffset, VECTOR2(scopeNailX(rnd), -266), playerNo, scopeDepth(rnd)));
	}

	if (specialTimeCnt >= (ANIM_FRAME * ANIM_CNT) - 2)
	{
		//�K�E�Z�I�����ɍs����
		damageFlag = true;
		specialFlag = false;
		Character::move = &Goukan5::Normal;
	}
	else if ((specialTimeCnt / ANIM_FRAME) >= 3)
	{
		if (CheckSoundMem(lpSoundMng.GetID("se/5goEX01.wav")) == 0)
		{
			PlaySoundMem(lpSoundMng.GetID("se/5goEX01.wav"), DX_PLAYTYPE_BACK, true);
		}
	}

	specialTimeCnt++;

	return true;
}

void Goukan5::SpecialDraw(bool specialFlag)
{
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		if (SPcutinFlag == false)
		{
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawGraph(0, 0, lpImageMng.GetID("image/mask.png")[0], true);
			SPcutinFlag = true;
		}
	}

	if ((!turnFlag))
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 60,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15/10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15/10) - 60,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan5_Special.png")[specialTimeCnt / ANIM_FRAME], true);
	}
	else
	{
		DrawExtendGraph(
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) + (ANIM_SIZE_X * 15 / 10) - 30,
			drawOffset.y + (depth - ANIM_SIZE_Y * 15 / 10),
			drawOffset.x + (defensePos.x + (defenseSize.x / 2)) - (ANIM_SIZE_X / 2) - 30,
			drawOffset.y + depth,
			lpImageMng.GetID("image/goukan5_Special.png")[specialTimeCnt / ANIM_FRAME], true);
	}

	// ��Ĳݕ`��
	if (specialTimeCnt < CUTIN_END_FRAME)
	{
		SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);
		int tmpAnim = ((specialTimeCnt / CUTIN_ANIM_SPEED) <= (CUTIN_ANIM_MAX - 1) ? specialTimeCnt / CUTIN_ANIM_SPEED : CUTIN_ANIM_MAX - 1);
		int No = GetPlayerNo();
		if (No < 0 || 3 < No)
		{
			No = 0;
		}
		if (No % 2 == 1)
		{
			// 2P��4P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x - 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/5gou_cut.png")[tmpAnim], true);
		}
		else
		{
			// 1P��3P
			DrawExtendGraph(CutinPosTbl[No].x, CutinPosTbl[No].y, CutinPosTbl[No].x + 321, CutinPosTbl[No].y + 321, IMAGE_ID("image/cutin/5gou_cut.png")[tmpAnim], true);
		}
	}
	//----------
}

int Goukan5::GetDefPower(void)
{
	return GOUKAN5_DEF_POWER;
}
