#include "DxLib.h"
#include "SoundMng.h"
#include "SpecialAttack_Kusanagi.h"

SpecialAttack_Kusanagi::SpecialAttack_Kusanagi(VECTOR2 drawOffset, int popNo) :Obj(drawOffset)
{
	init("image/kusanagi.png", VECTOR2(132, 132), VECTOR2(1, 1));

	dethFlag = false;

	pos.x = 200;
	pos.y = 335 - divSize.y;

	depth = pos.y + divSize.y;

	playerNo = popNo;

	attackPos = pos;
	attackSize = divSize * 15 / 10;

	attackRangeFront = 25;
	attackRangeBack = 15;

	power = 20;

	upFlag = true;

	speed_X = (MOVE_SCOPE_X) / 30;
	speed_Y = MOVE_SCOPE_Y / 30;

	upCnt = 0;
}

SpecialAttack_Kusanagi::~SpecialAttack_Kusanagi()
{
}

bool SpecialAttack_Kusanagi::CheckDeth(void)
{
	return dethFlag;
}

bool SpecialAttack_Kusanagi::CheckObjType(OBJ_TYPE type)
{
	return (OBJ_TYPE_SP_ATTACK == type);
}

bool SpecialAttack_Kusanagi::CheckHit(VECTOR2 defensePos, VECTOR2 defenseSize, int depth)
{
	if (this->depth + attackRangeFront >= depth && this->depth - attackRangeBack <= depth)
	{
		if (this->attackPos.x < defensePos.x + defenseSize.x
			&&this->attackPos.x + attackSize.x > defensePos.x
			&&this->attackPos.y < defensePos.y + defenseSize.y
			&&this->attackPos.y + attackSize.y > defensePos.y)
		{
			return true;
		}
	}
	return false;
}

int SpecialAttack_Kusanagi::GetPlayerNo(void)
{
	return playerNo;
}

void SpecialAttack_Kusanagi::Draw(void)
{
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 100);

	DrawOval(drawOffset.x + pos.x + ((divSize.x * 15 / 10) / 2), drawOffset.y + depth - 5, divSize.x / 6, 4, 0x000000, true);

	SetDrawBlendMode(DX_BLENDMODE_NOBLEND, 0);

	Obj::Draw();
}

void SpecialAttack_Kusanagi::SetMove(const GameCtl & ctl, weekListObj objList)
{
	if (CheckSoundMem(lpSoundMng.GetID("se/6goEX03.wav")) == 0)
	{
		PlaySoundMem(lpSoundMng.GetID("se/6goEX03.wav"), DX_PLAYTYPE_LOOP, true);
	}
	if (upFlag)
	{
		pos.y -= speed_Y;
	}
	else
	{
		pos.y += speed_Y;
		pos.x += speed_X;
	}

	if (pos.y + divSize.y <= 0)
	{
		pos.y = -divSize.y;
	}

	attackPos = pos;

	depth = pos.y + divSize.y;

	if (depth <= 0)
	{
		upFlag = false;
		upCnt++;
	}
	if (depth >= MOVE_SCOPE_Y)
	{
		upFlag = true;
	}

	if (upCnt >= 2)
	{
		StopSoundMem(lpSoundMng.GetID("se/6goEX03.wav"));
		dethFlag = true;
	}
}
